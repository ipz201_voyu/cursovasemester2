<?php

namespace models;

use core\Core;

class Brands
{
    /**
     * створення нового бренду в базі даних
    */
    public function add($groupRow)
    {
        if (empty($groupRow["nameBrand"])) {
            return "Бренд не може бути порожнім!";
        }

        $countGroupInDB = Core::getInstance()->getDb()->select(
            "brands",
            "*",
            [
                "idTovarGroup" => $groupRow["idTovarGroup"],
                "nameBrand" => $groupRow["nameBrand"]
            ]
        );
        if (count($countGroupInDB) > 0) {
            return "Бренд в даній категорії уже є в базі даних!";
        }
        Core::getInstance()->getDb()->insert("brands", [
            "nameBrand" => $groupRow["nameBrand"],
            "idTovarGroup" => intval($groupRow["idTovarGroup"])
        ]);
        return true;
    }

    /**
     * повернення усіх брендів
     */
    public function getAll()
    {
        return Core::getInstance()->getDb()->select(
            "brands",
            "*",
            null,
            null,
            null,
            null,
            ["tovargroups"],
            ["tovargroups.idTovarGroup" => "brands.idTovarGroup"]
        );
    }

    /**
     * оновлення даних певного бренду
     */
    public function update($groupRow)
    {
        if (empty($groupRow["nameBrand"])) {
            return "Бренд не може бути порожнім!";
        }

        $countGroupInDB = Core::getInstance()->getDb()->select(
            "brands",
            "*",
            [
                "idTovarGroup" => $groupRow["idTovarGroup"],
                "nameBrand" => $groupRow["nameBrand"]
            ],
            "nameBrand"
        );
        if (count($countGroupInDB) > 0 and $countGroupInDB[0]["idBrand"] != $groupRow["idBrand"]) {
            return "Бренд в даній категорії уже є в базі даних!";
        }

        Core::getInstance()->getDb()->update(
            "brands",
            ["nameBrand" => $groupRow["nameBrand"]],
            ["idBrand" => intval($groupRow["idBrand"])]
        );
        return true;
    }

    /**
     * видалення певного бренду за айді
     */
    public function delete($idBrand)
    {
        $tovarsModul = new \models\Products();
        $tempTovars = Core::getInstance()->getDb()->select("temptovars", "*", ["idBrand" => $idBrand]);
        foreach ($tempTovars as $tovar)
            $tovarsModul->delete($tovar["idTempTovar"], true);

        $tovars = Core::getInstance()->getDb()->select("products", "*", ["idBrand" => $idBrand]);
        foreach ($tovars as $tovar)
            $tovarsModul->delete($tovar["idTovar"]);

        Core::getInstance()->getDb()->delete( "brands",["idBrand" => $idBrand]);
        return true;
    }

    /**
     * отримання всіх брендів за групою товарів
     */
    public function getAllByTovarGroup($idTovarGroup)
    {
        return Core::getInstance()->getDb()->select("brands", "idBrand", [
            "idTovarGroup" => $idTovarGroup
        ]);
    }

    public function getCountBrands(){
        return Core::getInstance()->getDb()->myQuery("SELECT Count(nameBrand) as count from
                             (SELECT nameBrand FROM brands GROUP BY nameBrand) temp")[0]["count"];
    }
}
