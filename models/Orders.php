<?php

namespace models;

use Cassandra\Date;
use core\Core;

class Orders
{
    public function addOrder($idQuestionnaire, $ignoreSate=false)
    {
        $questionnaire = Core::getInstance()->getDb()->select("questionnaire", "*",
            ["idQuestionnaire" => $idQuestionnaire])[0];
        if($questionnaire) {

            $dateStart = $ignoreSate?null:date('d-m-y');

            Core::getInstance()->getDb()->insert("orders",
                [
                    "addressDelivery"=>$questionnaire["addressDelivery"],
                    "fullNameUser"=>$questionnaire["fullNameUser"],
                    "countTovar"=>$questionnaire["countTovar"],
                    "telUser"=>$questionnaire["telUser"],
                    "emailUser"=>$questionnaire["emailUser"],
                    "idTovar"=>$questionnaire["idTovar"],
                    "dateStart"=>$dateStart,
                    "idClient"=>$questionnaire["idClient"],
                    "price"=>$questionnaire["price"]
                ]);
                }
    }

    public function getForClient($idClient)
    {
        $orders = Core::getInstance()->getDb()->select("orders","idTovar, countTovar, dateStart, dateFinish, price",
            ["idClient" => $idClient]);
        return $orders;
    }

    public function getForSeller($idUser)
    {
        $orders = Core::getInstance()->getDb()->select("orders","idOrder, orders.idTovar,nameTovar, 
        orders.countTovar as countTovars,priceTovar, dateStart, dateFinish, price",
            "idUser = $idUser and not isNull(dateStart) and isNull(dateFinish)",
            null,null,null,["tovars"], ["tovars.idTovar"=>"orders.idTovar"]);
        return $orders;
    }

    public function getStatisticForSeller($idSeller){

        function firstQuerySubString ($idSeller, $whereAnd){
            return " SELECT tovars.idTovar, SUM(orders.countTovar) as countTovars, nameTovar,nameTovarGroup, price FROM orders
        JOIN tovars on(orders.idTovar = tovars.idTovar)
        JOIN brands on (brands.idBrand = tovars.idBrand)
        JOIN tovargroups on (brands.idTovarGroup = tovargroups.idTovarGroup)
        WHERE idUser = '$idSeller' and $whereAnd
        GROUP BY tovars.idTovar, nameTovar, price, nameTovarGroup";
        }

        $ordersReady = Core::getInstance()->getDb()->myQuery(
            firstQuerySubString($idSeller,"NOT ISNULL(dateFinish)")
        );

        $ordersProcessed = Core::getInstance()->getDb()->myQuery(
            firstQuerySubString($idSeller,"ISNULL(dateFinish) and NOT ISNULL(dateStart)")
        );

        $ordersUnaccepted = Core::getInstance()->getDb()->myQuery(
            firstQuerySubString($idSeller,"ISNULL(dateStart)")
        );
        return ["ordersReady"=>$ordersReady,
            "ordersProcessed"=>$ordersProcessed,
            "ordersUnaccepted"=>$ordersUnaccepted];
    }

    public function getMoney($idSeller)
    {
        $sqlStr = "Select Sum(SumTovars) as money FROM (SELECT price as SumTovars FROM orders
        JOIN tovars on(orders.idTovar = tovars.idTovar)
        WHERE idUser = '$idSeller' and not ISNULL(dateFinish)
        GROUP BY tovars.idTovar, nameTovar, price) temp";
        return Core::getInstance()->getDb()->myQuery($sqlStr)[0]["money"];
    }

    public function setFinishDate($idOrder){
        Core::getInstance()->getDb()->update("orders", ["dateFinish"=>date('d-m-y')], ["idOrder"=>$idOrder]);
    }
}