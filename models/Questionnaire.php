<?php

namespace models;

use core\Core;

class Questionnaire
{
    public function add($row, $idClient)
    {

        $productsModel = new \models\Products();

        $tovar = $productsModel->getShrotInfoById($row["idTovar"]);

        return Core::getInstance()->getDb()->insert("questionnaire", [
            "fullNameUser"=>$row["fullNameUser"],
            "addressDelivery"=>$row["adressUser"],
            "telUser"=>$row["telephoneUser"],
            "idTovar"=>$row["idTovar"],
            "countTovar"=>$row["countTovar"],
            "emailUser"=>$row["emailUser"],
            "price"=>$row["countTovar"]*(100-$tovar["actionTovar"])/100*$tovar["priceTovar"],
            "idClient"=>$idClient
        ]);
    }

    public function getInfo($idQuestionnaire)
    {
        return Core::getInstance()->getDb()->select("questionnaire","nameTovar, tovars.countTovar as storeCount,
          questionnaire.countTovar as wantedCount,price , actionTovar, priceTovar, fullNameUser, addressDelivery, telUser, emailUser, idQuestionnaire",
            ["idQuestionnaire" => $idQuestionnaire], null, null, null,
            ["tovars"], ["tovars.idTovar"=>"questionnaire.idTovar"])[0];
    }

    public function getForUser($idUser)
    {
        return Core::getInstance()->getDb()->select("questionnaire","nameTovar, questionnaire.countTovar as wantedCount,
         actionTovar, priceTovar, idQuestionnaire", [
            "idUser" => $idUser
        ], null, null, null, ["tovars"], ["tovars.idTovar"=>"questionnaire.idTovar"]);
    }

    public function getShortQuestionnaireInfo($idQuestionnaire)
    {
        return Core::getInstance()->getDb()->select("questionnaire","nameTovar, idClient , fullNameUser, emailUser", [
            "idQuestionnaire" => $idQuestionnaire
        ],
        null, null, null, ["tovars"],["tovars.idTovar"=>"questionnaire.idTovar"])[0];
    }
    public function delete($idQuestionnaire){
        $userInfo = $this->getShortQuestionnaireInfo($idQuestionnaire);
        Core::getInstance()->getDb()->delete("questionnaire", ["idQuestionnaire" => $idQuestionnaire]);
        return $userInfo;
    }

    public function accept($idQuestionnaire){
        $tovarIdAndCount = Core::getInstance()->getDb()->select("questionnaire","idTovar, countTovar",
            ["idQuestionnaire" => $idQuestionnaire])[0];
        $countStoreTovar=Core::getInstance()->getDb()->select("tovars","countTovar",
            ["idTovar" => $tovarIdAndCount["idTovar"]])[0]["countTovar"];

        if($tovarIdAndCount["countTovar"]>$countStoreTovar)
            return "Кількість товару менше за запитану!";

        Core::getInstance()->getDb()->update("tovars", "countTovar = "."countTovar - ".$tovarIdAndCount["countTovar"],
            ["idTovar"=>$tovarIdAndCount["idTovar"]]);
        return $this->delete($idQuestionnaire);
    }

    public function getForClient($idClient)
    {
        $questionnaires = Core::getInstance()->getDb()->select("questionnaire","idQuestionnaire, idTovar, countTovar, price, 'Очікує підтвердження' as status",
            ["idClient" => $idClient]);
        return $questionnaires;
    }

    public function close($id,$idClient){
        $idClientInOrder = $this->getShortQuestionnaireInfo($id)["idClient"];
        if($idClientInOrder == $idClient) {
            Core::getInstance()->getDb()->delete("questionnaire", ["idQuestionnaire" => $id]);
            return '';
        }
        return "Не цей юзер робив замовлення";
    }
}