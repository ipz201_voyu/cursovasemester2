<?php

namespace models;

use core\Core;

class Groups
{
    /**
     * додання нової категорії товарів
     * @param $groupRow array назва нової категорії
     */
    public function addGroup($groupRow)
    {
        if (empty($groupRow["nameTovarGroup"])) {
            return "Категорія не може бути порожня!";
        }
        if ($groupRow["nameTovarGroup"][strlen($groupRow["nameTovarGroup"]) - 1] == "/") {
            $groupRow["nameTovarGroup"] = substr($groupRow["nameTovarGroup"], 0, -1);
        }

        $countGroupInDB = Core::getInstance()->getDb()->select(
            "tovargroups",
            "*",
            [
                "nameTovarGroup" => $groupRow["nameTovarGroup"]
            ]
        );
        if (count($countGroupInDB) > 0) {
            return "Категорія уже є в базі даних!";
        }
        Core::getInstance()->getDb()->insert("tovargroups", $groupRow);
        return true;
    }

    /**
     * повертає усі групи товарів
     * @return array
     */
    public function getAllGroups()
    {
        return Core::getInstance()->getDb()->select("tovargroups", "*");
    }

    /**
     * повертає групу товарів за певного айді
     * @return array
     */
    public function getGroupById($id)
    {
        return Core::getInstance()->getDb()->select("tovargroups", "*", ["idTovarGroup" => $id])[0];
    }

    /**
     * оновлення назви групи товарів
     * @param $groupRow array нова назва та айді елемента
     */
    public function updateGroup($groupRow)
    {
        if (empty($groupRow["nameTovarGroup"])) {
            return "Категорія не може бути порожня!";
        }
        if ($groupRow["nameTovarGroup"][strlen($groupRow["nameTovarGroup"]) - 1] == "/") {
            $groupRow["nameTovarGroup"] = substr($groupRow["nameTovarGroup"], 0, -1);
        }

        $countGroupInDB = Core::getInstance()->getDb()->select(
            "tovargroups",
            "*",
            [
                "nameTovarGroup" => $groupRow["nameTovarGroup"]
            ]
        );
        if (count($countGroupInDB) > 0 and $countGroupInDB[0]["idTovarGroup"] != $groupRow["idTovarGroup"]) {
            return "Категорія уже є в базі даних!";
        }

        Core::getInstance()->getDb()->update(
            "tovargroups",
            ["nameTovarGroup" => $groupRow["nameTovarGroup"]],
            ["idTovarGroup" => $groupRow["idTovarGroup"]]
        );
        return true;
    }

    /**
     * видалення групи товарів
     * @param $idTovarGroup string|int айді групи
     */
    public function deleteGroup($idTovarGroup)
    {
        $brandsModel = new \models\Brands();
        $brands = $brandsModel->getAllByTovarGroup($idTovarGroup);
        foreach ($brands as $brand)
            $brandsModel->delete($brand["idBrand"]);

        Core::getInstance()->getDb()->delete(
            "tovargroups",
            ["idTovarGroup" => $idTovarGroup]
        );
    }

    /**
     * отримання даних за якими можна фільтрувати товари
     * @param $idGroup string айді категорії товару за яким відбудеться пошук даних
     * @return array повертає знайдені дані
     */
    public function getCharacteristics($idGroup){
        $brands= Core::getInstance()->getDb()->myQuery('SELECT DISTINCT idBrand, nameBrand FROM brands WHERE idTovarGroup='.$idGroup);
        $max = Core::getInstance()->getDb()->myQuery('SELECT MAX(priceTovar) as max FROM products JOIN brands
    ON (brands.idBrand = products.idBrand) WHERE idTovarGroup='.$idGroup)[0]["max"];
        $min = Core::getInstance()->getDb()->myQuery('SELECT MIN(priceTovar) as min FROM products JOIN brands
    ON (brands.idBrand = products.idBrand) WHERE idTovarGroup='.$idGroup)[0]["min"];
        return ["brands"=>$brands, "min"=>$min, "max"=>$max];
    }

    public function getCountGroups(){
        return Core::getInstance()->getDb()->select(
            "tovargroups",
            "count(idTovarGroup) as count"
        )[0]["count"];
    }
}
