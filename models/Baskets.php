<?php

namespace models;

use core\Core;

class Baskets
{
    public function add($idTovar, $idUser)
    {
        return Core::getInstance()->getDb()->insert(
            "baskets",
            [
                "idTovar" => $idTovar,
                "idUser" => $idUser
            ]
        );
    }

    public function get($idUser)
    {
        return Core::getInstance()->getDb()->select(
            "baskets",
            [
                "idBasket", "tovars.idTovar", "nameTovar", "countTovar", "priceTovar", "actionTovar"
            ],
            ["baskets.idUser" => $idUser],
            null,
            null,
            null,
            ["tovars"],
            ["tovars.idTovar" => "baskets.idTovar"]
        );
    }

    public function getByTovarId($idTovar, $idUser)
    {
        return Core::getInstance()->getDb()->select(
            "baskets",
            "idBasket",
            [
                "idUser" => $idUser,
                "idTovar" => $idTovar
            ]
        )[0]["idBasket"];
    }

    public function delete($idBasket)
    {
        $res = Core::getInstance()->getDb()->select("baskets", "idUser, idTovar", [
            "idBasket" => $idBasket
        ]);
        Core::getInstance()->getDb()->delete("baskets", ["idBasket" => $idBasket]);
        return $res[0];
    }
}
