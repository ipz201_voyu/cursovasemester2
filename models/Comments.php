<?php

namespace models;

use core\Core;

class Comments
{
    protected $user = null;

    public function __construct()
    {
        $userModel = new Users();
        $this->user = $userModel->getUser();
    }

    public function add($idTovar, $textComment, $ratingTovar, $idForComment = null)
    {
        if ($idForComment == null) {
            $idForComment = -1;
        }
        return Core::getInstance()->getDb()->insert("comments", [
            "idTovar" => $idTovar,
            "idUser" => $_SESSION["user"]["idUser"],
            "dateAdding" => date("Y-m-d H:i:s"),
            "ratingTovar" => $ratingTovar,
            "textComment" => $textComment,
            "idForComment" => $idForComment
        ]);
    }

    public function addToComment($idTovar, $textComment, $idForComment)
    {
        return Core::getInstance()->getDb()->insert("comments", [
            "idTovar" => $idTovar,
            "idUser" => $_SESSION["user"]["idUser"],
            "dateAdding" => date("Y-m-d H:i:s"),
            "textComment" => $textComment,
            "idForComment" => $idForComment
        ]);
    }

    public function getCommentsTovar($idTovar)
    {
        $arrWhere = [
            "comments.idTovar" => $idTovar,
            "idForComment" => '-1'
        ];
        $stringWhere = "comments.idTovar = '" . $idTovar . "' and idForComment = '-1'";

        if ($this->user["idUser"]) {
            $arrWhere["comments.idUser"] = $this->user["idUser"];
            $stringWhere = "idTovar = '" . $idTovar . "' and idForComment = -1 and comments.idUser != '" . $this->user["idUser"] . "'";
        }
        $commentActualUser = Core::getInstance()->getDb()->select(
            "comments",
            "idComment, textComment, ratingTovar, dateAdding, idForComment,
        CONCAT(firstNameUser,' ',secondNameUser) as nameUser, CONCAT(image,'_1',typeImage) as href",
            $arrWhere,
            "dateAdding",
            null,
            null,
            ["users"],
            ["users.idUser" => "comments.idUser"]
        );
        if ($this->user["idUser"]) {
            $commentsAnotherUsers = Core::getInstance()->getDb()->select(
                "comments",
                "idComment, textComment, ratingTovar, dateAdding, idForComment,
        CONCAT(firstNameUser,' ',secondNameUser) as nameUser, CONCAT(image,'_1',typeImage) as href",
                $stringWhere,
                "dateAdding DESC",
                null,
                null,
                ["users"],
                ["users.idUser" => "comments.idUser"]
            );
            return array_merge($commentActualUser, $commentsAnotherUsers);
        }
        return $commentActualUser;
    }

    public function getCommentsByIdTovarAndIdFor($idTovar, $idFor)
    {
        $arrWhere = [
            "comments.idTovar" => $idTovar,
            "idForComment" => $idFor,
        ];
        $stringWhere = "comments.idTovar = '" . $idTovar . ' and idForComment = ' . $idFor . "'";
        if ($this->user["idUser"]) {
            $arrWhere["comments.idUser"] = $this->user["idUser"];
            $stringWhere = "idTovar = '" . $idTovar . "' and idForComment = '" . $idFor . "' and comments.idUser != '" . $this->user["idUser"] . "'";
        }
        $commentActualUser = Core::getInstance()->getDb()->select(
            "comments",
            "idComment, textComment, dateAdding, idForComment,
        CONCAT(firstNameUser,' ',secondNameUser) as nameUser, CONCAT(image,'_1',typeImage) as href",
            $arrWhere,
            "dateAdding ASC",
            null,
            null,
            ["users"],
            ["users.idUser" => "comments.idUser"]
        );

        if ($this->user["idUser"]) {
            $commentsAnotherUsers = Core::getInstance()->getDb()->select(
                "comments",
                "idComment, textComment, dateAdding, idForComment,
        CONCAT(firstNameUser,' ',secondNameUser) as nameUser, CONCAT(image,'_1',typeImage) as href",
                $stringWhere,
                null,
                null,
                null,
                ["users"],
                ["users.idUser" => "comments.idUser"]
            );
            return array_merge($commentActualUser, $commentsAnotherUsers);
        }
        return $commentActualUser;
    }

    public function checkUsersCommentsTovars($idTovar)
    {
        $comment = Core::getInstance()->getDb()->select(
            "comments",
            "idComment",
            [
                "idUser" => $this->user["idUser"],
                "idForComment" => '-1',
                "idTovar" => $idTovar,
            ]
        )[0];
        if ($comment) {
            return true;
        }
        return false;
    }

    public function getCommentById($idComment)
    {
        return Core::getInstance()->getDb()->select(
            "comments",
            "idComment, idTovar, textComment, ratingTovar, dateAdding, idForComment,
        CONCAT(firstNameUser,' ',secondNameUser) as nameUser, CONCAT(image,'_1',typeImage) as href",
            ["idComment" => $idComment],
            "dateAdding DESC",
            null,
            null,
            ["users"],
            ["users.idUser" => "comments.idUser"]
        )[0];
    }

    public function getAvgStars($idTovar)
    {
        $rating = Core::getInstance()->getDb()->select(
            "comments",
            "AVG(ratingTovar) as avg",
            "ratingTovar IS NOT NULL AND idTovar= $idTovar"
        )[0]["avg"];
        if ($rating === 0) {
            $rating = 5;
        }
        return $rating;
    }

    public function getMainCommentsCount($idTovar)
    {
        return Core::getInstance()->getDb()->select(
            "comments",
            "COUNT(ratingTovar) as count",
            "ratingTovar IS NOT NULL AND idTovar= $idTovar"
        )[0]["count"];
    }

    public function deleteCommentsByIdTovar($id)
    {
        Core::getInstance()->getDb()->delete("comments", ["idTovar" => $id]);
    }
}
