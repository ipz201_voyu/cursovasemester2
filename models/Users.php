<?php

namespace models;

use core\Core;
use core\Model;

class Users
{
    public function __construct()
    {
        $res = "";
    }

    /**
     * змінює поточне фото з акаунта користувача
     * @param $table string назва таблиці
     * @param $id string|int айді юзера в якому зміниться посилання на аватарку
     * @param $href string посилання на аватарку
     * @param $type string тип малюнка
     */
    public function changePhoto($table, $id, $href, $type)
    {
        $image = Core::getInstance()->getDb()->select($table, ["image", "typeImage"], ["idUser" => $id])[0];
        if (is_file('images/users/' . "_1" . $href . $type) && is_file('images/users/' . $image["image"] . $image["typeImage"])) {
            unlink('images/users/' . $image["image"] . "_1" . $image["typeImage"]);
            unlink('images/users/' . $image["image"] . "_2" . $image["typeImage"]);
        }
        Core::getInstance()->getDb()->update(
            $table,
            [
                "image" => $href,
                "typeImage" => $type
            ],
            ["idUser" => $id]
        );
    }

    /**
     * видаляє фото за його посиланням
     * @param $href string посилання малюнку
     */
    public function deletePhoto($href)
    {
        $image = Core::getInstance()->getDb()->select("users", ["idUser", "image", "typeImage"], ["image" => $href])[0];
        if (is_file('images/users/'  . $image["image"]. "_1" . $image["typeImage"])) {
            unlink('images/users/' . $image["image"] . "_1" . $image["typeImage"]);
            unlink('images/users/' . $image["image"] . "_2" . $image["typeImage"]);
        }
        Core::getInstance()->getDb()->update(
            "users",
            [
                "image" => "",
                "typeImage" => ""
            ],
            ["idUser" => $image["idUser"]]
        );
        return $image["idUser"];
    }


    /**
     * валідація даних які вводить користувач при реєстрації чи зміні даних
     * @param $formRow array рядок з параметрами
     * @param $ignoreEmptyPassword false параметр, який буде ігнорувати перевірку паролю, аб ні
     * @param $id null айді користувача
     * @return bool|string повертає true якщо валідовані дані коректні або помилки
     */
    public function validateRegistration($formRow, $ignoreEmptyPassword = false, $id = null)
    {
        if (empty($formRow['login']))
            return "Логін не може бути порожнім!";

        if ($this->checkLoginInDb("tempusers", $formRow['login']))
            return "Логін уже використовується!";

        if ($this->checkLoginInDb("users", $formRow['login'], $id))
            return "Логін уже використовується!";

        if (empty($formRow['password']) and !$ignoreEmptyPassword)
            return "Пароль не може бути порожнім!";

        if (strlen($formRow['password']) < 8 and !$ignoreEmptyPassword)
            return "Пароль не може бути менше 8 символів!";

        if ($formRow['password'] != $formRow['repeatPassword'] and !$ignoreEmptyPassword)
            return "Паролі не співпадають!";

        if (empty($formRow['firstName']))
            return "Ім'я не може бути порожнім!";

        if (empty($formRow['secondName']))
            return "Прізвище не може бути порожнім!";

        if (empty($formRow['telephone']))
            return "Номер телефону не може бути порожнім!";

        return true;
    }

    /**
     * додає нормального користувача
     * @param $userRow array параметри які будуь додані в таблицю юзерів
     * @return string id нового юзера
     */
    public function addUser($userRow)
    {
        return Core::getInstance()->getDb()->insert("users", [
            "firstNameUser" => $userRow['firstNameUser'],
            "secondNameUser" => $userRow['secondNameUser'],
            "loginUser" => $userRow['loginUser'],
            "passwordUser" => $userRow['passwordUser'],
            "accessUser" => $userRow['accessUser'],
            "salt" => $userRow['salt'],
            "image" => $userRow['image'],
            "typeImage" => $userRow['typeImage'],
            "telephoneUser" => $userRow['telephoneUser']
        ]);
    }

    /**
     * додає тимчасового користувача
     * @param $userRow array параметри які будуь додані в таблицю тимчасових юзерів
     * @return array|string повертає помилки або ключ варифікації та id останнього елемента
     */
    public function addTemplateUser($userRow)
    {
        $resValidate = $this->validateRegistration($userRow);
        if ($resValidate !== true) {
            return $resValidate;
        }
        $salt = strval(time());
        $lastId = Core::getInstance()->getDb()->insert("tempusers", [
            "firstNameUser" => $userRow['firstName'],
            "secondNameUser" => $userRow['secondName'],
            "loginUser" => $userRow['login'],
            "passwordUser" => hash("ripemd128", $userRow['password'] . $salt),
            "accessUser" => 0,
            "salt" => $salt,
            "telephoneUser"=>$userRow['telephone']

        ]);
        return $this->createTempKey(0, $lastId);
    }

    /**
     * створює тимчасовий ключ варифікації
     * @param $status int статус ключа
     * @param $lastId string|int id останнього тимчасово-доданого користувача
     * @return array ключ варифікації та id останнього елемента
     */
    public function createTempKey($status, $lastId)
    {
        $keyId = Core::getInstance()->getDb()->insert("tempkeys", [
            "idUser" => intval($lastId),
            "statusKey" => $status,
            "dateKey" => date("Y-m-d H:i:s"),
            "hashKey" => "qweq"
        ]);
        $hashKeyId = hash("ripemd128", $keyId);
        Core::getInstance()->getDb()->update(
            "tempkeys",
            ["hashKey" => $hashKeyId],
            ["idKey" => $keyId]
        );
        return ["tempKeyHash" => $hashKeyId, "id" => $lastId];
    }

    /**
     * оновлює тимчасовий ключ варифікації
     * @param $lastId string|int id останнього тимчасово-доданого користувача
     * @return array|null ключ варифікації
     */
    public function updateTempKey($lastId)
    {
        $key = $this->getTempKeyByIdUser($lastId);
        if (empty($key)) {
            return null;
        }
        $this->removeTempKey($key["idKey"]);

        $keyId = Core::getInstance()->getDb()->insert("tempkeys", [
            "idUser" => $key["idUser"],
            "statusKey" => $key["statusKey"],
            "dateKey" => date("Y-m-d H:i:s"),
            "hashKey" => "qweq"
        ]);
        $hashKeyId = hash("ripemd128", $keyId);
        Core::getInstance()->getDb()->update(
            "tempkeys",
            ["hashKey" => $hashKeyId],
            ["idKey" => $keyId]
        );
        return ["tempKeyHash" => $hashKeyId];
    }


    /**
     * повертає знайденого за логіном та паролем коистувача
     * @param $userRow array логін та пароль користувача який хоче залогінитись
     * @return array|string дані знайденого користувача
     */
    public function authUser($userRow)
    {
        $salt = Core::getInstance()->getDb()->select("users", "salt", ["loginUser" => $userRow["login"]]);

        if (empty($salt[0])) {
            return "Password or login is not correct";
        }
        $res = Core::getInstance()->getDb()->select("users", "*", [
            "loginUser" => $userRow['login'],
            "passwordUser" => hash("ripemd128", $userRow['password'] . $salt[0]["salt"])
        ]);
        if (empty($res[0])) {
            return "Password or login is not correct";
        }
        return $res[0];
    }

    /**
     * переавтинтифіковує користувача(потрібно для оновлення поточних даних користувача на сторінці)
     * @param $id string логін та пароль користувача який хоче залогінитись
     * @return array|string помилку або знайденого за даними користувача
     */
    public function reAuthUser($id)
    {
        $res = Core::getInstance()->getDb()->select("users", "*", ["idUser" => $id]);
        if (empty($res[0])) {
            return "Помилка!";
        }
        return $res[0];
    }

    /**
     * оновлення паролю користувачм(для забутого паролю)
     * @param $password string новий пароль
     * @param $id string id користувача пароль якого потрібно змінити
     */
    public function updatePasswordUser($password, $id)
    {
        $salt = strval(time());
        Core::getInstance()->getDb()->update(
            "users",
            [
                "passwordUser" => hash("ripemd128", $password . $salt),
                "salt" => $salt
            ],
            ["idUser" => $id]
        );
    }


    /**
     * перевіряє чи користувач залогінений
     * @return bool
     */
    public function isAuth()
    {
        return isset($_SESSION["user"]);
    }

    /**
     * повертає користувача, який є залогіненим зара
     * @return mixed|null користувача, якщо він є
     */
    public function getUser()
    {
        if ($this->isAuth()) {
            return $_SESSION["user"];
        } else {
            return null;
        }
    }

    /**
     * перевіряє чи є користувач з заданим логіном в таблиці
     * @param $table string таблиця в якій буде пошук
     * @param $login string логін користувача
     * @param $id string айді користувача з певним логіном
     * @return bool результат перевірки
     */
    public function checkLoginInDb($table, $login, $id = null)
    {
        $res = Core::getInstance()->getDb()->select($table, "idUser", ["loginUser" => $login]);
        if (count($res) > 0 && $res[0]["idUser"] != $id) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * перевіряє чи є тимчвсовий ключ в базі даних
     * @param $hash string ключ за яким відбудеться пошук
     * @return bool резуьтат перевірки
     */
    public function checkTempKeyInDb($hash)
    {
        $res = Core::getInstance()->getDb()->select(
            "tempkeys",
            "hashKey",
            ["hashKey" => $hash]
        );
        if (count($res) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * повертає ключ за хешем
     * @param $hash string хеш
     * @return bool ключ
     */
    public function getTempKey($hash)
    {
        return Core::getInstance()->getDb()->select("tempkeys", "*", ["hashKey" => $hash])[0];
    }

    /**
     * видаляє ключ за певним айді
     * @param $id string айді
     */
    public function removeTempKey($id)
    {
        Core::getInstance()->getDb()->delete("tempkeys", ["idKey" => $id]);
    }

    /**
     * переводить юзера з тимчасової таблиці в основну
     * @param $tempRow array параметри тимчасової таблиці
     */
    public function addNormalUser($tempRow)
    {
        $res = Core::getInstance()->getDb()->select(
            "tempusers",
            "*",
            ["idUser" => $tempRow["idUser"]]
        )[0];
        $this->addUser($res);
        Core::getInstance()->getDb()->delete("tempusers", ["idUser" => $res["idUser"]]);
    }

    /**
     * переводить юзера за логіном
     * @param $login string логін користувача
     * @return mixed|null айді юзера
     */
    public function getUserIdByLogin($login)
    {
        $res = Core::getInstance()->getDb()->select("users", "idUser", ["loginUser" => $login]);
        if (count($res) > 0) {
            return $res[0]["idUser"];
        } else {
            return null;
        }
    }

    /**
     * перевіряє чи є в даного юзера тимчасовий ключ
     * @param $idUser string айді користувача
     * @return bool
     */
    public function checkTempKeyByIdUserInBd($idUser)
    {
        $res = Core::getInstance()->getDb()->select(
            "tempkeys",
            "idUser",
            ["idUser" => $idUser]
        );
        if (count($res) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * отримання тимчасового ключа за id юзера
     * @param $idUser string айді користувача
     * @return bool
     */
    public function getTempKeyByIdUser($idUser)
    {
        $res = Core::getInstance()->getDb()->select(
            "tempkeys",
            "*",
            ["idUser" => $idUser]
        );
        if (count($res) > 0) {
            return $res[0];
        } else {
            return null;
        }
    }

    /**
     * повертає користувача за його id
     * @param $id
     * @return mixed
     */
    public function getUserById($id)
    {
        return Core::getInstance()->getDb()->select("users", "*", ["idUser" => $id])[0];
    }

    /**
     * оновлення даних користувача
     * @param $userRow array новий рядок даних
     * @param $id string айді юзера
     * @return string|bool помилки або підтвердження їх відсутності
     */
    public function updateUser($userRow, $id)
    {
        $resValidate = $this->validateRegistration($userRow, true, $id);
        if ($resValidate !== true) {
            return $resValidate;
        }
        if (!empty($userRow['password'])) {
            $salt = strval(time());
            Core::getInstance()->getDb()->update("users", [
                "firstNameUser" => $userRow['firstName'],
                "secondNameUser" => $userRow['secondName'],
                "loginUser" => $userRow['login'],
                "passwordUser" => hash("ripemd128", $userRow['password'] . $salt),
                "salt" => $salt
            ], ["idUser" => $id]);
        } else {
            Core::getInstance()->getDb()->update("users", [
                "firstNameUser" => $userRow['firstName'],
                "secondNameUser" => $userRow['secondName'],
                "loginUser" => $userRow['login']
            ], ["idUser" => $id]);
        }
        return true;
    }

    /**
     * видалення юзера за його айді
     * @param $id
     */
    public function deleteUser($id)
    {
        $user = $this->getUserById($id);
        $tovarsModul = new \models\Products();
        $tempTovars = Core::getInstance()->getDb()->select("temptovars", "*", ["idUser" => $id]);
        foreach ($tempTovars as $tovar)
            $tovarsModul->delete($tovar["idTempTovar"], true);

        $tovars = Core::getInstance()->getDb()->select("products", "*", ["idUser" => $id]);
        foreach ($tovars as $tovar)
            $tovarsModul->delete($tovar["idTovar"]);

        Core::getInstance()->getDb()->delete("comments", ["idUser" => $id]);
        Core::getInstance()->getDb()->delete("products", ["idUser" => $id]);
        Core::getInstance()->getDb()->delete("baskets", ["idUser" => $id]);
        Core::getInstance()->getDb()->delete("comments", ["textComment LIKE " . $user["firstNameUser"] . " " . $user["secondNameUser"] .
            "% AND idForComment = -1"]);

        if (!empty($user["image"])) {
            unlink('images/users/' . $user["image"] . "_1" . $user["typeImage"]);
            unlink('images/users/' . $user["image"] . "_2" . $user["typeImage"]);
        }
        Core::getInstance()->getDb()->delete("users", ["idUser" => $id]);
    }

    /**
     * отримання користувачів для оновлення їх рівнів доступу
     * @param $count string кількість користувачів
     * @param $startIndex int порчатковий порядковий номер юзера
     * @return array|string
     */
    public function getUsersToUpdateAccess($count, $startIndex = 0)
    {
        return Core::getInstance()->getDb()->select(
            "users",
            "idUser, accessUser, loginUser, CONCAT(firstNameUser,' ',secondNameUser) as nameUser",
            null,
            "nameUser",
            $count,
            $startIndex
        );
    }

    /**
     * отримання користувачів для оновлення їх рівнів доступу
     * @param $where array параметри за якими користувачів буде знайдено
     * @param $count string кількість користувачів
     * @param $startIndex int порчатковий порядковий номер юзера
     * @return array|string
     */
    public function getUsersToUpdateAccessWhere($where, $count, $startIndex = 0)
    {
        return Core::getInstance()->getDb()->select(
            "users",
            "idUser, accessUser, loginUser, CONCAT(firstNameUser,' ',secondNameUser) as nameUser",
            "CONCAT(firstNameUser,' ',secondNameUser) LIKE '%$where%' OR  loginUser LIKE '%$where%'",
            "nameUser",
            $count,
            $startIndex
        );
    }

    /**
     * оновлення рівня доступу користувача за його id
     * @param $id
     * @param $access
     */
    public function updateUserAccess($id, $access)
    {
        Core::getInstance()->getDb()->update("users", ["accessUser" => $access], ["idUser" => $id]);
    }

    /**
     * отримання користувача для оновлення доступу за id
     * @param $idUser
     * @return mixed
     */
    public function getUserToUpdateAccessById($idUser)
    {
        return Core::getInstance()->getDb()->select(
            "users",
            "idUser, accessUser, loginUser, CONCAT(firstNameUser,' ',secondNameUser) as nameUser",
            ["idUser" => $idUser]
        )[0];
    }

    public function getCountUsers(){
        return Core::getInstance()->getDb()->select(
            "users",
            "count(idUser) as count"
        )[0]["count"];
    }

    public function getCountSellers(){
        return Core::getInstance()->getDb()->select(
            "users",
            "count(idUser) as count",
            "accessUser = 1  or accessUser=5"
        )[0]["count"];
    }

    public function getCountTovarsAdmins(){
        return Core::getInstance()->getDb()->select(
            "users",
            "count(idUser) as count",
            ["accessUser"=>2]
        )[0]["count"];
    }

    public function getCountUsersAdmins(){
        return Core::getInstance()->getDb()->select(
            "users",
            "count(idUser) as count",
            "accessUser=3"
        )[0]["count"];
    }

    public function getAllSellers()
    {
        return Core::getInstance()->getDb()->select(
            "users",
            "CONCAT(firstNameUser,' ', secondNameUser) as fullName, idUser",
            "accessUser = 1  or accessUser=5"
        );
    }
}
