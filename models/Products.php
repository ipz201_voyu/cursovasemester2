<?php

namespace models;

use core\Core;

class Products
{
    private static function getCount()
    {
        global $Config;
        return $Config["CountIndexTovars"];
    }

    private function validate($groupRow, $id = null, $temp = false)
    {
        $newRow = [];
        if (strlen($groupRow["name"]) < 2) {
            return "Некоректна назва!";
        } else {
            $newRow["nameTovar"] = $groupRow["name"];
        }
        if ($id == null) {
            if (
                $this->getProductByName("tovars", $groupRow["name"]) or
                $this->getProductByName("temptovars", $groupRow["name"])
            ) {
                return "Даний товар уже є в базі даних!";
            }
        } else {
            if ($temp) {
                $tempId = Core::getInstance()->getDb()->select("temptovars", "idTempTovar", ["nameTovar" => $groupRow["name"]])[0]["idTempTovar"];
                if ($tempId and $tempId != $id) {
                    return "Даний товар уже є в базі даних!";
                }
            } else {
                $res1 = $this->getProductByName("tovars", $groupRow["name"]);
                if (!empty($res1) and $res1["idTovar"] != $id) {
                    return "Даний товар уже є в базі даних!";
                }
                $res2 = $this->getProductByName("temptovars", $groupRow["name"]);
                if (!empty($res2) and $res2["idTovar"] != $id) {
                    return "Даний товар уже є в базі даних!";
                }
            }
        }

        if (intval($groupRow["guarantee"]) < 0) {
            return "Некоректна гарантія!";
        } else {
            $newRow["guaranteeTovar"] = $groupRow["guarantee"];
        }

        if (intval($groupRow["count"]) < 0) {
            return "Некоректна кількість товару!";
        } else {
            $newRow["countTovar"] = $groupRow["count"];
        }

        if (intval($groupRow["action"]) < 0 or intval($groupRow["action"]) > 100) {
            return "Некоректна знижка на товар!";
        } else {
            $newRow["actionTovar"] = $groupRow["action"];
            if ($newRow["actionTovar"] == '') {
                $newRow["actionTovar"] = '0';
            }
        }

        if (floatval($groupRow["price"]) < 0) {
            return "Некоректна ціна на товар!";
        } else {
            $newRow["priceTovar"] = $groupRow["price"];
        }

        if (strlen($groupRow["countryCreator"]) < 2) {
            return "Некоректна країна-виробник!";
        } else {
            $newRow["countryCreator"] = $groupRow["countryCreator"];
        }

        if ($groupRow["idUser"] != null) {
            $newRow["idUser"] = $groupRow["idUser"];
        } else {
            return "Незалогінениц користувач не може додавати товари!";
        }

        if ($groupRow["info"] != null) {
            $newRow["infoTovar"] = $groupRow["info"];
        } else {
            return "Товар без короткої інформації не буде популярним!";
        }

        if ($groupRow["description"] != null) {
            $newRow["descriptionTovar"] = $groupRow["description"];
        } else {
            return "Товар без короткої інформації не буде популярним!";
        }

        $newRow["idBrand"] = $groupRow["brand"];
        return $newRow;
    }

    public function addProduct($groupRow)
    {
        $resValidate = $this->validate($groupRow);
        if (is_string($resValidate)) {
            return $resValidate;
        }
        $resValidate["dateSending"] = date("Y-m-d H:i:s");
        $resValidate["typeChanging"] = 0;
        return intval(Core::getInstance()->getDb()->insert("temptovars", $resValidate));
    }

    public function getProductByName($table, $name)
    {
        $res = Core::getInstance()->getDb()->select($table, "*", ["nameTovar" => $name], null, null, null, null, null);
        if (count($res) > 0) {
            return $res[0];
        } else {
            return false;
        }
    }

    public function getProductById($id, $temp = false)
    {
        $dopTemp = "";
        $table = "tovars";
        if ($temp) {
            $dopTemp = "Temp";
            $table = "temptovars";
        }
        return Core::getInstance()->getDb()->select($table, "*", [
            "id" . $dopTemp . "Tovar" => $id
        ], null, null, null, [
            "brands", "tovargroups"
        ], [
            $dopTemp . "tovars.idBrand" => "brands.idBrand",
            "brands.idTovarGroup" => "tovargroups.idTovarGroup"
        ])[0];
    }

    public function getShrotInfoById($id)
    {
        return Core::getInstance()->getDb()->select(
            "tovars",
            [
                "idTovar", "nameTovar", "countTovar", "priceTovar", "actionTovar"
            ],
            ["idTovar" => $id]
        )[0];
    }

    public function getTempProductByIdTovar($id)
    {
        return Core::getInstance()->getDb()->select("temptovars", "idTempTovar", ["idTovar" => $id])[0]["idTempTovar"];
    }

    public function getSomeProducts($count, $idGroup = null, $start = 0)
    {
        if (!empty($idGroup)) {
            return Core::getInstance()->getDb()->select("tovars", [
                "tovars.idTovar", "nameTovar", "countTovar", "priceTovar", "actionTovar"
            ], [
                "tovargroups.idTovarGroup" => $idGroup,
            ], null, $count, $start, ["brands", "tovargroups"], ["brands.idBrand" => "tovars.idBrand",
                "brands.idTovarGroup" => "tovargroups.idTovarGroup"]);
        }

        return Core::getInstance()->getDb()->select(
            "tovars",
            [
                "tovars.idTovar", "nameTovar", "countTovar", "priceTovar", "actionTovar"
            ],
            null,
            null,
            $count,
            $start
        );
    }


    public function getSomeProductsByParams($count, $idGroup, $priceMin, $priceMax, $brands, $action=false, $start = 0)
    {
        $brandsString = "";
        if ($brands != null) {
            $brandsString = " AND brands.idBrand IN (";
            foreach ($brands as $brand) {
                $brandsString = $brandsString . " " . $brand . ",";
            }
            $brandsString = substr($brandsString, 0, -1) . ")";
        }
        $priceString = "";
        if ($action==='true')
            $priceString = " AND actionTovar > 0";

        return Core::getInstance()->getDb()->select("tovars", [
            "tovars.idTovar", "nameTovar", "countTovar", "priceTovar", "actionTovar"
        ], "tovargroups.idTovarGroup = " . $idGroup . " and priceTovar BETWEEN " . $priceMin . " AND " . $priceMax . $brandsString . $priceString,
            null, $count, $start, ["brands", "tovargroups"], ["brands.idBrand" => "tovars.idBrand",
                "brands.idTovarGroup" => "tovargroups.idTovarGroup"]);
    }


    public function delAllByBrand($idBrand)
    {
        Core::getInstance()->getDb()->delete("tovars", ["idBrand" => $idBrand]);
    }

    public function getTempProducts()
    {
        return Core::getInstance()->getDb()->select(
            "temptovars",
            [
                "idTempTovar",
                "temptovars.idUser",
                "nameTovar",
                "loginUser",
                "dateSending",
                "typeChanging"
            ],
            null,
            null,
            null,
            null,
            [
                "users"
            ],
            [
                "users.idUser" => "temptovars.idUser"
            ]
        );
    }

    public function delete($id, $temp = false, $inheritDelete = false)
    {
        if ($temp) {
            $images = Core::getInstance()->getDb()->select("temptovarsimages", "*", ["idTempTovar" => $id]);
            foreach ($images as $image) {
                unlink('images/tovars/' . $image["hrefImage"] . "_1" . $image["typeImage"]);
                unlink('images/tovars/' . $image["hrefImage"] . "_2" . $image["typeImage"]);
            }
            Core::getInstance()->getDb()->delete("temptovarsimages", ["idTempTovar" => $id]);
            Core::getInstance()->getDb()->delete("temptovars", ["idTempTovar" => $id]);
        } else {
            Core::getInstance()->getDb()->delete("comments", ["idTovar" => $id]);
            $images = Core::getInstance()->getDb()->select("tovarsimages", "*", ["idTovar" => $id]);
            foreach ($images as $image) {
                unlink('images/tovars/' . $image["hrefImage"] . "_1" . $image["typeImage"]);
                unlink('images/tovars/' . $image["hrefImage"] . "_2" . $image["typeImage"]);
            }
            Core::getInstance()->getDb()->delete("tovarsimages", ["idTovar" => $id]);
            Core::getInstance()->getDb()->delete("baskets", ["idTovar" => $id]);
            $tempId = $this->getTempProductByIdTovar($id);
            if ($tempId) {
                $this->delete($tempId, true);
            }
            Core::getInstance()->getDb()->delete("tovars", ["idTovar" => $id]);
        }

    }

    public function acceptProduct($idTemp)
    {
        $values = $this->getProductById($idTemp, true);
        $idTovar = -1;
        if ($values["typeChanging"] == 1) {
            $images = Core::getInstance()->getDb()->select("tovarsimages", "*", ["idTovar" => $idTovar, "isDel"=>1]);
            foreach ($images as $image){
                unlink('images/tovars/' . $image["hrefImage"] . "_1" . $image["typeImage"]);
                unlink('images/tovars/' . $image["hrefImage"] . "_2" . $image["typeImage"]);
            }
            Core::getInstance()->getDb()->delete("tovarsimages", ["idTovar" => $values["idTovar"], "isDel"=>1]);
            Core::getInstance()->getDb()->update(
                "tovars",
                [
                    "actionTovar" => $values["actionTovar"],
                    "nameTovar" => $values["nameTovar"],
                    "idBrand" => $values["idBrand"],
                    "guaranteeTovar" => $values["guaranteeTovar"],
                    "countTovar" => $values["countTovar"],
                    "priceTovar" => $values["priceTovar"],
                    "idUser" => $values["idUser"],
                    "countryCreator" => $values["countryCreator"],
                    "infoTovar" => $values["infoTovar"],
                    "descriptionTovar" => $values["descriptionTovar"]
                ],
                ["idTovar" => $values["idTovar"]]
            );
            $idTovar = $values["idTovar"];
        } else {
            $idTovar = Core::getInstance()->getDb()->insert(
                "tovars",
                [
                    "actionTovar" => $values["actionTovar"],
                    "nameTovar" => $values["nameTovar"],
                    "idBrand" => $values["idBrand"],
                    "guaranteeTovar" => $values["guaranteeTovar"],
                    "countTovar" => $values["countTovar"],
                    "priceTovar" => $values["priceTovar"],
                    "idUser" => $values["idUser"],
                    "countryCreator" => $values["countryCreator"],
                    "infoTovar" => $values["infoTovar"],
                    "descriptionTovar" => $values["descriptionTovar"]
                ]
            );
        }
        $tempImages = Core::getInstance()->getDb()->select("temptovarsimages", "*", [
            "idTempTovar" => $idTemp
        ]);
        foreach ($tempImages as $tempImage) {
            $this->addImageProduct($tempImage, $idTovar);
        }
        $this->delete($idTemp, true);
    }

    public function resetImageByIdProduct($idTovar){
        Core::getInstance()->getDb()->update("tovarsimages", ["isDel"=>0],["idTovar" => $idTovar, "isDel"=>1]);
    }

    public function updateById($row, $id, $temp = false, $light = false)
    {
        $table = "tovars";
        $idKey = "idTovar";
        if ($temp) {
            $table = "temptovars";
            $idKey = "idTempTovar";
        }
        if ($light) {
            Core::getInstance()->getDb()->update(
                $table,
                [
                    "countTovar" => $row["count"],
                    "actionTovar" => $row["action"],
                    "priceTovar" => $row["price"]
                ],
                [
                    $idKey => $id
                ]
            );
            return true;
        } else {
            $resValidate = $this->validate($row, $id, $temp);

            if (is_string($resValidate)) {
                return $resValidate;
            }
            $resValidate["dateSending"] = date("Y-m-d H:i:s");

            if($row["idTempImage"])
                foreach ($row["idTempImage"] as $tempImage) {
                    Core::getInstance()->getDb()->delete("temptovarsimages", ["idTempImage" => $tempImage]);
                }
            if ($temp) {
                Core::getInstance()->getDb()->update("temptovars", $resValidate, ["idTempTovar" => $id]);
                return true;
            }
            $resValidate["typeChanging"] = 1;
            $resValidate["idTovar"] = $id;
            $res2 = Core::getInstance()->getDb()->select("temptovars", "*", ["idTovar" => $id]);
            Core::getInstance()->getDb()->update("tovarsimages", ["isDel" => 0], ["idTovar" => $id]);
            if($row["idImage"])
                foreach ($row["idImage"] as $image) {
                    Core::getInstance()->getDb()->update("tovarsimages", ["isDel" => 1], ["idImage" => $image]);
                }
            if ($res2) {
                Core::getInstance()->getDb()->update("temptovars", $resValidate, ["idTovar" => $id]);
                return true;
            } elseif (empty($res2)) {
                Core::getInstance()->getDb()->insert("temptovars", $resValidate);
                return true;
            }
        }
        return false;
    }

    public function getAdminProductMail($id)
    {
        return Core::getInstance()->getDb()->select(
            "temptovars",
            "loginUser, typeChanging",
            ["idTempTovar" => $id],
            null,
            null,
            null,
            ["users"],
            ["users.idUser" => "temptovars.idUser"]
        )[0];
    }

    public function getSomeProductsByUserId($idUser, $temp = false, $count = 5, $start = 0)
    {
        $dopTemp = "";
        $table = "tovars";
        $selectArray = ["idTovar", "nameTovar", "countTovar", "priceTovar", "actionTovar"];
        if ($temp) {
            $dopTemp = "Temp";
            $table = "temptovars";
            $selectArray[] = "idTempTovar";
            $selectArray[] = "typeChanging";
        }
        return Core::getInstance()->getDb()->select(
            $table,
            $selectArray,
            [
                "idUser" => $idUser,

            ],
            "nameTovar",
            $count,
            $start,
            [
                "brands"
            ],
            [
                $table . ".idBrand" => "brands.idBrand"
            ]
        );
    }

    public function addTempImageProduct($idTovar, $typeImage)
    {

        $lastId = Core::getInstance()->getDb()->insert("temptovarsimages", [
            "idTempTovar" => $idTovar,
            "typeImage" => $typeImage
        ]);
        $href = hash("ripemd128", intval(rand(0, 999999)));
        Core::getInstance()->getDb()->update(
            "temptovarsimages",
            ["hrefImage" => $href],
            ["idTempImage" => $lastId]
        );
        return $href;
    }

    public function addImageProduct($tempImage, $idTovar)
    {
        Core::getInstance()->getDb()->insert("tovarsimages", [
            "idTovar" => $idTovar,
            "hrefImage" => $tempImage["hrefImage"],
            "typeImage" => $tempImage["typeImage"],
        ]);
        Core::getInstance()->getDb()->delete("temptovarsimages", [
            "idTempImage" => $tempImage["idTempImage"]
        ]);
    }

    public function getImagesByIdProduct($idTovar, $temp = false, $isDel=false)
    {
        if ($temp) {
            return Core::getInstance()->getDb()->select("temptovarsimages", "*", ["idTempTovar" => $idTovar]);
        }
        if($isDel) {
            return Core::getInstance()->getDb()->select("tovarsimages", "*", ["idTovar" => $idTovar, "isDel"=>0]);
        }
        return Core::getInstance()->getDb()->select("tovarsimages", "*", ["idTovar" => $idTovar]);
    }

    public function deleteImage($idImage, $temp = false)
    {
        $imgInfo = null;
        if ($temp) {
            $imgInfo = Core::getInstance()->getDb()->select("temptovarsimages", ["typeImage", "hrefImage"], ["idTempImage" => $idImage])[0];
            Core::getInstance()->getDb()->delete("temptovarsimages", ["idTempImage" => $idImage]);
        } else {
            $imgInfo = Core::getInstance()->getDb()->select("tovarsimages", ["typeImage", "hrefImage"], ["idImage" => $idImage])[0];
            Core::getInstance()->getDb()->delete("tovarsimages", ["idImage" => $idImage]);
        }
        unlink('images/tovars/' . $imgInfo["hrefImage"] . "_2" . $imgInfo["typeImage"]);
        unlink('images/tovars/' . $imgInfo["hrefImage"] . "_1" . $imgInfo["typeImage"]);
    }

    public function getSomeProductsByName($searchStr)
    {
        return Core::getInstance()->getDb()->select(
            "tovars",
            [
                "tovars.idTovar", "nameTovar", "countTovar", "priceTovar", "actionTovar"
            ],
            "nameTovar LIKE '%" . $searchStr . "%'"
        );
    }

    public function getSomeProductsByBrandName($searchStr, $count = "default", $start = 0)
    {
        if($count == "default")
            $count = self::getCount();
        return Core::getInstance()->getDb()->select(
            "tovars",
            [
                "tovars.idTovar", "nameTovar", "countTovar", "priceTovar", "actionTovar", "tovars.idBrand as brand"
            ],
            "nameBrand LIKE '%" . $searchStr . "%'",
            null,
            $count,
            $start,
            ["brands"],
            ["tovars.idBrand" => "brands.idBrand"]
        );
    }

    public function getCountProducts(){
        return Core::getInstance()->getDb()->select(
            "tovars",
            "count(idTovar) as count"
        )[0]["count"];
    }

    public function getCountProductsByIdBrand($idBrand){
        return Core::getInstance()->getDb()->select(
            "tovars",
            "count(idTovar) as count",
            ["idBrand"=>$idBrand]
        )[0]["count"];
    }
}
