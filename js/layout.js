/*Товари з кошика*/
let myModal = new bootstrap.Modal(document.querySelector('.basket-modal'), {
    keyboard: false
})
document.querySelector(".basket-modal").addEventListener("click", () => {
    myModal.hide()
})

/*Додаткові налаштування ckEditor для адаптації на сторінці*/
let editors = document.querySelectorAll('.editor');
for (let i in editors) {
    ClassicEditor
        .create(editors[i], {
            // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
        })
        .then(editor => {
            window.editor = editor;
        })
        .catch(err => {
            console.error(err.stack);
        });
}

/*Отримання та відображення даних з модуля "Кошик"*/
if (document.querySelector("#basket")) {
    document.querySelector("#basket").addEventListener("click", () => {
        document.querySelector("#loading").style.display = "block";
        fetch("/baskets/index")
            .then(res => res.json())
            .then(data => {
                console.log(data);
                myModal.show();
                document.querySelector("#basket-tovars").firstElementChild.innerHTML = "";
                document.querySelector("#basket-tovars").firstElementChild.innerHTML += GenerateTovarsStr(data["tovars"], data["user"], true);
                document.querySelector("#loading").style.display = "none";
                BasketCarousel();
            })
    })
}

/*Отримання та відображення даних локального кошика*/
if (document.querySelector("#local-basket")) {
    document.querySelector("#local-basket").addEventListener("click", () => {
        let tovarsIndex = JSON.parse(window.localStorage.getItem("basket"));
        let formData = new FormData();
        formData.append("tovarsIndex", JSON.stringify(tovarsIndex));
        document.querySelector("#loading").style.display = "block";
        fetch("/baskets/localindex", {
            method: "post",
            body: formData
        }).then(res => res.json())
            .then(data => {
                myModal.show();
                document.querySelector("#basket-tovars").firstElementChild.innerHTML = "";
                document.querySelector("#basket-tovars").firstElementChild.innerHTML += GenerateTovarsStr(data["tovars"], false, true);
                document.querySelector("#loading").style.display = "none";
                BasketCarousel();
            })
    })
}

/*Функція, що буде нормалізовувати роботу каруселі для товарів з кошика*/
function BasketCarousel() {
    let carousels = document.querySelector(".basket-modal").querySelectorAll(".carousel");

    for (let carousel of carousels) {
        if (!carousel.querySelector("li")) {
            continue;
        }
        let i = 1;
        for (let li of carousel.getElementsByTagName('li')) {
            li.style.position = 'relative';
            li.insertAdjacentHTML('beforeend', `<span style="position:absolute;left:0;top:0">${i}</span>`);
            i++;
        }
        let width = parseFloat(window.getComputedStyle(carousel.querySelector("img")).width);
        let count = 1;
        let maxCount = carousel.querySelectorAll("img").length;

        let list = carousel.querySelector('ul');
        let listElems = carousel.querySelector('li');

        let position = 0;


        setInterval(() => {
            let dopPosition = position - width * count;
            if (dopPosition < -width * (maxCount - 1)) {
                dopPosition = 0;
            }
            position = dopPosition;
            list.style.marginLeft = position + 'px';
        }, 5000);
    }
}


document.querySelector("#delete-message-button").addEventListener("click", e=>{
    document.querySelector("#message").classList.remove("message-block-show");
    document.querySelector("#message").classList.add("message-block-hide");
    setTimeout( ()=>{
        document.querySelector("#message").style.display="none";
    }, 500)
})