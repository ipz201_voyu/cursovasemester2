let startIndex = 0;
let actualTypeTovars = 0;

/*Функція для запитів на отримання товарів користувача*/
function GetMyTovars(typeTovars){
    startIndex = 0;
    actualTypeTovars = typeTovars;
    let fetchHref = document.querySelector("#my-actual-tovars").href + `?typeTovars=${typeTovars}&&startIndex=` + startIndex;
    document.cookie = `typeMyTovars=${typeTovars};`;
    document.querySelector("#my-actual-tovars").style.color ="black";
    document.querySelector("#my-changing-tovars").style.color ="black";
    if(actualTypeTovars == 0){
        document.querySelector("#my-actual-tovars").style.color = "gray";
    }else{
        document.querySelector("#my-changing-tovars").style.color = "gray";
    }
    document.querySelector("#loading").style.display="block";
    fetch(fetchHref)
        .then(res => res.json())
        .then(data => {
            if(data["error"]){
                showMessage(data);
            }else {
                let tableBody = document.querySelector("#my-tovars-table").querySelector("tbody");
                tableBody.innerHTML = "";
                WriteMyTovars(data);
                startIndex += 5;
            }
            document.querySelector("#loading").style.display="none";
            if(document.querySelector(".table").querySelector("tbody").innerHTML===""){
                document.querySelector("#undefind-tovar").style.display="block";
            }else{
                document.querySelector("#undefind-tovar").style.display="none";
            }
        })
}

/*Перехід до звичайних товарів*/
document.querySelector("#my-actual-tovars").addEventListener("click", () => GetMyTovars(0))

/*Перехід до товарів, що очікують прийняття*/
document.querySelector("#my-changing-tovars").addEventListener("click", () => GetMyTovars(1))

/*Функція, що за даними генерує таблицю товарів користувача*/
function WriteMyTovars(data, start = null) {
    let tableBody = document.querySelector("#my-tovars-table").querySelector("tbody");
    for (let i = 0; i < data.length; i++) {
        let status = ""
        if (data[i]["idTempTovar"]) {
            if (data[i]["typeChanging"] == 0) {
                status = "Очікується відповідь на запит 'додавання'!";
            } else if (data[i]["typeChanging"] == 1) {
                status = "Очікується відповідь на запит 'редагування'!";
            }
        } else {
            status = "Додано до бази";
        }

        let params = "";
        if (data[i]['idTempTovar']) {
            params = "?idTemp=" + data[i]['idTempTovar'];
        } else if (data[i]['idTovar']) {
            params = "?id=" + data[i]['idTovar'];
        }
        tableBody.innerHTML += `<tr class="mainTr">
                <th scope="row">${startIndex===0?i+1:startIndex+i}</th>
                <td>
                    ${data[i]["nameTovar"]}
                </td>
                <td>
                ${status}
                </td>
                <td>
                    <a href=${'/products/lightupdate'+params} class="btn btn-success btn-sm">
                        lu
                    </a>
                    <a href=${"/products/mainupdate"+params} class="btn btn-primary btn-sm">
                        ru
                    </a>
                    <a href=${"/products/delete"+params} onclick="return false;" class="btn btn-danger btn-sm delete-tovars">
                        de
                    </a>

                    <a href=${"/products/index"+params} class="btn btn-light btn-sm">
                        sh
                    </a>
                </td>
            </tr>`
    }
    if (data.length === 5) {
        tableBody.innerHTML += `<a id="show-more">Показати ще</a>`
        document.querySelector("#show-more").addEventListener("click", () => {
            document.querySelector("#show-more").remove();
            let fetchHref = "/products/getmyproducts?typeTovars=" + actualTypeTovars + "&&startIndex=" + startIndex;
            document.querySelector("#loading").style.display="block";
            fetch(fetchHref)
                .then(res =>  res.json())
                .then(data => {
                    if(data["error"]){
                        showMessage(data);
                    }else {
                        WriteMyTovars(data)
                    }
                    document.querySelector("#loading").style.display="none";
                })
            startIndex += 5;
        })
    }
    if (start != null) {
        startIndex = start;
    }
    if(document.querySelector(".delete-tovars")){
        let deleteButtons = document.querySelectorAll(".delete-tovars");
        deleteButtons.forEach((elem=>elem.addEventListener("click", ()=>{
            callConfirmForm("Ви справді хочете видалити цей товар?");
            console.log(elem.href)
            document.querySelector(".modal-footer").querySelector(".btn-primary").addEventListener("click", () => {
                return false;
            })
            document.querySelector(".modal-footer").querySelector("#success-send-form").addEventListener("click", () => {

                window.location = elem.href;
            })
        })))
    }
}

/*Метод для парсингу кукі*/
function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}