/*Стоврення коментаря*/
function writeComment(nameUser, href, date, commentText, idForComment, idTovar, comments = null,
    isFirst = false, isReturn = false, rating = null) {

    let res = isReturn ? `` : `<div>`;
    res += writeSubComments(nameUser, href, date, commentText, idForComment, idTovar, rating);
    if (comments != null && comments.length !== 0) {
        res += '<hr>';
        for (let i = 0; i < comments.length; i++) {
            res += writeSubComments(comments[i]["nameUser"], comments[i]["href"], comments[i]["dateAdding"],
                comments[i]["textComment"], comments[i]["idForComment"], idTovar)
            res += '</div></div>';
        }
    }
    res += '</div></br></div>';
    res += isReturn ? `` : `</div>`
    if (!isReturn) {
        if (isFirst) {
            document.querySelector("#commentsBlock").innerHTML = res + document.querySelector("#commentsBlock").innerHTML;
        } else {
            document.querySelector("#commentsBlock").innerHTML += res;
        }
    } else {
        return res;
    }
    let unswers = document.querySelectorAll(".unswer");
    for (let i = 0; i < unswers.length; i++) {
        unswers[i].removeEventListener("click", buttonToFormButton)
        unswers[i].addEventListener("click", buttonToFormButton)
    }
}

/*Генерація коду коментаря*/
function writeSubComments(nameUser, href, date, commentText, idForComment, idTovar, rating = null) {
    return `<div class="media-block comment-block">
                <a class="media-left" href="#">
                <img class="img-circle img-sm" alt="Профіль користувача"
                        src=${href != null ? "/images/users/" + href : "https://www.kindpng.com/picc/m/451-4517876_default-profile-hd-png-download.png"}></a>
                        <div class="media-body">
                            <div class="mar-btm head-comment">
                                <p class="media-heading forUserName" ${rating === null ? "style=margin-top:15px;" : ""}>${nameUser}</p>
                                ${rating === null ? "" : `
                                    <div class="stars-body">
                                        <div class="stars-active" style="width:${rating / 5 * 100 + "%"}"></div>
                                    </div>`
    }
                                <p class="text-muted text-sm"><i class="fa fa-mobile fa-lg">${date}</i></p>
                            </div>
                            <p>${commentText}</p>
                            <div class="input-group mb-3" style="width: 100%;">
                                <a href="#" class="btn btn-sm btn-default btn-hover-primary unswer" onclick="return false;">Відповісти</a>
                                <div style="display:flex;">
                                    <input type="hidden" class="idForComment" value=${idForComment}>
                                    <input type="hidden" class="idTovar" value=${idTovar}>
                                    <input type="text" style="display:none;" name="commentText" class="form-control commentText">
                                    <button type="button" style="display:none;" class="btn btn-outline-secondary btn-sm btn-default btn-hover-primary sender">Надіслати</button>
                                </div>
                            </div>`
}

let activeUnswer = -1;

let timerCloseFormComment = null;

/*Перетворення кнопки на форму*/
function buttonToFormButton(e) {
    if (activeUnswer > -1) {
        formButtonToButton(activeUnswer);
        document.querySelectorAll(".sender")[activeUnswer].removeEventListener("click", sender)
    }
    let i = -1;
    let unswers = document.querySelectorAll(".unswer");
    for (let j = 0; j < unswers.length; j++) {
        if (unswers[j] === e.target) {
            i = j;
        }
    }
    document.querySelectorAll(".unswer")
    activeUnswer = i;
    document.querySelectorAll(".unswer")[i].style.display = "none";
    document.querySelectorAll(".commentText")[i].style.display = "inline-block";
    document.querySelectorAll(".commentText")[i].focus();
    document.querySelectorAll(".commentText")[i].addEventListener("focusout", ()=>
        timerCloseFormComment = setTimeout(()=>{
            formButtonToButton(i);
            }, 1000));
    document.querySelectorAll(".commentText")[i].value = document.querySelectorAll(".forUserName")[i].innerHTML + ", ";
    document.querySelectorAll(".sender")[i].style.display = "inline-block";
    document.querySelectorAll(".sender")[i].addEventListener("click", sender)

    function sender() {
        clearTimeout(timerCloseFormComment);
        document.querySelectorAll(".sender")[i].removeEventListener("click", sender)
        let text = document.querySelectorAll(".commentText")[i].value;
        let idFor = document.querySelectorAll(".idForComment")[i].value;
        let idTovar = document.querySelectorAll(".idTovar")[i].value;
        document.querySelectorAll(".commentText")[i].value = "";
        let formData = new FormData();
        formData.append('idTovar', idTovar);
        formData.append('textComment', text);
        formData.append('idFor', idFor);
        fetch("http://cursova/comments/addtocomment", {
                method: "post",
                body: formData
            })
            .then(res => res.json())
            .then(data => {
                let actualElem = document.querySelectorAll(".comment-block")[i];
                let parentSender = actualElem.parentNode.parentNode;
                if (parentSender.classList.contains("comment-block")) {
                    parentSender.outerHTML = writeComment(data["nameUser"], data["href"], data["dateAdding"],
                        data["textComment"], data["idForComment"], idTovar, data["dopComments"], false, true, data["ratingTovar"]);
                    actualElem = parentSender;
                } else {
                    actualElem.outerHTML = writeComment(data["nameUser"], data["href"], data["dateAdding"],
                        data["textComment"], data["idForComment"], idTovar, data["dopComments"], false, true, data["ratingTovar"]);
                }
                let unswers = document.querySelectorAll(".unswer");
                for (let i = 0; i < unswers.length; i++) {
                    unswers[i].addEventListener("click", buttonToFormButton)
                }
                actualElem.querySelector(".stars-active").style.width = data["ratingTovar"] / 5 * 100 + "%";
            })
        formButtonToButton(i);
    }

}

/*Перетворення форми на кнопки*/
function formButtonToButton(i) {
    document.querySelectorAll(".sender")[i].style.display = "none";
    document.querySelectorAll(".commentText")[i].style.display = "none";
    document.querySelectorAll(".unswer")[i].style.display = "inline-block";
}

/*Написання усіх коментів*/
function writeAllComments(comments, tovarId) {
    if (comments != null) {
        for (let i = 0; i < comments.length; i++) {
            writeComment(comments[i]["nameUser"], comments[i]["href"], comments[i]["dateAdding"], comments[i]["textComment"],
                comments[i]["idForComment"], tovarId, comments[i]["dopComments"], false, false, comments[i]["ratingTovar"]);
        }
    }
}

/*Додання події на генерацію комента*/
function addCommentsAddClick(id) {
    if (document.querySelector("#addComments")) {
        document.querySelector("#addComments").addEventListener("click", () => sender());

        function sender() {
            let ratingValue = document.querySelector("input[name=ratingComment]:checked").value;
            let commentText = document.querySelector("textarea[name=textComment]").value;
            let idTovar = id;
            let formData = new FormData();
            formData.append('idTovar', idTovar);
            formData.append('textComment', commentText);
            formData.append('ratingComment', ratingValue);
            fetch("http://cursova/comments/addtotovar", {
                    method: "post",
                    body: formData
                })
                .then(res => res.json())
                .then(data => {
                    document.querySelector(".sender-tovar-comment").style.display = "none";
                    writeComment(data["nameUser"], data["href"], data["dateAdding"], data["textComment"],
                        data["idForComment"], data["idTovar"], null, true, false, data["ratingTovar"])
                    document.querySelector(".head-stars-body").querySelector(".stars-active").style.width = data["mainRating"] * 20 + '%';
                    document.querySelector(".count-comments").innerHTML = data["countComments"];
                })
        }
    }
}

window.addEventListener("keyup", (e)=>{
    if (e.key === 'Enter') {
        let actualInputs = document.querySelectorAll("input[name=commentText]");
        let actualInput = null;
        for (let elem of actualInputs) {
            if (elem.style.display !== "none") {
                actualInput = elem;
                break;
            }
        }
        if(actualInput!=null)
            actualInput.nextElementSibling.dispatchEvent(new Event("click"));

        return false;
    }
});