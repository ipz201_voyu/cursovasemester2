let actualIndexImage = 0;
let countDBImages = 0;
let actualIndexDBImage = 0;
let access = 0;
let images = null;

/*Заповнювач випадаючих списків, що відповідають за групи та бренди*/
function WriteGroupsBrands(brands, nameTovarGroup = null) {
    let groupsName = [];
    console.log(brands.length)
    for (let i = 0; i < brands.length; i++) {
        console.log(brands[i]["group"])
        if (groupsName.indexOf(brands[i]["group"]["nameTovarGroup"]) === -1) {
            groupsName.push(brands[i]["group"]["nameTovarGroup"]);
        }
    }
    for (let i = 0; i < groupsName.length; i++) {
        document.querySelector("#group").innerHTML += `<option>${groupsName[i]}</option>`;
    }
    if (nameTovarGroup != null) {
        document.querySelector("#group").value = nameTovarGroup;
    }


    document.querySelector("#group").addEventListener("change", function () {
        rewriteBrand();
    })

    rewriteBrand();

    function rewriteBrand() {
        let select = document.querySelector("#group").value;
        document.querySelector("#brand").innerHTML = "";
        for (let i = 0; i < brands.length; i++) {
            if (brands[i]["group"]["nameTovarGroup"] === select) {
                document.querySelector("#brand").innerHTML += `<option value=${brands[i]["idBrand"]}>
            ${brands[i]["nameBrand"]}</option>`
            }
        }
    }
}

/*Функція, що повертає малюнок з считаного файла*/
function previewFile(file) {
    let imgCarusel = document.createElement("img");
    let reader = new FileReader();
    reader.onloadend = function () {
        imgCarusel.src = reader.result;
        imgCarusel.onload = function () {
            if (640.0 / imgCarusel.width > 480.0 / imgCarusel.height) {
                imgCarusel.style.height = "480px";
            } else {
                imgCarusel.style.width = "640px";
            }
        };
        imgCarusel.classList.add("d-block")
        imgCarusel.alt = "Слайд каруселі"
    }
    if (file) {
        reader.readAsDataURL(file);
    } else {
        imgCarusel.src = "";
    }
    return imgCarusel;
}

/*Функція, що з малюнків утворених previewFile створює карусель до товару*/
function WriteCarousel(rewriteDBImages = false) {
    actualIndexImage = 0;
    let files = document.querySelector('#input-files').files;

    if (images != null && rewriteDBImages) {
        document.querySelector(".carousel-inner").innerHTML="";
        for (let image of images) {
            let divCarusel = document.createElement("div");
            divCarusel.classList.add("carousel-item");
            if (document.querySelector(".carousel-inner").childElementCount < 1) {
                divCarusel.classList.add("active");
            }
            let imgCarusel = document.createElement("img");
            imgCarusel.classList.add("d-block")
            imgCarusel.alt = "Слайд каруселі"
            imgCarusel.src = "/images/products/" + image["hrefImage"] + "_2" + image["typeImage"];
            divCarusel.appendChild(imgCarusel);
            document.querySelector(".carousel-inner").appendChild(divCarusel)
        }
        document.querySelector("#delete-image-fromDB").classList.remove("visually-hidden");
        document.querySelector("#delete-image").classList.add("visually-hidden");
        actualIndexDBImage = 0;
    }

    if (files.length > 0) {
        if (countDBImages == 0) {
            document.querySelector(".carousel-control-prev-icon").classList.remove("visually-hidden");
            document.querySelector(".carousel-control-next-icon").classList.remove("visually-hidden");
            document.querySelector("#delete-image").classList.remove("visually-hidden");
        }
        for (let file of files) {
            let divCarusel = document.createElement("div");
            divCarusel.classList.add("carousel-item");
            if (document.querySelector(".carousel-inner").childElementCount < 1) {
                divCarusel.classList.add("active");
            }
            divCarusel.appendChild(previewFile(file))
            document.querySelector(".carousel-inner").appendChild(divCarusel)
        }
        actualIndexImage = 0;
        if (files.length > 1) {
            document.querySelector("#carousel-control-prev").classList.remove("img-controll-hidden");
            document.querySelector("#carousel-control-next").classList.remove("img-controll-hidden");
        }
    } else {
        if (countDBImages == 0) {
            document.querySelector(".carousel-inner").innerHTML = "<img src=\"https://kebabchef.ua/images/photo_default_1_0.png\" id='dafault-image' alt=\"Default image\">";
            document.querySelector(".carousel-control-prev-icon").classList.add("visually-hidden");
            document.querySelector(".carousel-control-next-icon").classList.add("visually-hidden");
            document.querySelector("#delete-image").classList.add("visually-hidden");
        }
        document.querySelector("#carousel-control-prev").classList.add("img-controll-hidden");
        document.querySelector("#carousel-control-next").classList.add("img-controll-hidden")
    }
}

/*Функція, що реалізовує переміщення вперед по каруселі та індексам малюнків*/
function Prev() {
    if (access === 0) {
        if (document.querySelector("#delete-image-fromDB")) {
            if (document.querySelector("#delete-image-fromDB").classList.contains("visually-hidden")) {
                actualIndexImage--;
                if (actualIndexImage < 0) {
                    actualIndexDBImage = countDBImages - 1;
                    document.querySelector("#delete-image-fromDB").classList.remove("visually-hidden")
                    document.querySelector("#delete-image").classList.add("visually-hidden")
                }
            } else {
                actualIndexDBImage--;
                if (actualIndexDBImage < 0) {
                    if (document.querySelector('#input-files').files.length != 0) {
                        actualIndexImage = document.querySelector('#input-files').files.length - 1;
                        document.querySelector("#delete-image-fromDB").classList.add("visually-hidden")
                        document.querySelector("#delete-image").classList.remove("visually-hidden")
                    } else {
                        actualIndexDBImage = countDBImages - 1;
                    }
                }
            }
        } else {
            actualIndexImage--;
            if (actualIndexImage < 0) {
                actualIndexImage = document.querySelector('#input-files').files.length - 1;
            }
        }
    }
    console.log("actualIndexDBImage: " + actualIndexDBImage + " actualIndexImage: " + actualIndexImage);
}

/*Функція, що реалізовує переміщення назад по каруселі та індексам малюнків*/
function Next() {
    if (access === 0) {
        if (document.querySelector("#delete-image-fromDB")) {
            if (document.querySelector("#delete-image-fromDB").classList.contains("visually-hidden")) {
                actualIndexImage++;
                if (actualIndexImage > document.querySelector('#input-files').files.length - 1) {
                    actualIndexDBImage = 0;
                    document.querySelector("#delete-image-fromDB").classList.remove("visually-hidden")
                    document.querySelector("#delete-image").classList.add("visually-hidden")
                }
            } else {
                actualIndexDBImage++;
                if (actualIndexDBImage > countDBImages - 1) {
                    if (document.querySelector('#input-files').files.length != 0) {
                        actualIndexImage = 0;
                        document.querySelector("#delete-image-fromDB").classList.add("visually-hidden")
                        document.querySelector("#delete-image").classList.remove("visually-hidden")
                    } else {
                        actualIndexDBImage = 0;
                    }
                }
            }
        } else {
            actualIndexImage++;
            if (actualIndexImage > document.querySelector('#input-files').files.length - 1) {
                actualIndexImage = 0;
            }
        }
    }
    console.log("actualIndexDBImage: " + actualIndexDBImage + " actualIndexImage: " + actualIndexImage);
}

/*Функція яка робить затримку між переходами, щоб не виникали баги при них*/
function accessChange() {
    if (access === 0) {
        access = 1;
        setTimeout(() => {
            access = 0;
        }, 700)
    }
}

/*Перемальовка кнопок видалення малюнків, що збережені в базі даних до товарів*/
function SetDBImages(tovarImages) {
    countDBImages = tovarImages.length;
    images = tovarImages;

    document.querySelector("#delete-image-fromDB").addEventListener("click", () => {
        if (actualIndexDBImage < countDBImages) {
            let hidInput = document.createElement("input");
            hidInput.type="hidden";
            hidInput.classList.add("form-control");
            if (images[actualIndexDBImage]["idTempImage"]) {
                let inputs = document.querySelectorAll('input[name="idTempImage[]"]');
                inputs.forEach(elem=>{
                    if(elem.value == images[actualIndexDBImage]["idTempImage"]){
                        return;
                    }
                })
                hidInput.name="idTempImage[]";
                hidInput.value=`${images[actualIndexDBImage]["idTempImage"]}`;
                images = images.filter(elem=>elem["idTempImage"]!=images[actualIndexDBImage]["idTempImage"]);
            } else {
                let inputs = document.querySelectorAll('input[name="idImage[]"]');
                inputs.forEach(elem=>{
                    if(elem.value == images[actualIndexDBImage]["idImage"]){
                        return;
                    }
                })
                hidInput.name="idImage[]";
                hidInput.value=`${images[actualIndexDBImage]["idImage"]}`;
                images = images.filter(elem=>elem["idImage"]!=images[actualIndexDBImage]["idImage"]);
            }
            document.querySelector("#tovar-form").appendChild(hidInput);
            countDBImages = images.length;
            WriteCarousel(true);
            console.log(images);
        }
    });
}

/*Навішуємо подію на input, що приймає файли*/
document.getElementById("input-files").addEventListener("change", function () {
    document.querySelector("#photo-label").innerHTML = "Фото товару";
    document.querySelector("#photo-label").style.color = "black";
    if (countDBImages == 0) {
        document.querySelector(".carousel-inner").innerHTML = "";
    }
    document.querySelector(".carousel-inner").innerHTML = "";
    WriteCarousel(true);
});

document.querySelector("#carousel-control-prev").addEventListener("click", Prev);
document.querySelector("#carousel-control-prev").addEventListener("keydown", Prev);
document.querySelector("#carousel-control-next").addEventListener("keydown", Next);
document.querySelector("#carousel-control-next").addEventListener("click", Next);

document.querySelector("#carousel-control-next").addEventListener("keydown", accessChange)
document.querySelector("#carousel-control-prev").addEventListener("keydown", accessChange)
document.querySelector("#carousel-control-next").addEventListener("click", accessChange)
document.querySelector("#carousel-control-prev").addEventListener("click", accessChange)

/*Навішуємо подію на кнопку, що видаляє малюнок*/
document.querySelector("#delete-image").addEventListener("click", () => {
    function removeFileFromFileList(index) {
        const dt = new DataTransfer()
        const input = document.querySelector('#input-files')
        const {
            files
        } = input

        for (let i = 0; i < files.length; i++) {
            const file = files[i]
            if (index !== i)
                dt.items.add(file)
        }

        input.files = dt.files;
    }

    removeFileFromFileList(actualIndexImage);
    document.querySelector(".carousel-inner").innerHTML = "";
    WriteCarousel(true);
})