/*Перемальовувач body таблиці брендів для тих в кого є доступ на редагування*/
function Writer(brands, groups) {
    let resStr = ""
    let i = 0;
    for (const brand of brands) {
        i++;
        resStr +=
            `<tr>
                        <th scope="row">${i}</th>
                        <td>
                            <div class="hidden-form-div">
                                <input type="hidden" name = "idBrand" value=${brand["idBrand"]}>
                                <input type="hidden" name = "idTovarGroup" value=${brand["idTovarGroup"]}>
                                <input type="text" name = "nameBrand" value=${brand["nameBrand"]}>
                            </div>
                            <div class="open-form">${brand["nameBrand"]}</div>
                        </td>
                        <td>${brand["nameTovarGroup"]}</td>
                        <td>${brand["countTovars"]}</td>
                        <td>
                            <a class="del-href" onclick="return false; " href="/brands/delete?idBrand=${brand["idBrand"]}">
                                <svg color="black" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                </svg>
                            </a>
                        </td>
                    </tr>`
    }
    resStr += `<tr>
                    <th>
                        <button type="button" id="table-button" class="btn btn-primary" style="margin: 0">Зберегти</button>
                    </th>
                    <td>
                        <input type="text" name="nameNewBrand" placeholder="Назва бренду" required>
                    </td>
                    <td id="group-select">
                        <select name="idNewTovarGroup" id="group">`
    for (const group of groups) {

        resStr += `<option value=${group["idTovarGroup"]}>${group["nameTovarGroup"]}</option>`
    }
    resStr += `        </select>
                    </td>
                    <td></td>
                    <td><a id="reset" onclick="return false;">
                                <svg color="black" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                </svg>
                            </a></td>
                </tr>`;
    if (document.querySelector(".table")) {
        document.querySelector(".table").querySelector("tbody").innerHTML += resStr;
    } else {
        if (groups[0]) {
            document.querySelector(".table-responsive-sm").innerHTML = `Жодного бренду не додано<table class="table">${resStr}</table>`;
        }
    }

    if (groups[0]) {
        /*навішення події на кнопку Додати*/
        document.querySelector("#table-button").addEventListener("click", () => {
            if (!document.querySelector("input[name=nameBrand]") || document.querySelector("input[name=nameBrand]").value != "") {
                let formData = new FormData();
                formData.append('nameBrand', document.querySelector("input[name=nameNewBrand]").value);
                formData.append('idTovarGroup', document.querySelector("select[name=idNewTovarGroup]").value);
                document.querySelector("#loading").style.display="block";
                fetch('http://cursova/brands/add', {
                    method: "POST",
                    body: formData
                })
                    .then(res => res.json())
                    .then(data => {
                        document.querySelector(".table-responsive-sm").innerHTML =
                            `<table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Назва бренду</th>
                    <th scope="col">Назва категорії</th>
                    <th scope="col">Кількість товару</th>
                    <? if ($user["accessUser"] == 2 or $user["accessUser"] == 5): ?>
                        <th scope="col">Видалити категорію</th>
                    <?php endif; ?>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>`
                        document.querySelector(".table").querySelector("tbody").innerHTML = "";
                        Writer(data["brands"], groups);
                        showMessage(data, "Додання бренду успішно виконано!");
                        document.querySelector("#loading").style.display="none";
                    })
            } else {
                alert("error!")
            }

        })

        /*навішення події на кнопки Видалити(асинхронний запрос на видалення бренду з бд)*/
        let dels = document.querySelectorAll(".del-href");
        for (const del of dels) {
            del.addEventListener("click", () => {
                callConfirmForm("Ви справді бажаєте видалити цей бренд?(у результаті цього будуть видалені усі товари від даного бренду!)");
                document.querySelector(".modal-footer").querySelector(".btn-primary").addEventListener("click",()=>{
                    return false;
                })

                document.querySelector(".modal-footer").querySelector("#success-send-form").addEventListener("click",()=>{
                    document.querySelector("#loading").style.display="block";
                    fetch(del.href)
                        .then(res => res.json())
                        .then(data => {
                            document.querySelector(".table").querySelector("tbody").innerHTML = "";
                            Writer(data["brands"], groups)
                            showMessage(data, "Видалення бренду успішно виконано!");
                            document.querySelector("#loading").style.display="none";
                        })
                })
            })
        }

        const formsBrother = document.querySelectorAll(".open-form");

        /*Навішення події - перетворення текстового поля з певними значеннями на форми*/
        for (let i = 0; i < formsBrother.length; i++) {
            formsBrother[i].addEventListener("dblclick", () => {
                let form = formsBrother[i].parentElement.children[0];
                form.classList.remove("hidden-form-div");
                formsBrother[i].classList.add("hidden-div");
                form.querySelector("input[name=nameBrand]").focus();
            })

            /*Навішення події - запит на зміну певних значень в брендах*/
            formsBrother[i].parentElement.children[0].addEventListener("focusout", function () {
                if (document.querySelector("input[name=nameBrand]").value != "") {
                    let formData = new FormData();
                    formData.append('idBrand', formsBrother[i].parentElement.children[0].querySelector("input[name=idBrand]").value);
                    formData.append('idTovarGroup', formsBrother[i].parentElement.children[0].querySelector("input[name=idTovarGroup]").value);
                    formData.append('nameBrand', formsBrother[i].parentElement.children[0].querySelector("input[name=nameBrand]").value);
                    document.querySelector("#loading").style.display="block";
                    fetch('http://cursova/brands/update', {
                        method: "POST",
                        body: formData
                    })
                        .then(res => res.json())
                        .then(data => {
                            document.querySelector(".table").querySelector("tbody").innerHTML = "";
                            Writer(data["brands"], groups)
                            showMessage(data, "Оновлення бренду успішно виконано!");
                            document.querySelector("#loading").style.display="none";
                        })
                } else {
                    alert("error!")
                }
                //formsbrother[i].parentElement.children[0].submit();
            });
        }

        /*Подія на кнопці reset*/
        document.querySelector("#reset").addEventListener("click", () => {
            document.querySelector("input[name=nameNewBrand]").value = "";
            let str = `<select name="idNewTovarGroup" id="group">`
            for (const group of groups) {

                str += `<option value=${group["idTovarGroup"]}>${group["nameTovarGroup"]}</option>`
            }
            str += `        </select>`
            document.querySelector("#groupSelect").innerHTML = str;
        })
    }
}

/*Перемальовувач body таблиці брендів для тих в кого нема цього доступу */
function WriterLight(brands) {
    let resStr = ""
    let i = 0;
    for (const brand of brands) {
        i++;
        resStr +=
            `<tr>
                        <th scope="row">${i}</th>
                        <td>
                            <div class="hidden-form-div">
                                <input type="hidden" name = "idBrand" value=${brand["idBrand"]}>
                                <input type="hidden" name = "idTovarGroup" value=${brand["idTovarGroup"]}>
                                <input type="text" name = "nameBrand" value=${brand["nameBrand"]}>
                            </div>
                            <div class="open-form">${brand["nameBrand"]}</div>
                        </td>
                        <td>${brand["nameTovarGroup"]}</td>
                        <td>${brand["countTovars"]}</td>
                    </tr>`
    }
    document.querySelector(".table").querySelector("tbody").innerHTML += resStr;
}