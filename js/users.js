/*зміна видимості кнопки видалення фотографії*/
function changeVisibilityDeleteImage() {
    if (document.querySelector("#profile-img").src !== "https://www.kindpng.com/picc/m/451-4517876_default-profile-hd-png-download.png")
        document.querySelector("#delete-image").classList.remove("visually-hidden");
    else
        document.querySelector("#delete-image").classList.add("visually-hidden");
}

/*Обробка считаної фотографії(аватара) та відображення її вмісту у відповідному полі*/
function previewFile() {
    let preview = document.querySelector('#profile-img');
    let file = document.querySelector('#photo-account').files[0];
    let reader = new FileReader();

    reader.onloadend = function () {
        preview.src = reader.result;
        preview.style.height = "450px";
        changeVisibilityDeleteImage()
    }

    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "https://www.kindpng.com/picc/m/451-4517876_default-profile-hd-png-download.png";
    }
}


document.querySelector("#photo-account").addEventListener("change", previewFile);

if (document.querySelector("#delete-user")) {
    document.querySelector("#delete-user").addEventListener("click", () => {
        callConfirmForm("Ви справді бажаєте видалити свій профіль?");

        document.querySelector(".modal-footer").querySelector(".btn-primary").addEventListener("click", () => {
            return false;
        })

        document.querySelector(".modal-footer").querySelector("#success-send-form").addEventListener("click", () => {
            window.location = document.querySelector("#delete-user").href;
        })
    })
}

document.querySelector("#delete-image").addEventListener("click", () => {
    if (document.querySelector("#photo-account").files.length !== 0 &&
        !document.querySelector("#delete-image").classList.contains("visually-hidden")) {

        document.querySelector("#photo-account").value = null;
        document.querySelector("#profile-img").src = "https://www.kindpng.com/picc/m/451-4517876_default-profile-hd-png-download.png";
        changeVisibilityDeleteImage();

    } else if (document.querySelector("#photo-account").files.length === 0 &&
        !document.querySelector("#delete-image").classList.contains("visually-hidden")) {
        callConfirmForm("Ви справді бажаєте видалити цю фотографію зі свого профілю?(Ця дія видалить фотографію без можливості скасувати дію!)");

        document.querySelector(".modal-footer").querySelector(".btn-primary").addEventListener("click", () => {
            return false;
        })

        document.querySelector(".modal-footer").querySelector("#success-send-form").addEventListener("click", () => {
            document.querySelector("#loading").style.display = "block";
            fetch("/users/deleteimage?hrefImage=" + document.querySelector("#profile-img").src.split("/")[5])
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    document.querySelector("#profile-img").src = "https://www.kindpng.com/picc/m/451-4517876_default-profile-hd-png-download.png";
                    document.querySelector("#profile-img-min").src = "https://www.kindpng.com/picc/m/451-4517876_default-profile-hd-png-download.png";
                    changeVisibilityDeleteImage();
                    document.querySelector("#loading").style.display = "none";
                })

        })
    }
});

changeVisibilityDeleteImage();