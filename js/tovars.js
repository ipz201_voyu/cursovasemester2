const normalCountTovars = 1;
let actualIndex = 0;
let brandId = null;
let actualFilterParams = null;

const setBrandId = id => brandId = id;

/*Головна фугкція для роботи з товарами*/
function MainTovarsFunction() {
    /*Додання коректної роботи каруселі до товарів*/
    function CarouselWork(onClick = false) {
        let carousels = document.querySelectorAll(".carousel");

        for (let carousel of carousels) {
            if (!carousel.querySelector("li")) {
                continue;
            }
            let i = 1;
            for (let li of carousel.getElementsByTagName('li')) {
                li.style.position = 'relative';
                li.insertAdjacentHTML('beforeend', `<span style="position:absolute;left:0;top:0">${i}</span>`);
                i++;
            }
            let width = parseFloat(window.getComputedStyle(carousel.querySelector("img")).width);
            let count = 1;
            let maxCount = carousel.querySelectorAll("img").length;

            let list = carousel.querySelector('ul');
            let listElems = carousel.querySelector('li');

            let position = 0;

            if (onClick) {
                carousel.querySelector('.prev').addEventListener("click", function () {

                    let dopPosition = position + width * count;
                    if (dopPosition > 0) {
                        dopPosition = -width * (maxCount - 1);
                    }
                    position = dopPosition;
                    list.style.marginLeft = position + 'px';
                })

                carousel.querySelector('.next').addEventListener("click", function () {
                    let dopPosition = position - width * count;
                    if (dopPosition < -width * (maxCount - 1)) {
                        dopPosition = 0;
                    }
                    position = dopPosition;
                    list.style.marginLeft = position + 'px';
                })
            } else {
                setInterval(() => {
                    let dopPosition = position - width * count;
                    if (dopPosition < -width * (maxCount - 1)) {
                        dopPosition = 0;
                    }
                    position = dopPosition;
                    list.style.marginLeft = position + 'px';
                }, 5000);
            }
        }
    }

    /*Додання функціоналу на кнопку кошик*/
    let baskets = document.querySelectorAll(".baskets");

    for (let i = 0; i < baskets.length; i++)
        baskets[i].addEventListener("click", basketAdder)

    let basketsChecked = document.querySelectorAll(".baskets-checked");

    for (let i = 0; i < basketsChecked.length; i++)
        basketsChecked[i].addEventListener("click", basketRemover);


    /*Додання функціоналу на кнопку локального кошику*/
    let localBaskets = document.querySelectorAll(".local-baskets")
    if (localBaskets.length > 0) {
        for (let i = 0; i < localBaskets.length; i++) {
            let basketTovar = localBaskets[i].querySelector("input").value;
            let baskets = JSON.parse(window.localStorage.getItem("basket"))
            if (baskets !== null) {
                for (let j = 0; j < baskets.length; j++) {
                    if (basketTovar == baskets[j]) {
                        localBaskets[i].classList.add("checked");
                        break;
                    }
                }
            }
            localBaskets[i].addEventListener("click", localBasketChanger)
        }
    }

    CarouselWork();
}

function GenerateShowMoreTovars() {
    let str = `<div id="show-more-block" class="tovar-block col-xl-2 col-lg-3 col-md-4 col-sm-4 col-xs-6">
            <div id="show-more-arrow">
                &#11119;
            </div>
            <div id="show-more-tovar">
                Показати ще
            </div>
    </div>`;
    return str
}

function ShowMoreFunction(actualFilterParams) {

    document.querySelector("#show-more-block ").addEventListener("click", () => {
        const groups = document.querySelector("#groups");
        const checkedGroup = groups.querySelector(".group-checked");
        const checkedId = checkedGroup?checkedGroup.querySelector("a").href.split("=")[1]:null;
        let formData = new FormData();

        if (document.querySelector("#main-search-input").value != "") {
            formData.append("brandId", document.querySelector("#main-search-input").value);
        } else {
            if (document.querySelector("#characters") && actualFilterParams != null) {
                console.log(actualFilterParams.get("idTovarGroup"));
                formData = actualFilterParams;
            }
        }

        formData.append("actualIndex", actualIndex);
        if(checkedId)
            formData.append("idTovarGroup", checkedId);

        document.querySelector("#loading").style.display = "block";

        fetch("/site/gettovarsbyrules", {
            method: "POST",
            body: formData
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                document.querySelector("#show-more-block ").remove();
                WriteTovars(data["tovars"], data["user"]);
                document.querySelector("#loading").style.display = "none";
            })
    })
}

/*вимальовування товарів*/
function WriteTovars(tovars, user = false, reWrite = false) {
    if (reWrite) {
        document.querySelector("#tovars").firstElementChild.innerHTML = "";
    }
    if(!reWrite) {
        let tovarStr = GenerateTovarsStr(tovars, user);
        document.querySelector("#tovars").firstElementChild.innerHTML +=
            tovarStr===`<div style="margin-top:50px; display:flex;justify-content: center; align-items: center;">
<h2 style="text-align: center">Товарів не знайдено!</h2></div>`?"":tovarStr;
    }else{
        document.querySelector("#tovars").firstElementChild.innerHTML += GenerateTovarsStr(tovars, user);
        actualIndex=0;
    }
    if (tovars.length == normalCountTovars) {
        document.querySelector("#tovars").firstElementChild.innerHTML += GenerateShowMoreTovars();
        actualIndex += normalCountTovars;
        ShowMoreFunction(actualFilterParams);
    }
    MainTovarsFunction();
}
