//id користувача
let globalUserId = -1;
//Кількість юзерів, яких потрібно повернути запитом
let maxCountUsers = 20;
//Значення за яким будуть висвітлені імена або логіни юзерів
let searchValue = "";
//початковий порядковий номер юзерів, котрі будуть повернені з бази даних
let startIndex = 0;


/*Перепис дії кнопки пошуку на клік*/
function rewriterSearcher(e) {
    searchValue = document.querySelector("input[name=searcherUser]").value;
    startIndex = 0;
    let formData = new FormData();
    formData.set("where", searchValue);
    formData.set("start", startIndex);
    fetch("/users/controll", {
            method: "post",
            body: formData
        }).then(res => res.json())
        .then(data => {
            writeTbody(data["users"], globalUserId, true);
            if(document.querySelector(".table-responsive-sm").querySelector("#view-more"))
                document.querySelector(".table-responsive-sm").querySelector("#view-more").remove()
        })
}

/*Переведення рівня доступу у текст, щоб було зрозуміло які привілегії у користувача*/
function accessNameByAccess(access) {
    let accessName = "Помилковий доступ!";
    switch (access) {
        case "0":
            accessName = "Користувач";
            break;
        case "1":
            accessName = "Продавець";
            break;
        case "2":
            accessName = "Товарний адмін";
            break;
        case "3":
            accessName = "Користувацький адмін";
            break;
        case "4":
            accessName = "Помічник директора";
            break;
        case "5":
            accessName = "Директор";
            break;
    }
    return accessName;
}

/*Перепис основної частини таблиці за допомогою отриманих даних та id користувача,
 (isReWrite потрібний для того, щоб визначити режим роботи з елементом)*/
function writeTbody(data, userId, isReWrite = true) {
    globalUserId = userId;
    if (isReWrite) {
        document.querySelector("tbody").innerHTML = "";
    }

    for (let i = 0; i < data.length; i++) {
        let accessName = accessNameByAccess(data[i]["accessUser"]);
        document.querySelector("tbody").innerHTML += `
                <th scope="col">${startIndex + i + 1}</th>
                <td>${data[i]["nameUser"]}</td>
                <td>${data[i]["loginUser"]}</td>
                <td>
                    <div class="${userId !== data[i]["idUser"] ? "changerAccess" : ""}">${data[i]["accessUser"]}</div>
                    ${userId !== data[i]["idUser"] ?
            `<div class="formAccess" style="display:none;">
                            <input type="hidden" name="idUser" value="${data[i]["idUser"]}">
                            <input type="number" name="accessUser" value="${data[i]["accessUser"]}" min="0" max="4">
                        </div>` :
            ""}
                </td>
                <td>${accessName}</td>
                `
    }
    if (data.length === maxCountUsers) {
        document.querySelector(".table-responsive-sm").innerHTML += `<a id="view-more" href="#">Показати ще</a>`;
        startIndex += maxCountUsers;
        document.querySelector("#view-more").addEventListener("click", () => {
            let formData = new FormData();
            formData.set("where", searchValue);
            formData.set("start", startIndex);
            fetch("/users/controll", {
                    method: "post",
                    body: formData
                }).then(res => res.json())
                .then(data => {
                    console.log(data["users"])
                    writeTbody(data["users"], globalUserId, false);
                    if(document.querySelector(".table-responsive-sm").querySelector("#view-more"))
                        document.querySelector(".table-responsive-sm").querySelector("#view-more").remove();
                })
        })
    }

    document.querySelector("#search-users-button").removeEventListener("click", rewriterSearcher)
    document.querySelector("#search-users-button").addEventListener("click", rewriterSearcher)
    let accesses = document.querySelectorAll(".changerAccess");
    for (let i = 0; i < accesses.length; i++) {
        accesses[i].addEventListener("click", () => {
            accesses[i].style.display = "none";
            accesses[i].nextElementSibling.style.display = "block";
            let input = accesses[i].nextElementSibling.lastElementChild;
            input.focus()

            function focusouter(e) {
                let formData = new FormData();
                formData.set("idUser", input.previousElementSibling.value);
                formData.set("accessUser", input.value);
                fetch("/users/updateaccess", {
                        method: "post",
                        body: formData
                    }).then(res => res.json())
                    .then(data => {
                        console.log(data)
                        if (data["error"]) {
                            alert(data["error"])
                            input.value = input.parentElement.previousElementSibling.innerHTML;
                        } else {
                            input.parentElement.previousElementSibling.innerHTML = data["accessUser"];
                            input.value = data["accessUser"];
                            input.parentElement.parentElement.nextElementSibling.innerHTML = accessNameByAccess(data["accessUser"]);
                        }
                    })
                accesses[i].style.display = "block";
                accesses[i].nextElementSibling.style.display = "none";
                input.removeEventListener("focusout", focusouter)
            }

            input.addEventListener("focusout", focusouter)


        })
    }
}