/*Функція що генерує повідомлення для користувача*/
function showMessage(data, message=null){
    document.querySelector("#message").classList.add("message-block-show");
    document.querySelector("#message").classList.remove("message-block-hide");
    document.querySelector("#message").style.display="block";
    if(data["error"]!=null){
        document.querySelector("#message").classList.add("alert-danger");
        document.querySelector("#message-content").innerHTML=data["error"];
    }else{
        document.querySelector("#message").classList.add("alert-success");
        document.querySelector("#message-content").innerHTML=message;
    }
}