/*Функція, що додає товар до кошика*/
function basketAdder(e) {
    let idTovar = e.target.querySelector("input[name=idTovar]").value;
    let idUser = e.target.querySelector("input[name=idUser]").value;
    let formData = new FormData();
    formData.set("idTovar", idTovar);
    formData.set("idUser", idUser);
    fetch("/baskets/add", {
        method: "post",
        body: formData
    }).then(res => res.json())
        .then(data => {
            e.target.classList.remove("baskets");
            e.target.classList.add("baskets-checked");
            e.target.innerHTML = `<input type="hidden" name="idBasket" value="${data}">`
            let newElem = e.target.cloneNode(1);
            newElem.addEventListener("click", basketRemover);
            e.target = e.target.parentNode.replaceChild(newElem, e.target);
        })
}

/*Функція, що видаляє товар з кошика*/
function basketRemover(e) {
    let idBasket = e.target.querySelector("input[name=idBasket]").value;
    let formData = new FormData();
    formData.set("idBasket", idBasket);
    fetch("/baskets/delete", {
        method: "post",
        body: formData
    }).then(res => res.json())
        .then(data => {
            e.target.innerHTML = `<input type="hidden" name="idTovar" value="${data["idTovar"]}">`
            e.target.innerHTML += `<input type="hidden" name="idUser" value="${data["idUser"]}">`
            e.target.classList.add("baskets");
            e.target.classList.remove("baskets-checked");
            let newElem = e.target.cloneNode(1);
            newElem.addEventListener("click", basketAdder);
            e.target = e.target.parentNode.replaceChild(newElem, e.target);
        })
}

/*Функція, що додає товар до кошика, якщо він ще не збережений у локальній корзині, або видаляє його за іншої умови*/
function localBasketChanger(e) {
    let idTovar = e.target.querySelector("input[name=idTovar]").value;
    let localBackets = JSON.parse(window.localStorage.getItem("basket"));
    if (localBackets == null) {
        localBackets = [];
    }
    if (e.target.classList.contains("checked")) {
        localBackets = localBackets.filter(function (elem) {
            return elem !== idTovar
        })
        e.target.classList.remove("checked");
    } else {
        e.target.classList.add("checked");
        localBackets.push(idTovar);
    }
    window.localStorage.setItem("basket", JSON.stringify(localBackets));
}