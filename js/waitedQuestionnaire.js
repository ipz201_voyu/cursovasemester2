function writeTbody(data) {
    for (let i = 0; i < data.length; i++) {
        document.querySelector("tbody").innerHTML += `
                <tr style="cursor: pointer" onclick="window.location = '/questionnaire/index?id=${data[i]["idQuestionnaire"]}'">
                        <th scope="col">${i + 1}</th>
                        <td>${data[i]["nameTovar"]}</td>
                        <td>${data[i]["wantedCount"]}</td>
                        <td>${data[i]["price"]}</td>
                </tr>`
    }
    if(data.length===0){
        document.querySelector("#my-products-table").insertAdjacentHTML("afterend",
            "<div style='display:flex;justify-content: center; font-weight: bold;'>Замовлень немає!</div>"
            )
    }
}


function setFunctional(storeCount) {
    document.querySelector("#unaccept").addEventListener("click", () =>
        callConfirmForm("Ви справді не візьметесь за виконання даного замовлення?"));
    document.querySelector(".modal-footer").querySelector(".btn-primary").addEventListener("click", () => {
        return false;
    })

    document.querySelector(".modal-footer").querySelector("#success-send-form").addEventListener("click", () => {
        if (!document.querySelector("#unaccept-message").value)
            showMessage({"error": `Введіть причину відхилення замовлення!`});
        else
            window.location = document.querySelector("#unaccept").href + "&message=" + document.querySelector("#unaccept-message").value;
    });

    document.querySelector("#accept").addEventListener("click", () => {

        let wantedCount = Number(document.querySelector("#count-wanted").innerHTML.split(":")[1]);
        if (wantedCount > storeCount) {
            showMessage({
                "error": `Покупець хоче купити - ${wantedCount}, а на складі є лише ${storeCount},
             що є недопустимим(для виконання замовлення збільшіть кількість товару на складі!)`
            });
        } else {
            window.location = document.querySelector("#accept").href;
        }
    });
}