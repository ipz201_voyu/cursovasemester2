/*генерація рядків з товарами*/
function GenerateTovarsStr(tovars, user = false, ignoreBaskets = false) {
    let tovarStr = "";
    tovars.forEach(tovar => {
        tovarStr += `
        <div class="tovar-block col-xl-2 col-lg-3 col-md-4 col-sm-4 col-xs-6">

            <!--Верхня частина товару(над назвою)-->
            <div class="images-tovar">
             <!--Кошик товару(відображає чи збережений він чи його можна додати)-->`
        if (!ignoreBaskets) {
            if (user) {
                if (tovar["idBasket"] == null) {
                    tovarStr += `
                    <div class="baskets">
                        <input type="hidden" name="idTovar" value="${tovar["idTovar"]}">
                        <input type="hidden" name="idUser" value="${user["idUser"]}">
                    </div>`
                } else {
                    tovarStr += `<div class="baskets-checked">
                                <input type="hidden" name="idBasket" value="${tovar["idBasket"]}">
                            </div>`
                }
            } else {
                tovarStr += `<div class="local-baskets">
                            <input type="hidden" name="idTovar" value="${tovar["idTovar"]}">
                        </div> `
            }
        }

        if (tovar["actionTovar"] != 0) {
            tovarStr += `<!--Знижка товару-->
                        <div class="znizhka">
                            Акція!
                        </div>`
        }

        tovarStr += `<!--Зображення товару(карусель зображень)-->
                    <div class="dop-carousel-div">
                        <div class="carousel">
                            <div class="gallery">
                                <ul class="images">`

        if (tovar["images"].length > 0) {
            tovar["images"].forEach(image => {
                tovarStr += `<li>
                                <img src="/images/products/${image["hrefImage"]}_1${image["typeImage"]}" alt="default">
                            </li>`
            })
        } else {
            tovarStr += `<li>
                            <img src="https://kebabchef.ua/images/photo_default_1_0.png" alt="default">
                        </li>`
        }

        tovarStr += `</ul>
                </div>
            </div>
        </div>
    </div>

    <!--Назва товару-->
    <a class="href-h5" href="/products/index?id=${tovar["idTovar"]}">
        <h5>
            ${tovar["nameTovar"]}
        </h5>
    </a>

    <!--Рейтинг товару-->
    <div class="rating">
        <div class="stars-body">
            <div class="stars-active" style="width:${tovar["rating"] ? (Number(tovar["rating"]) * 20) + '%' : "100%"}"></div>
        </div>
    </div>`
        tovarStr += `<!--Ціна товару--><div>`;
        if (tovar["actionTovar"] != 0) {
            tovarStr += `
            <h6 class="text-muted old-price">
                ${tovar["priceTovar"]} &#8372;
            </h6>
            <h3 class="text-danger">
                ${Number(tovar["priceTovar"]) * Number(100 - tovar["actionTovar"]) / 100}
                &#8372;
            </h3>`
        } else {
            tovarStr += `
            <br>
            <h3 class="text-black">
                ${tovar["priceTovar"]} &#8372;
            </h3>`
        }
        tovarStr += `</div></div>`
    })
    if (tovarStr==="") {
        tovarStr = `<div style="margin-top:50px; display:flex;justify-content: center; align-items: center;">
<h2 style="text-align: center">Товарів не знайдено!</h2></div>`;
    }
    return tovarStr;
}
