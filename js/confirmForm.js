let confirmForm = new bootstrap.Modal(document.querySelector('#confirm-form'), {
    keyboard: false
})

document.querySelector("#confirm-form").addEventListener("click",()=>{
    confirmForm.hide()
})

/*Функція, що викликатиме форму з підтвердженням дії*/
function callConfirmForm(text, btnStyle=0){
    console.log("hjk");
    document.querySelector(".modal-body").innerHTML = text;
    if (btnStyle==1){
        document.querySelector("#success-send-form").classList.remove("btn-danger");
        document.querySelector("#success-send-form").classList.add("btn-success");
    }else{
        document.querySelector("#success-send-form").classList.remove("btn-success");
        document.querySelector("#success-send-form").classList.add("btn-danger");
    }
    confirmForm.show();
}