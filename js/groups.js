/*Перемальовувач категорій товарів*/
function ReWriterGroups(data) {
    document.querySelector("#groups").innerHTML = "<br>";
    for (let j = 0; j < data.length; j++) {
        let elem = data[j];
        document.querySelector("#groups").innerHTML +=
            `<div class="d-flex">
                    <div class="delete-group" style="margin-top: 8px">
                        <a class="href-delete-group" onclick="return false" href='/groups/delete?idTovarGroup=${elem["idTovarGroup"]}'>
                            <svg color="red" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                            </svg>
                        </a>
                    </div>
                    <div class="update-group">
                        <a href = "/site/index/?idTovarGroup=${elem['idTovarGroup']}" onclick="return false;" class="catalogs btn ">${elem['nameTovarGroup']}</a>
                        <input type="hidden" name = "idTovarGroup" value=${elem["idTovarGroup"]} />
                        <input type="text" name="nameTovarGroup" class="update-name-group hidden-input" value=${elem["nameTovarGroup"]} />
                    </div>
                </div>`
    }
    document.querySelector("#groups").innerHTML += `
        <div class="add-group">
            <input type="text" class="hidden-input" id="add-group-name-input" name = "nameTovarGroup">
            <a onclick="return false;" class="link-primary" id="add-group">Додати категорію товарів</a>
        </div>`
    AddFunctional();
}

/*Додання функціоналу до форми, яка з'явиться при редагуванні групи*/
function AddFunctional() {
    /*Додання функціоналу до кнопок видалення*/
    let deleteGroups = document.querySelectorAll(".href-delete-group");
    for (let i = 0; i < deleteGroups.length; i++) {
        deleteGroups[i].addEventListener("click", () => {
            callConfirmForm("Ви справді бажаєте видалити цю категорію товарів?(у результаті цього будуть видалені усі бренди та товари даної категорії!)");
            document.querySelector(".modal-footer").querySelector(".btn-primary").addEventListener("click", () => {
                return false;
            })
            document.querySelector(".modal-footer").querySelector("#success-send-form").addEventListener("click", () => {
                document.querySelector("#loading").style.display = "block";
                fetch(deleteGroups[i].href)
                    .then(res => res.json())
                    .then(data => {
                        showMessage(data, "Видалення назви групи товарів успішно виконано!");
                        if (data["groups"]) ReWriterGroups(data["groups"]);
                        document.querySelector("#loading").style.display = "none";
                    })
            })
        })
    }

    /*Додання функціоналу до кнопки додавання*/
    document.querySelector("#add-group").addEventListener("click", function () {
        document.querySelector("#add-group-name-input").classList.remove("hidden-input");
        document.querySelector("#add-group-name-input").focus();
        document.querySelector("#add-group").classList.add("hidden-href");
    });
    document.querySelector("#add-group-name-input").addEventListener("focusout", function () {
        let formData = new FormData();
        formData.append('nameTovarGroup', document.querySelector("#add-group-name-input").value);
        document.querySelector("#loading").style.display = "block";
        fetch("/groups/add", {
            method: 'POST',
            body: formData
        }).then(res => res.json())
            .then(data => {
                showMessage(data, "Додання назви групи товарів успішно виконано!");
                if (data["groups"]) ReWriterGroups(data["groups"]);
                document.querySelector("#loading").style.display = "none";
                document.querySelector("#add-group-name-input").classList.add("hidden-input");
                document.querySelector("#add-group").classList.remove("hidden-href");
            })
    });

    /*Додання функціоналу до кнопок оновлення*/
    let clickCountArr = [];
    let timeoutID = 0;
    let actualFormParentId = -1;
    let groups = document.querySelectorAll(".update-name-group");
    let groupsId = document.querySelectorAll("input[name=idTovarGroup]")
    const catalogs = document.querySelectorAll(".catalogs");


    for (let i = 0; i < catalogs.length; i++) {
        catalogs[i].addEventListener("click", () => {
            OnClick(i);
        })
        groups[i].addEventListener("focusout", function () {
            if (actualFormParentId !== -1) {
                let formData = new FormData();
                formData.append('nameTovarGroup', groups[i].value);
                formData.append('idTovarGroup', groupsId[i].value);
                document.querySelector("#loading").style.display = "block";
                fetch("/groups/update", {
                    method: 'POST',
                    body: formData
                }).then(res => res.json())
                    .then(data => {
                        showMessage(data, "Оновлення назви групи товарів успішно виконано!");
                        if (data["groups"]) ReWriterGroups(data["groups"]);
                        document.querySelector("#loading").style.display = "none";
                        InputToHref(i);
                    })
            }
        });
        clickCountArr[i] = 0;
    }

    /*Додання функціоналу до натискання на назву групи*/
    function OnClick(i) {
        clickCountArr[i]++;
        if (clickCountArr[i] >= 2) {
            clickCountArr[i] = 0;
            clearTimeout(timeoutID);
            HrefToInput(i);
            actualFormParentId = i;
        } else if (clickCountArr[i] === 1) {
            let callBack = function () {
                if (clickCountArr[i] === 1) {
                    clickCountArr[i] = 0;
                    if (document.querySelector(".group-checked") && !catalogs[i].parentElement.parentElement.classList.contains("group-checked")) {
                        document.querySelector(".group-checked").classList.remove("group-checked");
                    }
                    if (catalogs[i].parentElement.parentElement.classList.contains("group-checked")) {
                        document.querySelector("#loading").style.display = "block";
                        fetch("/site/gettovarsbyrules")
                            .then(res => res.json())
                            .then(data => {
                                WriteTovars(data["tovars"], data["user"], true);
                                if (document.querySelector("#characters"))
                                    document.querySelector("#characters").remove();
                                catalogs[i].parentElement.parentElement.classList.remove("group-checked");
                                actualFilterParams = null;
                                document.querySelector("#loading").style.display = "none";
                                setBrandId(null);
                            })
                    } else {
                        document.querySelector("#loading").style.display = "block";
                        fetch("/site/gettovarsbyrules?idTovarGroup=" + catalogs[i].href.split("?")[1].split("=")[1])
                            .then(res => res.json())
                            .then(data => {
                                if (document.querySelector("#characters"))
                                    document.querySelector("#characters").remove();
                                ReWriterCharacters(data["rules"]);
                                WriteTovars(data["tovars"], data["user"], true);
                                catalogs[i].parentElement.parentElement.classList.add("group-checked");
                                document.querySelector("#loading").style.display = "none";
                                setBrandId(null);
                            })
                        //window.location = catalogs[i].href;
                    }
                }
            };
            timeoutID = setTimeout(callBack, 200);
        }
    }

    /*Переведення тексту в форму*/
    function HrefToInput(i) {
        let parent = catalogs[i].parentElement;
        parent.querySelector("input[name=nameTovarGroup]").classList.remove("hidden-input")
        parent.querySelector("input[name=nameTovarGroup]").focus();
        parent.children[0].classList.add("hidden-href");
    }

    /*Переведення форми в текст*/
    function InputToHref(i) {
        let parent = catalogs[i].parentElement;
        parent.querySelector("input[name=nameTovarGroup]").classList.add("hidden-input")
        parent.children[0].classList.remove("hidden-href");
    }
}

/*Перепис критерій для пошуку товарів*/
function ReWriterCharacters(data) {
    let charactersDiv = document.createElement("div");
    charactersDiv.id = "characters";
    let brandsStr = "";
    Object.entries(data["brands"]).forEach(elem => {
        brandsStr += `<div class="form-check" style="margin-left: 12px;">
            <input class="form-check-input" type="checkbox" name="charactersBrands[]" value="${elem[1]["idBrand"]}">
                <label class="form-check-label">
                    ${elem[1]["nameBrand"]}
                </label>
        </div>`
    })
    const diapazon = data["min"] !== data["max"] ? `
<div class="range-slider">
    <span id="range-values"> Ціна: <input type="number" value=${parseFloat((data["max"] - data["min"]) / 4 + data["min"]).toFixed(2)}
     min=${data["min"]} max=${data["max"]} id="minPrice" step="0.5"> - <input type="number"
      value=${parseFloat((data["max"] - data["min"]) / 2 + data["min"]).toFixed(2)} min=${data["min"]} max=${data["max"]}
       step="0.5" id="maxPrice"> </span>
    <input value=${(data["max"] - data["min"]) / 4 + data["min"]} min=${data["min"]} max=${data["max"]} step="0.5" type="range">
    <input value=${(data["max"] - data["min"]) / 2 + data["min"]} min=${data["min"]} max=${data["max"]} step="0.5" type="range">
</div>` : "";
    const action = `<div class="form-check" style="margin-left: 12px;">
            <input class="form-check-input" type="checkbox" name="actionCharacter">
                <label class="form-check-label">Знижка</label>
        </div>`;
    const button = `<div class="form-group" style="display:flex; margin:5px; justify-content: center">
                <button class="btn btn-success" id="filter-tovars">Знайти</button>
           </div>`;
    const dopLine = `<div class="form-group" style="display:flex; justify-content: center">
                        <hr align="center" width="90%" size="3" color="lightgray" />
                        </div>`;
    if (data["brands"].length !== 0) {
        charactersDiv.innerHTML = dopLine + brandsStr + diapazon + action + button;
        document.querySelector("#groups").appendChild(charactersDiv);
        PriceWorker();
        AddToFilterEvent();
    }
}

/*Додання події(оновлення) при кліку на кнопку пошуку в фільтрі*/
function AddToFilterEvent() {
    document.querySelector("#filter-tovars").addEventListener("click", () => {
        let idTovarGroup="";

        if(document.querySelector(".group-checked").querySelector(".update-group"))
            idTovarGroup = document.querySelector(".group-checked").querySelector(".update-group").querySelector("a")
                .href.split("?")[1].split("=")[1];
        else
            idTovarGroup = document.querySelector(".group-checked").querySelector("a").href.split("?")[1]
                .split("=")[1];

        let max = document.querySelector("#maxPrice")?document.querySelector("#maxPrice").value:null;
        let min = document.querySelector("#minPrice")?document.querySelector("#minPrice").value:null;
        let brandsDiv = document.querySelectorAll(`input[name="charactersBrands[]"]`);
        let brandsValue = [];
        let action = document.querySelector("input[name = actionCharacter]").checked;
        brandsDiv.forEach(elem => {
            if (elem.checked)
                brandsValue.push(elem.value);
        })
        let formData = new FormData();
        formData.append('idTovarGroup', idTovarGroup);
        formData.append('min', min);
        formData.append('max', max);
        brandsValue.forEach(elem => formData.append('brands[]', elem))
        formData.append('action', action);
        actualFilterParams = formData;
        document.querySelector("#loading").style.display = "block";
        fetch("/site/gettovarsbyrules", {
            method: "POST",
            body: formData
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                WriteTovars(data["tovars"], data["user"], true);
                document.querySelector("#loading").style.display = "none";
                setBrandId(null);
            })
    })
}

if (document.querySelector((".normal-catalogs"))) {
    let catalogs = document.querySelectorAll(".normal-catalogs");
    catalogs.forEach(catalog => {
        catalog.addEventListener("click", () => {
            if (document.querySelector(".group-checked") && catalog!==document.querySelector(".group-checked").firstElementChild) {
                document.querySelector(".group-checked").classList.remove("group-checked");
            }
                if (catalog.parentElement.classList.contains("group-checked")) {
                    document.querySelector("#loading").style.display = "block";
                    fetch("/site/gettovarsbyrules")
                        .then(res => res.json())
                        .then(data => {
                            WriteTovars(data["tovars"], data["user"], true);
                            if (document.querySelector("#characters"))
                                document.querySelector("#characters").remove();
                            catalog.parentElement.classList.remove("group-checked");
                            setBrandId(null);
                            document.querySelector("#loading").style.display = "none";
                        })
                } else {
                    document.querySelector("#loading").style.display = "block";
                    fetch("/site/gettovarsbyrules?idTovarGroup=" + catalog.href.split("?")[1].split("=")[1])
                        .then(res => res.json())
                        .then(data => {
                            if (document.querySelector("#characters"))
                                document.querySelector("#characters").remove();
                            ReWriterCharacters(data["rules"]);
                            WriteTovars(data["tovars"], data["user"], true);
                            catalog.parentElement.classList.add("group-checked");
                            document.querySelector("#loading").style.display = "none";
                            setBrandId(null);
                        })
                }
            }
        )
        })
    }
