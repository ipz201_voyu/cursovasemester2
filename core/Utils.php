<?php

namespace core;

class Utils
{
    public static function ArrayFilter($array, $fields)
    {
        $newArray = [];
        foreach ($fields as $field) {
            $newArray[$field] = $array[$field];
        }
        return $newArray;
    }
}
