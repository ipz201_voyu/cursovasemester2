<?php

namespace core;

/**
 * головний клас ядра програми
 * (синглтон)
 * */
class Core
{
    private static $instance;
    private static $mainTemplate;
    private static $db;
    private function __construct()
    {
        global $Config;
        spl_autoload_register("\core\Core::__autoload");
        self::$db = new \core\DB(
            $Config["Database"]["Server"],
            $Config["Database"]["UserName"],
            $Config["Database"]["Password"],
            $Config["Database"]["Database"]
        );
    }

    /**
     * повертає екзеипля ядра
     * @return Core
     */
    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new \core\Core();
            return self::getInstance();
        } else {
            return self::$instance;
        }
    }
    /**
     * основний процес системи
     */

    public function getDb()
    {
        return self::$db;
    }

    public function run()
    {
        $path = $_GET["path"];
        $pathParts = explode("/", $path);
        $className = empty($pathParts[0]) ? "controllers\\Site" : "controllers\\" . ucfirst($pathParts[0]);
        $methodName = empty($pathParts[1]) ? "actionIndex" : "action" . ucfirst($pathParts[1]);

        if (class_exists($className)) {

            $controller = new $className();
            if (method_exists($controller, $methodName)) {
                $method = new \ReflectionMethod($className, $methodName);
                $paramsArray = [];
                foreach ($method->getParameters() as $param) {
                    array_push($paramsArray, isset($_GET[$param->name]) ? $_GET[$param->name] : null);
                }
                $result = $method->invokeArgs($controller, $paramsArray);
                if (is_array($result)) {
                    self::$mainTemplate->setParams($result);
                }
            } else {
                throw new \Exception("404 Not found!");
            }
        } else {
            throw new \Exception("404 Not found!");
        }
    }

    public function done()
    {
        self::$mainTemplate->display("views/layout/index.php");
    }
    /**
     * Ініціалізація системи
     */
    public function init()
    {
        session_start();
        self::$mainTemplate = new Template();
    }

    /**
     * Підключення класів
     * @param $className string назва класу
     *
     */
    public static function __autoload($className)
    {
        $fileName = $className . ".php";
        if (is_file($fileName)) {
            include($fileName);
        }
    }
}
