<?php

namespace core;

use function Sodium\add;

/**
 * шаблонізатор(клас який підставляє параметри в основну сторінку php)
 */
class Template
{
    private $parameters;
    public function __constructor()
    {
        $this->parameters = [];
    }

    /**
     * считує  значення параметра
     */
    public function getParam($name)
    {
        return $this->parameters[$name];
    }

    /**
     * задає значення параметра
     */
    public function setParam($name, $value)
    {
        $this->parameters[$name] = $value;
    }

    /**
     * задає значення масиву параметрів
     */
    public function setParams($array)
    {
        foreach ($array as $key => $value) {
            $this->setParam($key, $value);
        }
    }

    /**
     * метод що повертає html-код сторінки заповнений даними
     */
    public function render($path)
    {
        if (is_array($this->parameters))
            extract($this->parameters);
        ob_start();
        include($path);
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    /**
     * метод що показує html-код на сторінці
     */
    public function display($path)
    {
        echo self::render($path);
    }
}
