<?php

namespace core;

class DB
{
    protected $pdo;

    public function __construct($server, $login, $password, $database)
    {
        $this->pdo = new \PDO("mysql:host={$server};dbname={$database};charset=UTF8", $login, $password);
    }

    /**
     * Метод що вставляє певні дані в певну таблицю
     * @param $table string назва таблиці
     * @param $row array|string параметри, які будуть вставлені в таблицю
     * @return string індекс останнього доданого елемента
     */
    public function insert($table, $row)
    {
        $fieldsStr = implode(", ", array_keys($row));
        $valuesParts = [];
        foreach ($row as $key => $value) {
            $valuesParts[] = "?";
        }

        $valuesStr = implode(", ", $valuesParts);
        $sql = "INSERT INTO $table ($fieldsStr) VALUES ($valuesStr)";
        $sth = $this->pdo->prepare($sql);
        $sth->execute(array_values($row));
        return $this->pdo->lastInsertId();
    }

    /**
     * Метод що видаляє певні дані з певної таблиці
     * @param $table string назва таблиці
     * @param $where null|array|string параметри, за якими будуть видалені дані з таблиці
     */
    public function delete($table, $where = null)
    {
        $sql = "DELETE FROM {$table}";

        if (is_array($where) && count($where) > 0) {
            $whereParts = [];
            foreach ($where as $key => $value) {
                $whereParts[] = "$key=?";
            }
            $whereStr = implode(" AND ", $whereParts);
            $sql .= " WHERE " . $whereStr;
        }

        if (is_string($where)) {
            $sql .= " WHERE " . $where;
        }
        $sth = $this->pdo->prepare($sql);
        if (is_array($where) && count($where) > 0) {
            $sth->execute(array_values($where));
        } else {
            $sth->execute();
        }
    }

    /**
     * Метод що оновлює певні дані з певної таблиці
     * @param $table string назва таблиці
     * @param $newRow null|array|string параметри, на які будуть змінені дані
     * @param $where null|array|string параметри, за якими будуть змінені дані з таблиці
     */
    public function update($table, $newRow = null, $where = null)
    {
        $sql = "UPDATE $table SET ";
        $arrValues = [];
        if (is_array($newRow) && count($newRow) > 0) {
            $newRowParts = [];
            foreach ($newRow as $key => $value) {
                $newRowParts[] = "$key=?";
                $arrValues[] = $value;
            }
            $newRowStr = implode(", ", $newRowParts);
            $sql .= $newRowStr;
        }
        if (is_string($newRow)) {
            $sql .= $newRow;
        }

        if (is_array($where) && count($where) > 0) {
            $whereParts = [];
            foreach ($where as $key => $value) {
                $whereParts[] = "$key=?";
                $arrValues[] = $value;
            }
            $whereStr = implode(" AND ", $whereParts);
            $sql .= " WHERE " . $whereStr;
        }
        if (is_string($where)) {
            $sql .= " WHERE " . $where;
        }
        $sth = $this->pdo->prepare($sql);
        if ((is_array($where) && count($where) > 0) || (is_array($newRow) && count($newRow) > 0)) {
            $sth->execute($arrValues);
        } else {
            $sth->execute();
        }
    }

    /**
     * Метод що повертає дані з таблиці
     * @param $table string назва таблиці
     * @param $fields array|string поля, які потрібно повернути
     * @param $where null|array|string умови за якими відбудеться вибірка даних з таблиці
     * @param $orderBy null|array|string поля за якими буде сортування даних
     * @param $limit null|array|string кількість рядків
     * @param $start null|array|string порядковий номер рядка з якого почнеться вибірка
     * @param $table2 null|array|string назви таблиць, які приєднаються до початкової
     * @param $on null|array|string поля по яким буде приєднання даних
     * @return array|string
     */
    public function select($table, $fields = "*", $where = null, $orderBy = null, $limit = null, $start = null, $table2 = null, $on = null, $true = false)
    {
        $fieldsStr = "*";
        if (is_string($fields))
            $fieldsStr = $fields;

        if (is_array($fields)) {
            $fieldsStr = implode(", ", $fields);
        }
        $sql = "SELECT {$fieldsStr} FROM {$table}";

        if (is_array($table2) && count($table2) > 0) {
            $joinParts = [];
            $table2Parts = [];
            $onParts = [];
            for ($i = 0; $i < count($table2); $i++) {
                $table2Parts[] = "LEFT JOIN $table2[$i]";
            }
            foreach ($on as $key => $value) {
                $onParts[] = "ON $key = $value";
            }
            if (count($onParts) != count($table2Parts)) {
                return "Кількість on не співпадає з кількістю таблиць";
            }
            for ($i = 0; $i < count($onParts); $i++) {
                $joinParts[] = $table2Parts[$i] . " " . $onParts[$i];
            }
            $joinStr = implode(" ", $joinParts);
            $sql .= " " . $joinStr;
        }
        if (!empty($where)) {
            $sql .= "\n";
        }

        if (is_array($where) && count($where) > 0) {
            $whereParts = [];
            foreach ($where as $key => $value) {
                $whereParts[] = "$key = '$value'";
            }
            $whereStr = implode(" AND ", $whereParts);
            $sql .= " WHERE " . $whereStr;
        }

        if (is_string($where)) {
            $sql .= " WHERE " . $where;
        }

        if (is_array($orderBy) && count($orderBy) > 0) {
            $orderByParts = [];
            foreach ($orderBy as $key => $value) {
                $orderByParts[] = "$key $value";
            }
            $orderByStr = implode(", ", $orderByParts);
            $sql .= " ORDER BY " . $orderByStr;
        }
        if (is_string($orderBy)) {
            $sql .= " ORDER BY " . $orderBy;
        }
        if (!empty($limit)) {
            if (!empty($start))
                $sql .= " LIMIT $start, $limit";
            else
                $sql .= " LIMIT $limit";
        }
        if ($true) {
            return $sql;
        }

        $sth = $this->pdo->prepare($sql);
        $sth->execute();
        return $sth->fetchAll();
    }

    public function myQuery($sql){
        $sth = $this->pdo->prepare($sql);
        $sth->execute();
        return $sth->fetchAll();
    }
}
