<?php

namespace core;

/**
 * батьківський контроллер
 */
class Controller
{
    /**
     * метод що перевіряє чи був надісланий POST-запит
     * @return bool
     * */
    public function isPost()
    {
        return $_SERVER["REQUEST_METHOD"] == "POST";
    }

    /**
     * метод що перевіряє чи був надісланий GET-запит
     * @return bool
     * */
    public function isGet()
    {
        return $_SERVER["REQUEST_METHOD"] == "GET";
    }

    /**
     * метод що генерує параметри для головної index сторінки
     * @param $viewName string назва додаткового html-коду
     * @param $localParams array параметри додаткового html-коду
     * @param $globalParams array параметри основного html-коду
     * @return array|mixed|null
     */
    public function render($viewName, $localParams = null, $globalParams = null)
    {
        $tpl = new Template();
        if (is_array($localParams)) {
            $tpl->setParams($localParams);
        }
        if (!is_array($globalParams)) {
            $globalParams = [];
        }
        $moduleName = strtolower((new \ReflectionClass($this))->getShortName());
        $globalParams["PageContent"] = $tpl->render("views/{$moduleName}/{$viewName}.php");
        $_SESSION['messageType'] = $_SESSION['messageTypeDop'];
        $_SESSION['messageValue'] = $_SESSION['messageValueDop'];
        unset($_SESSION['messageTypeDop']);
        unset($_SESSION['messageValueDop']);
        return $globalParams;
    }

    /**
     * метод що переадресовує на початкову сторінку з активним повідомленням
     * @param $type int тип помилки(1 - помилка, 0 - виконано)
     * @param $message string текст помилки
     * */
    public function generateMessageIndex($type, $message, $href=null)
    {
        if ($type == 0) {
            $_SESSION['messageTypeDop'] = "success";
        } else {
            $_SESSION['messageTypeDop'] = "danger";
        }
        $_SESSION['messageValueDop'] = $message;
        if($href){
            header("Location: /".$href);
            return 0;
        }
        header("Location: /site/index");
        return 0;
    }
}
