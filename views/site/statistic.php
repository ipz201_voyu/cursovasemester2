<style>
    .active-seller{
        background-color: lightgray;
    }
    td, th{
        text-align: center;
    }
</style>

<div class="header-statistic">
    <!--Таблиця юзерів-->
    <table class="table" id="my-statistic-table">
        <thead>
        <tr>
            <th>Кількість користувачів</th>
            <th>Кількість продавців</th>
            <th>Кількість адміністраторів</th>
            <th>Кількість товарів</th>
            <th>Кількість брендів</th>
            <th>Кількість категорій</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?=$statistic["countUsers"]?></td>
            <td><?=$statistic["countSellers"]?></td>
            <td><?=$statistic["countAdmins"]?></td>
            <td><?=$statistic["countTovars"]?></td>
            <td><?=$statistic["countBrands"]?></td>
            <td><?=$statistic["countGroups"]?></td>
        </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-4" id="sellers">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Повне ім'я</th>
                    <th scope="col">Заробив</th>
                </tr>
                </thead>
                <tbody>
                <?$i=0; foreach ($statistic["allSellers"] as $seller): $i++;?>
                    <tr id="seller-<?=$seller["idUser"]?>">
                        <td><?=$i?></td>
                        <td><?=$seller["fullName"]?></td>
                        <td><?=$seller["money"]?$seller["money"]:0?></td>
                    </tr>
                <?endforeach;?>
                </tbody>
            </table>
        </div>
        <div class="col-8" id="sellerInfo">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Назва товару</th>
                    <th>Кількість товару</th>
                    <th>Сума</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    const userRows = document.querySelector("#sellers").querySelector("tbody").querySelectorAll("tr");
    Array.from(userRows).forEach(elem=>{
        elem.addEventListener("click", e=>{
            const idSeller = e.target.closest("tr").id.split("-")[1];
            fetch("/orders/getsellerinfo?idSeller="+idSeller)
            .then(res=>res.json())
            .then(data=>{
                document.querySelector(".active-seller")?.classList.remove("active-seller");
                elem.classList.add("active-seller");
                document.querySelector("#dont-find")?.remove();
                let i =0;
                let newStr = "";
                if(!data.length){
                    document.querySelector("#sellerInfo").querySelector("tbody").innerHTML = "";
                    document.querySelector("#sellerInfo").querySelector("tbody").insertAdjacentHTML("afterend",
                        "<div id='dont-find' style='text-align: start; font-weight: bold;'>Замовлень немає!</div>"
                    )
                    return;
                }
                data.forEach(elem=>{
                    i++;
                    newStr+= (`<tr>
                        <td>
                        ${i}
                        </td>
                        <td>
                            ${elem["nameTovar"]}
                        </td>
                        <td>
                            ${elem["countTovars"]}
                        </td>
                        <td>
                            ${elem["price"]*elem["countTovars"]}
                        </td>
                    </tr>`);
                    document.querySelector("#sellerInfo").querySelector("tbody").innerHTML = newStr;
                });
            })
        })
    });
    userRows?Array.from(userRows)[0].click():"";
</script>