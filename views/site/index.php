<link rel="stylesheet" href="/css/siteIndex.css">
<?php

use models\Users;

$modelUser = new Users();
$user = $modelUser->getUser();
?>
<div class="row align-items-start card-parent" style="border: 1px solid black">
    <!--Панель з категоріями товарів(групами)-->
    <div class="col-2" id="groups" style="border-right: 1px solid black">
        <? if ($user["accessUser"] > 1) : ?>
            <br>
        <? endif; ?>

        <? foreach ($groups as $group) : ?>
            <? if ($user["accessUser"] == 2 or $user["accessUser"] == 4 or  $user["accessUser"] == 5) : ?>
                <!--<div class="d-flex">

                    <--Блок з видаленням категорій товарів(груп)--
                    <div class="delete-group" style="margin-top: 8px">
                        <a class="href-delete-group" onclick="return false" href=<?= "/groups/delete?idTovarGroup=" . $group["idTovarGroup"] ?>>
                            <svg color="red" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                            </svg>
                        </a>
                    </div>

                    <--Блок з оновленням категорій товарів(груп)--
                    <div class="update-group">
                        <a href="/site/gettovars/?idTovarGroup=<?= $group["idTovarGroup"] ?>" onclick="return false;" class="catalogs btn "><?= $group["nameTovarGroup"] ?></a>
                        <input type="hidden" name="idTovarGroup" value=<?= $group["idTovarGroup"] ?>>
                        <input type="text" name="nameTovarGroup" class="update-name-group hidden-input" value=<?= $group["nameTovarGroup"] ?>>
                    </div>
                </div>-->
            <? else : ?>

                <!--Блок зі значенням категорії та посиланням на відображення товарів з неї-->
                <div>
                    <a href="/site/gettovars/?id=<?= $group["idTovarGroup"] ?>" onclick="return false;" class="btn normal-catalogs"><?= $group["nameTovarGroup"] ?></a>
                </div>
            <? endif; ?>
        <? endforeach; ?>

        <!--Блок з додаванням нової категорії товарі(групи)-->
        <? if ($user["accessUser"] == 2 or $user["accessUser"] == 4 or  $user["accessUser"] == 5) : ?>
            <div class="add-group">
                <input type="text" class="hidden-input" id="add-group-name-input" name="nameTovarGroup">
                <a onclick="return false;" class="link-primary" id="add-group">Додати категорію товарів</a>
            </div>
        <? endif; ?>
    </div>

    <!--Блок з товарами-->
    <div class="col-10" id="tovars" style="border-right: 1px solid black">
        <div class="row">

        </div>
    </div>
</div>

<script src="/js/rangePrice.js"></script>
<script src="/js/tovarsModule.js"></script>
<script src="/js/tovars.js"></script>
<script src="/js/backetModule.js"></script>
<script src="/js/groups.js"></script>


<script>
    <? if ($user["accessUser"] == 2 or $user["accessUser"] == 4 or  $user["accessUser"] == 5) : ?>
        ReWriterGroups(<?php echo json_encode($groups)?>)
    <?endif;?>

    WriteTovars(<?php echo json_encode($tovars)?>, <?php echo json_encode($user)?> )

    <?if($group["brandId"]):?>
        setBrandId(<?php echo json_encode($group["brandId"])?>)
    <?endif;?>
</script>

