<?php

use models\Users;

$modelUser = new Users();
$user = $modelUser->getUser();
$MessageTypeSession = null;
$MessageValueSession = null;
if (!empty($_SESSION["messageType"])) {
    $MessageTypeSession = $_SESSION["messageType"];
    $MessageValueSession = $_SESSION["messageValue"];
    unset($_SESSION["messageValue"]);
    unset($_SESSION["messageType"]);
}
?>


<!Doctype html>
<html>

<head>
    <title><?= $MainTitle ?></title>
    <link href="/css/layout.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link type="text/css" href="/alien/sample/css/sample.css" rel="stylesheet" media="screen"/>
    <link type="text/css" rel="stylesheet" href="/css/comments.css">
</head>

<body>

<!-- Вікно з підтвердженням дії -->
<div class="modal fade" id="confirm-form" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog-parent">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Попередження!!!</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Якщо Ви бачите це вікно саме з таким вмістом, хутко закрийте його
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Скасувати</button>
                    <button type="button" class="btn btn-danger" id="success-send-form">Підтвердити</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Вікно загрузки-->
<div id="loading">
    <div class="windows8">
        <div class="wBall" id="wBall_1">
            <div class="wInnerBall"></div>
        </div>
        <div class="wBall" id="wBall_2">
            <div class="wInnerBall"></div>
        </div>
        <div class="wBall" id="wBall_3">
            <div class="wInnerBall"></div>
        </div>
        <div class="wBall" id="wBall_4">
            <div class="wInnerBall"></div>
        </div>
        <div class="wBall" id="wBall_5">
            <div class="wInnerBall"></div>
        </div>
    </div>
    <div id="loadingText">Loading</div>
</div>

<!--Товари з кошика-->
<div class="modal fade basket-modal card-parent" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog-parent">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="col-12" id="basket-tovars">
                        <div class="row">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Навігаційна панель-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">Cursova</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!--Головні елементи панелі навігації-->
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <?php if (!$modelUser->isAuth()) : ?>
                    <li class="nav-item">
                        <a class="nav-link" href="#" id="local-basket" onclick="return false;">Кошик</a>
                    </li>
                <?php endif; ?>
                <?php if ($user["accessUser"] == 1 or $user["accessUser"] == 5) : ?>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/products/myproducts">Мої товари</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/questionnaire/getall">Не оброблені замовлення</a>
                    </li>
                <?php endif; ?>
                <?php if ($user["accessUser"] == 1 or $user["accessUser"] == 2 or $user["accessUser"] == 5) : ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/brands/index">Список брендів</a>
                    </li>
                <?php endif; ?>
                <?php if ($user["accessUser"] == 2 or $user["accessUser"] == 4 or $user["accessUser"] == 5) : ?>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/products/varificate">Верифікація товарів</a>
                    </li>
                <?php endif; ?>
                <?php if ($user["accessUser"] == 3 or $user["accessUser"] == 4 or $user["accessUser"] == 5) : ?>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/users/controll">Контроль користувачів</a>
                    </li>
                <?php endif; ?>
            </ul>

            <!--Форма пошуку-->
            <form class="d-flex" method="post" action="/site/">
                <input class="form-control me-2" type="search" name="searchStr" id = "main-search-input" placeholder="Пошук" aria-label="Search">
                <button class="btn btn-outline-success" id = "main-search-button" type="submit">Знайти</button>
            </form>

            <!--Вхід/(випадаючий список з виходом)-->
            <?php if (!$modelUser->isAuth()) : ?>
                <a class="btn btn-primary" aria-current="page" href="/users/login">Вхід</a>
            <?php else : ?>
                <div class="d-flex">
                    <ul class="navbar-nav mb-2 mb-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-bs-toggle="dropdown" aria-expanded="false">
                                <?= $user["loginUser"] ?>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="/users/update?id=<?= $user["idUser"] ?>">Профіль</a>
                                </li>
                                <li><a class="dropdown-item" href="#" id="basket" onclick="return false;">Кошик</a></li>
                                <li><a class="dropdown-item" aria-current="page" href="/questionnaire/getallmyorders">Мої замовлення</a></li>

                                <?php if ($user["accessUser"]==1||$user["accessUser"]==5):?>
                                    <li><a class="dropdown-item" aria-current="page" href="/orders/getforseller">Статистика продажу</a></li>
                                <?endif;?>
                                <?php if ($user["accessUser"]==1||$user["accessUser"]==5):?>
                                    <li><a class="dropdown-item" aria-current="page" href="/orders/getorders">Замовлення в обробці</a></li>
                                <?endif;?>
                                <?php if ($user["accessUser"]==5):?>
                                    <li><a class="dropdown-item" aria-current="page" href="/site/statistic">Статистика на сайті</a></li>
                                <?endif;?>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item" aria-current="page" href="/users/logout">Вихід</a></li>
                            </ul>
                        </li>
                    </ul>

                    <!--Аватарка користувача-->
                    <div>
                        <?php if (!empty($user["image"])) : ?>
                            <img id="profile-img-min"
                                 src="<?= "../../images/users/" . $user["image"] . "_1" . $user["typeImage"] ?>"
                                 class="img-fluid" alt="Sample image"/>
                        <?php else : ?>
                            <img id="profile-img-min"
                                 src="https://www.kindpng.com/picc/m/451-4517876_default-profile-hd-png-download.png"
                                 class="img-fluid" alt="Sample image"/>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</nav>

<!--Блок з повідомленнями-->
<div class="alert alert-dismissible fade show" id="message" role="alert">
    <div id="message-content"></div>
    <button type="button" id="delete-message-button" class="btn-close"></button>
</div>

<!--головний блок сторінки-->
<div id="main-block"><?= $PageContent ?></div>

<!--Footer-->
<footer class="bg-light text-center text-lg-start bg-gray">
    <div class="text-center p-3">
        © 2021 Copyright: <a class="text-dark" href="#">Alex Verbovskiy</a>
    </div>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ"
        crossorigin="anonymous"></script>

<script src="/alien/ckeditor.js"></script>
<script src="/js/tovarsModule.js"></script>
<script src="/js/layout.js"></script>
<script src="/js/messager.js"></script>
<script src="/js/confirmForm.js"></script>
<script src="/js/backetModule.js"></script>
<script>
    if(<?php echo json_encode($MessageTypeSession)?>!=null){
        console.log(<?php echo json_encode($MessageTypeSession)?>)
        document.querySelector("#message").style.display ="block";
        document.querySelector("#message").classList.add("alert-"+<? echo json_encode($MessageTypeSession)?>);
        document.querySelector("#message-content").innerHTML=<? echo json_encode($MessageValueSession)?>;
    }
    if(<?php echo json_encode($MessageType)?>!=null){
        document.querySelector("#message").style.display ="block";
        document.querySelector("#message").classList.add("alert-"+<? echo json_encode($MessageType)?>);
        document.querySelector("#message-content").innerHTML=<? echo json_encode($MessageContent)?>;
    }
</script>
</body>

</html>