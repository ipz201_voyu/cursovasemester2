<div class="table-responsive-sm">
    <?if(!$ignore):?>
    <button id="readyOrders" class="btn btn-success">
        Виконані замовлення
    </button>
    <button id="processOrders" class="btn btn-primary">
        Замовлення, що виконуються
    </button>
    <button id="unucceptedOrders" class="btn btn-danger">
        Не виконані замовлення
    </button>
    <?endif;?>
    <!--Таблиця юзерів-->
    <table class="table" id="my-statistic-table">
        <thead>
        <tr>
            <th>#</th>
            <th>Назва товару</th>
            <th>Кількість товару</th>
            <th>Сума</th>
            <?if(!$ignore):?>
                <th>Категорія</th>
            <?else:?>
                <th></th>
            <?endif;?>
        </tr>
        </thead>
        <tbody>

        <?if($ignore):?>
            <?$i=0; foreach ($statistics as $statistic): $i++;?>

            <tr>
                <td>
                    <?=$i?>
                </td>
                <td>
                    <?=$statistic["nameTovar"]?>
                </td>
                <td>
                    <?=$statistic["countTovars"]?>
                </td>
                <td>
                    <?=intval($statistic["price"])*intval($statistic["countTovars"])?>
                </td>
                <td>
                    <a href="/orders/readyorder?idOrder=<?=$statistic["idOrder"]?>">Виконано</a>
                </td>
            </tr>
        <?endforeach;?>
        <?endif;?>
        </tbody>
    </table>
</div>

<?if(!$ignore):?>
<script>

    const generateNewTbodyStr = (array)=>{
        let i =0;
        let newStr = "";
        array.forEach(elem=>{
            i++;

            newStr+= (`<tr>
                <td>
                ${i}
                </td>
            <td>
                ${elem["nameTovar"]}
            </td>
            <td>
                ${elem["countTovars"]}
            </td>
            <td>
                ${elem["price"]*+elem["countTovars"]}
            </td>
            <td>
                ${elem["nameTovarGroup"]}
            </td>
            </tr>`);
        })
        return newStr;
    }

    const addEvent = (idButton, statistic)=>{
        document.querySelector(idButton).addEventListener("click", ()=>{
            const tbody = document.querySelector("#my-statistic-table").querySelector("tbody");
            const orders = statistic;

            document.querySelector("#dont-find")?.remove();

            tbody.innerHTML=generateNewTbodyStr(orders);
            if(orders.length<1){
                document.querySelector("#my-statistic-table").insertAdjacentHTML("afterend",
                    "<div id='dont-find' style='display:flex;justify-content: center; font-weight: bold;'>Замовлень немає!</div>"
                )
            }
        });
    }

    addEvent("#readyOrders", <?php echo json_encode($statistics["ordersReady"]) ?>);
    addEvent("#processOrders", <?php echo json_encode($statistics["ordersProcessed"]) ?>);
    addEvent("#unucceptedOrders", <?php echo json_encode($statistics["ordersUnaccepted"]) ?>);

    document.querySelector("#readyOrders").click();

</script>
<?endif;?>