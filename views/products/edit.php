<?php

use models\Users;

$modelUser = new Users();
$user = $modelUser->getUser();
?>

<link rel="stylesheet" href="/css/editTovar.css">

<!--Форма товару-->
<form class="mx-1 mx-md-4" id="tovar-form" method="post" onsubmit="checkCountFiles(this);return false" enctype="multipart/form-data">
    <div class="row align-items-center" style="margin: 0;">

        <!--Карусель(набір малюнків) товару-->
        <div id="carouselExampleControls" class="carousel slide edit-carousel col-md-11 col-lg-4 col-xl-6" data-bs-keyboard="false" data-bs-touch="false" data-bs-ride="false" data-bs-interval="false">
            <? if (!empty($tovar["tovarImages"])) : ?>
                <button type="button" class="btn btn-success" id="delete-image-fromDB">
                    x
                </button>
                <button type="button" class="btn btn-danger visually-hidden" id="delete-image">
                    x
                </button>
                <div class="carousel-inner">
                    <?php $i = 0; ?>
                    <?php foreach ($tovar["tovarImages"] as $img) : ?>
                        <?php if ($i == 0) : ?>
                            <div class="carousel-item active">
                            <? else : ?>
                                <div class="carousel-item">
                                <? endif; ?>
                                <img class="d-block w-100" src="<?= "/images/products/" . $img["hrefImage"] . "_2" . $img["typeImage"] ?>" alt="First slide">
                                </div>
                                <?php $i = 1; ?>
                            <?php endforeach; ?>
                            </div>
                            <button id="carousel-control-prev" <? if (count($tovar["tovarImages"]) > 1) : ?>class="img-controll-hidden" <? endif; ?> type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </button>
                            <button id="carousel-control-next" <? if (count($tovar["tovarImages"]) > 1) : ?>class="img-controll-hidden" <? endif; ?> type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </button>
                        <? else : ?>
                            <button class="btn btn-danger visually-hidden" type="button" id="delete-image">
                                x
                            </button>
                            <div class="carousel-inner default-carousel">
                                <img src="https://kebabchef.ua/images/photo_default_1_0.png" id="dafault-image" alt="Default image">
                            </div>
                            <button id="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon visually-hidden"></span>
                            </button>
                            <button id="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                <span class="carousel-control-next-icon visually-hidden"></span>
                            </button>
                        <? endif; ?>
                </div>
                <div class="col-md-11 col-lg-8 col-xl-6">

                    <!--Назва товару-->
                    <div class="form-group">
                        <label for="name">Назва товару</label>
                        <input type="text" class="form-control" name="name" id="name" aria-describedby="name" value="<?= (!empty($tovar["nameTovar"]) ? $tovar["nameTovar"] : $_POST["name"]) ?>" placeholder="Назва" required>
                    </div>

                    <!--Країна виробник товару-->
                    <div class="form-group">
                        <label for="country-creator">Країна виробник</label>
                        <input type="text" class="form-control" name="countryCreator" id="country-creator" value="<?= (!empty($tovar["countryCreator"]) ? $tovar["countryCreator"] : $_POST["countryCreator"]) ?>" aria-describedby="countryCreator" placeholder="Країна виробник" required>
                    </div>

                    <!--Власник товару-->
                    <input type="hidden" name="idUser" value="<?= $user["idUser"] ?>">

                    <!--Гарантія товару-->
                    <div class="form-group">
                        <label for="guarantee">Гарантія(місяці)</label>
                        <input type="number" class="form-control" name="guarantee" id="guarantee" value="<?= (!empty($tovar["guaranteeTovar"]) ? $tovar["guaranteeTovar"] : $_POST["guarantee"]) ?>" aria-describedby="guarantee" placeholder="Гарантія" required>
                    </div>

                    <!--Кількість товару-->
                    <div class="form-group">
                        <label for="count">Кількість</label>
                        <input type="number" class="form-control" name="count" id="count" aria-describedby="count" value="<?= (!empty($tovar["countTovar"]) ? $tovar["countTovar"] : $_POST["count"]) ?>" placeholder="Кількість товару, який ви хочете продати" required>
                    </div>

                    <!--Знижка товару-->
                    <div class="form-group">
                        <label for="action">Знижка(відсотки)</label>
                        <input type="number" class="form-control" name="action" id="action" aria-describedby="action" placeholder="Знижка у відсотках" value="<?= (!empty($tovar["actionTovar"]) ? $tovar["actionTovar"] : $_POST["action"]) ?>" min="0" max="100" step="1">
                    </div>

                    <!--id товару-->
                    <? if (!empty($tovar["idTovar"])) : ?>
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="id" value="<?= $tovar["idTovar"] ?>" min="0" max="100" step="1">
                        </div>
                    <? endif; ?>
                    <? if (!empty($tovar["idTempTovar"])) : ?>
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="idTempTovar" value="<?= $tovar["idTempTovar"] ?>" min="0" max="100" step="1">
                        </div>
                    <? endif; ?>

                    <!--Ціна товару-->
                    <div class="form-group">
                        <label for="price">Ціна</label>
                        <input type="number" class="form-control" name="price" id="price" value="<?= (!empty($tovar["priceTovar"]) ? $tovar["priceTovar"] : $_POST["price"]) ?>" aria-describedby="price" placeholder="Ціна товару" required>
                    </div>

                    <!--Категорія товару-->
                    <div class="form-group">
                        <label for="group">Категорія</label>
                        <select class="form-control" id="group"></select>
                    </div>

                    <!--Бренд товару-->
                    <div class="form-group">
                        <label for="brand">Виробник(бренд)</label>
                        <select class="form-control" name="brand" id="brand"></select>
                    </div>

                    <!--Поле зі считуванням малюнків товару-->
                    <div class="d-flex flex-row align-items-center mb-4 ">
                        <div class="form-outline flex-fill mb-0">
                            <label id="photo-label" class="form-label" for="inputFiles[]">Фото товару</label>
                            <input type="file" name="inputFiles[]" accept="image/jpeg, image/png" id="input-files" class="form-control" multiple />
                        </div>
                    </div>

                </div>
        </div>

        <!--Додаткова інформація товару, що не передбачена шаблоном-->
        <div class="form-group">
            <label for="short-info">Додаткова коротка інформація(подібного формату до гарантії, ціни)</label>
            <textarea class="form-control editor" name="info" id="short-info" rows="3" placeholder="Додаткова інформація"></textarea>
        </div>

        <!--Повний опис товару-->
        <div class="form-group">
            <label for="description">Опис товару</label>
            <textarea class="form-control editor" name="description" id="description" rows="3" placeholder="Опис товару"></textarea>
        </div>

        <!--Кнопка збереження товару-->
        <button type="submit" class="btn btn-primary">Зберегти</button>
</form>

<script src="/js/addTovar.js"></script>
<script>
    document.querySelector("#short-info").innerHTML = <?php echo json_encode(!empty($tovar["infoTovar"]) ? $tovar["infoTovar"] : $_POST["info"]) ?>;
    document.querySelector("#description").innerHTML = <?php echo json_encode(!empty($tovar["descriptionTovar"]) ? $tovar["descriptionTovar"] : $_POST["description"]) ?>;
    let brands = <?php echo json_encode($brands); ?>;
    let nameTovarGroup = null;
    <?php if ($tovar["nameTovarGroup"]) : ?>
        nameTovarGroup = <?php echo json_encode($tovar["nameTovarGroup"]) ?>;
    <?php endif; ?>
    WriteGroupsBrands(brands, nameTovarGroup);
    <?php if ($tovar["tovarImages"]) : ?>
        SetDBImages(<?php echo json_encode($tovar["tovarImages"]); ?>)
    <?php endif; ?>
    //WriteCarousel(true);

    function checkCountFiles(form) {
        if (document.querySelector('#input-files').files.length + <?php echo json_encode(count($tovar["tovarImages"])); ?> === 0) {
            document.querySelector("#photo-label").innerHTML = "Товар не може бути без малюнків!!!";
            document.querySelector("#photo-label").style.color = "red";
        } else {
            form.submit();
        }
    }
</script>