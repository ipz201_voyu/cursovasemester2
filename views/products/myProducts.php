<?php

use models\Users;

$modelUser = new Users();
$user = $modelUser->getUser();
?>

<? if ($user["accessUser"] == 1 or $user["accessUser"] == 5) : ?>
    <a href="/products/add" class="link-primary" id="add-tovar">Add</a><br>
<? endif; ?>

<!--Меню переходів ввід рийнятих товарів до товарів, що очікують прийняття адмінами-->
<nav class="navbar navbar-light">
    <a class="navbar-brand" onclick="return false" href="/products/getmyproducts" id="my-actual-tovars">Товари на сайті</a>
    <a class="navbar-brand" onclick="return false" href="/products/getmyproducts" id="my-changing-tovars">Товари в обробці</a>
</nav>

<!--Таблиця з короткою інформацією товарів-->
<div class="table-responsive-sm">
    <table class="table" id="my-tovars-table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Назва товару</th>
                <th scope="col">Статус товару</th>
                <th scope="col">Дії</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div id="undefind-tovar"><h2>Товарів не додано!</h2></div>
</div>
<script src="/js/myTovars.js"></script>

<script>
    GetMyTovars(getCookie("typeMyTovars"));
</script>