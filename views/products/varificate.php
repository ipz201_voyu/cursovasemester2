<style>
    tr {
        cursor: pointer;
    }
</style>

<!--Таблиця товарів, що очікують підтвердження-->
<div class="table-responsive-sm">
    <table class="table ">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Назва товару</th>
                <th scope="col">Логін відправника</th>
                <th scope="col">Дата відправки</th>
                <th scope="col">Тип підтвердження товару</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tovars as $tovar) : ?>
                <tr onclick="window.location=`/tovars/acceptview?idTemp=<?= $tovar['idTempTovar'] ?>`;">
                    <th scope="row"><?= $i + 1 ?></th>
                    <td>
                        <?= $tovar["nameTovar"] ?>
                    </td>
                    <td>
                        <?= $tovar["loginUser"] ?>
                    </td>
                    <td>
                        <?= $tovar["dateSending"] ?>
                    </td>
                    <td>
                        <?php if ($tovar["typeChanging"] == 0) : ?>
                            Додати новий товар
                        <?php elseif ($tovar["typeChanging"] == 1) : ?>
                            Зберегти зміни в товарі
                        <?php endif; ?>
                    </td>
                </tr>
            <?php $i++;
            endforeach; ?>
        </tbody>
    </table>
    <?php if(empty($tovars)) :?>
        <div style='display:flex;justify-content: center; font-weight: bold;'>Замовлень немає!</div>
    <?endif;?>
</div>