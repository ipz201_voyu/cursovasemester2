<?php

use models\Users;

$modelUser = new Users();
$user = $modelUser->getUser();
?>

<link rel="stylesheet" href="/css/tovarIndex.css">
<link rel="stylesheet" href="/css/comments.css">
<div class="p-2 px-4 index-tovar">
    <div class="row g-0 position-relative">
        <div>

            <!--Ім'я товару-->
            <h2>
                <?= $tovar["nameTovar"] ?>
            </h2>

            <!--Рейтинг товару-->
            <? if ($access != 2) : ?>
                <div class="comments">
                    <div class="stars-body head-stars-body">
                        <div class="stars-active" style="width:<?= $tovar["rating"] ? ($tovar["rating"] * 20) . '%' : "100%" ?>"></div>
                    </div>
                    <div class="count-comments"><?= $tovar["countComments"]?></div>
                </div>
            <? endif; ?>
        </div>
        <div class="col-md-6 mb-md-0 p-md-4">
            <? if (!empty($tovar["tovarImages"])) : ?>

                <!--Карусель товару-->
                <div id="carouselExampleControls" class="carousel slide index-console" data-bs-keyboard="false" data-bs-touch="false" data-bs-ride="false" data-bs-interval="false">
                    <div class="carousel-inner">
                        <?php $i = 0; ?>
                        <?php foreach ($tovar["tovarImages"] as $img) : ?>
                            <?php if ($i == 0) : ?>
                                <div class="carousel-item active">
                                <? else : ?>
                                    <div class="carousel-item">
                                    <? endif; ?>
                                    <img class="d-block w-100" src="<?= "/images/products/" . $img["hrefImage"] . "_2" . $img["typeImage"] ?>" alt="First slide">
                                    </div>
                                    <?php $i = 1; ?>
                                <?php endforeach; ?>
                                </div>

                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon"></span>
                                </button>
                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                    <span class="carousel-control-next-icon"></span>
                                </button>
                    </div>
                <? else : ?>
                    <img class="d-block w-100" src="https://kebabchef.ua/images/photo_default_1_0.png" alt="Default image">
                <? endif; ?>
                </div>
                <div class="col-md-6 p-4 ps-md-0" id="about-tovar">

                    <!--Коротка інформація про товар-->
                    <div class="tovar-info">
                        <p><b>Країна виробник</b>: <?= $tovar["countryCreator"] ?></p>
                        <p><b>Бренд</b>: <?= $tovar["nameBrand"] ?></p>
                        <p><b>Гарантія</b>: <?= $tovar["guaranteeTovar"] ?></p>
                        <? if (!empty($tovar["infoTovar"])) : ?>
                            <p><?= $tovar["infoTovar"] ?></p>
                        <? endif; ?>
                        <? if ($access !== 1) : ?>
                    </div>

                    <!--Блок з ціною, знижкою та кількістю(з можливістю редагування для продавців)-->
                    <?php if ($tovar["actionTovar"] != 0) : ?>
                        <div class="relative" id="price-parent">
                            <div class="old-price"><?= $tovar["priceTovar"] ?> &#8372;</div>
                            <div class="price-block flex">
                                <div class="new-price">
                                    <?= intval($tovar["priceTovar"]) * intval(100 - $tovar["actionTovar"]) / 100 ?> &#8372;
                                </div>
                                <?php if(empty($user) or ($access != 2 and !$tovar["idTempTovar"])):?>
                                    <div>
                                        <a class="btn btn-success" href="/questionnaire/add?idTovar=<?= $tovar['idTovar'] ?>">Купити</a>
                                    </div>
                                <?endif;?>
                                <?php if (empty($tovar["idTempTovar"])) : ?>
                                    <?php if (!empty($user)) : ?>
                                        <?php if (empty($tovar["idBasket"])) : ?>
                                            <div class="baskets">
                                                <input type="hidden" name="idTovar" value="<?= $tovar["idTovar"] ?>">
                                                <input type="hidden" name="idUser" value="<?= $user["idUser"] ?>">
                                            </div>
                                        <? else : ?>
                                            <div class="baskets-checked">
                                                <input type="hidden" name="idBasket" value="<?= $tovar["idBasket"] ?>">
                                            </div>
                                        <? endif; ?>
                                    <? else : ?>
                                        <div class="local-baskets">
                                            <input type="hidden" name="idTovar" value="<?= $tovar["idTovar"] ?>">
                                        </div>
                                    <? endif; ?>
                                <? endif; ?>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="relative">
                            <div class="price-block flex">
                                <div class="price"><?= $tovar["priceTovar"] ?> &#8372;</div>
                                <?php if(empty($user) or ($access != 2 and !$tovar["idTempTovar"])):?>
                                    <div>
                                        <a class="btn btn-success" href="/questionnaire/add?idTovar=<?= $tovar['idTovar'] ?>">Купити</a>
                                    </div>
                                <?endif;?>
                                <?php if (empty($tovar["idTempTovar"])) : ?>
                                    <?php if (!empty($user)) : ?>
                                        <?php if (empty($tovar["idBasket"])) : ?>
                                            <div class="baskets">
                                                <input type="hidden" name="idTovar" value="<?= $tovar["idTovar"] ?>">
                                                <input type="hidden" name="idUser" value="<?= $user["idUser"] ?>">
                                            </div>
                                        <? else : ?>
                                            <div class="baskets-checked">
                                                <input type="hidden" name="idBasket" value="<?= $tovar["idBasket"] ?>">
                                            </div>
                                        <? endif; ?>
                                    <? else : ?>
                                        <div class="local-baskets">
                                            <input type="hidden" name="idTovar" value="<?= $tovar["idTovar"] ?>">
                                        </div>
                                    <? endif; ?>
                                <? endif; ?>
                            </div>
                        </div>
                    <? endif; ?>
                <? else : ?>
                    <div class="relative">
                        <form method="post" action="">
                            <div class="form-group">
                                <label for="count">Кількість</label>
                                <input type="number" class="form-control" name="count" id="count" aria-describedby="count" value="<?= $tovar["countTovar"] ?>" placeholder="Кількість товару, який ви хочете продати" required>
                            </div>
                            <input type="hidden" class="form-control" name="id" value="<?= $tovar['idTovar'] ?>">

                            <? if (!empty($tovar["idTempTovar"])) : ?>
                                <input type="hidden" class="form-control" name="idTemp" value="<?= $tovar['idTempTovar'] ?>">
                            <? endif; ?>

                            <div class="form-group">
                                <label for="action">Знижка(відсотки)</label>
                                <input type="number" class="form-control" name="action" id="action" aria-describedby="action" placeholder="Знижка у відсотках" value="<?= $tovar["actionTovar"] ?>" min="0" max="100" step="1">
                            </div>

                            <div class="form-group">
                                <label for="price">Ціна</label>
                                <input type="text" class="form-control" name="price" id="price" value="<?= $tovar["priceTovar"] ?>" aria-describedby="price" placeholder="Ціна товару" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Зберегти зміни</button>
                        </form>
                    </div>
                </div>
            <? endif; ?>
        </div>
    </div>

    <!--Опис товару-->
    <? if (!empty($tovar["descriptionTovar"])) : ?>
        <div class="row g-0 position-relative">
            <h3>
                Опис
                <bl><?= $tovar["nameTovar"] ?></bl>
            </h3>
            <p><?= $tovar["descriptionTovar"] ?></p>
        </div>
    <? endif; ?>

    <? if ($access == 2 and $tovar["idTempTovar"]) : ?>

        <!--Прийняття або відхилення товару адмінами-->
        <div style="display: flex">
            <a href="/products/accept?idTemp=<?= $tovar["idTempTovar"] ?>" id="accept-button" class="btn btn-success">
                Прийняти
            </a>
            <form method="post" action="/products/unaccept">
                <input type="hidden" name="idTemp" value="<?= $tovar["idTempTovar"] ?>">
                <input type="text" name="message" value="">
                <button type="submit" class="btn btn-danger">
                    Відхилити
                </button>
            </form>
        </div>
    <? else : ?>

        <!--Коментарі-->
        <div class="row g-0 position-relative">
            <h3>
                Коментарі
            </h3>
            <? if (!$isCommented and !empty($user)) : ?>
                <div class="card sender-tovar-comment">
                    <div class="row">
                        <div class="col-12">
                            <div class="comment-box ml-2">
                                <h4>Додайте коментар</h4>
                                <form method="post" action="/comments/addtotovar" onsubmit="return false;">
                                    <div class="rating"><input type="radio" name="ratingComment" value="5" id="5" checked><label for="5">☆</label> <input type="radio" name="ratingComment" value="4" id="4"><label for="4">☆</label> <input type="radio" name="ratingComment" value="3" id="3"><label for="3">☆</label> <input type="radio" name="ratingComment" value="2" id="2"><label for="2">☆</label> <input type="radio" name="ratingComment" value="1" id="1"><label for="1">☆</label></div>
                                    <div class="comment-area"><textarea class="form-control" name="textComment" placeholder="Яка ваша думка про даний товар?" rows="4"></textarea></div>
                                    <div class="comment-btns mt-2">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="pull-left">
                                                    <button class="btn btn-success btn-sm" type="reset">Скинути</button>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="pull-right">
                                                    <button class="btn btn-success send btn-sm" type="button" id="addComments">Надіслати <i class="fa fa-long-arrow-right ml-1"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <? endif; ?>

            <section class="container">
                <div class="row">

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-body" id="commentsBlock">

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <? endif; ?>
</div>
    <script src="/js/comments.js"></script>
    <script>
        writeAllComments(<?php echo json_encode($comments) ?>, <?php echo json_encode($tovar['idTovar']) ?>);
    </script>

    <? if (!$isCommented and $user) : ?>
        <script>
            addCommentsAddClick(<?php echo json_encode($tovar['idTovar']) ?>)
        </script>
    <? endif; ?>
<script src="/js/backetModule.js"></script>
<script>
    if (document.querySelector(".baskets")) document.querySelector(".baskets").addEventListener("click", basketAdder);
    if (document.querySelector(".baskets-checked")) document.querySelector(".baskets-checked").addEventListener("click", basketRemover);
    let baskets = JSON.parse(window.localStorage.getItem("basket"))
    if (baskets !== null && document.querySelector(".local-baskets")) {
        for (let j = 0; j < baskets.length; j++) {
            if (document.querySelector(".local-baskets").querySelector("input").value == baskets[j]) {
                document.querySelector(".local-baskets").classList.add("checked");
                break;
            }
        }
    }
    if (document.querySelector(".local-baskets")) document.querySelector(".local-baskets").addEventListener("click", localBasketChanger);
</script>