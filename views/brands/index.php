<link rel="stylesheet" href="/css/brands.css">
<?php

use models\Users;

$modelUser = new Users();
$user = $modelUser->getUser();
?>
<!--Генерація таблиці усіх брендів-->
<div class="table-responsive-sm">
    <?php if ($groups) : ?>
        <?php if ($brands) : ?>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Назва бренду</th>
                    <th scope="col">Назва категорії</th>
                    <th scope="col">Кількість товару</th>
                    <? if ($user["accessUser"] == 2 or $user["accessUser"] == 4 or $user["accessUser"] == 5): ?>
                        <th scope="col">Видалити категорію</th>
                    <?php endif; ?>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        <?php else : ?>
            Жодного бренду не додано
        <?php endif; ?>

    <?php else : ?>
        Жодного категорії товару не знайдено, для роботи з даним сектором додайте хоча б одну категорію товару!
    <?php endif; ?>
</div>
<script src="/js/brands.js"></script>
<script>
    <? if ($user["accessUser"] == 2 or $user["accessUser"] == 4 or  $user["accessUser"] == 5 ): ?>
    Writer(<?php echo json_encode($brands) ?>, <?php echo json_encode($groups) ?>)
    <?else:?>
    WriterLight(<?php echo json_encode($brands) ?>)
    <?endif;?>
</script>