<link rel="stylesheet" href="/css/user.css">

<section>
    <div class="container h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-lg-12 col-xl-11">
                <div class="card text-black bor-rad">
                    <div class="card-body p-md-5">
                        <div class="row justify-content-center">

                            <!--Форма реєстрації-->
                            <div class="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">
                                <?php if (!empty($firstName)) : ?>
                                    <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Оновлення даних</p>
                                <?php else : ?>
                                    <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Реєстрація</p>
                                <? endif; ?>
                                <form class="mx-1 mx-md-4" method="post" action="" enctype="multipart/form-data">

                                    <!--Поле з іменем-->
                                    <div class="d-flex flex-row align-items-center mb-4">
                                        <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <? if (!empty($firstName)) : ?>
                                                <input type="text" name="firstName" value="<?= $firstName ?>"
                                                       id="firstName" class="form-control"/>
                                            <? else : ?>
                                                <input type="text" name="firstName" value="<?= $_POST['firstName'] ?>"
                                                       id="firstName" class="form-control"/>
                                            <? endif; ?>
                                            <label class="form-label" for="firstName">Ім'я</label>
                                        </div>
                                    </div>

                                    <!--Поле з прізвищем-->
                                    <div class="d-flex flex-row align-items-center mb-4">
                                        <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <? if (!empty($secondName)) : ?>
                                                <input type="text" name="secondName" value="<?= $secondName ?>"
                                                       id="secondName" class="form-control"/>
                                            <? else : ?>
                                                <input type="text" name="secondName" value="<?= $_POST['secondName'] ?>"
                                                       id="secondName" class="form-control"/>
                                            <? endif; ?>
                                            <label class="form-label" for="secondName">Прізвище</label>
                                        </div>
                                    </div>

                                    <!--Поле з логіном-->
                                    <div class="d-flex flex-row align-items-center mb-4">
                                        <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <? if (!empty($login)) : ?>
                                                <input type="email" name="login" id="login" value="<?= $login ?>"
                                                       class="form-control"/>
                                            <? else : ?>
                                                <input type="email" name="login" id="login"
                                                       value="<?= $_POST['login'] ?>" class="form-control"/>
                                            <? endif; ?>
                                            <label class="form-label" for="login">Логін(email)</label>
                                        </div>
                                    </div>

                                    <!--Поле з номером телефону-->
                                    <div class="d-flex flex-row align-items-center mb-4">
                                        <i class="fas fa-key fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <? if (!empty($tel)) : ?>
                                                <input type="tel" name="telephone"
                                                       pattern="(0[0-9]{2}-[0-9]{3}-[0-9]{2}-[0-9]{2})|([0-9]{10})"
                                                       id="telephone" value="<?= $tel ?>" class="form-control"
                                                       required/>
                                            <? else : ?>
                                                <input type="tel" name="telephone"
                                                       pattern="([0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2})|([0-9]{10})"
                                                       id="telephone" value="<?= $_POST['telephone'] ?>"
                                                       class="form-control" required/>
                                            <? endif; ?>
                                            <label class="form-label" for="telephone">Номер телефону</label>
                                        </div>
                                    </div>


                                    <!--Поле з паролем-->
                                    <div class="d-flex flex-row align-items-center mb-4">
                                        <i class="fas fa-lock fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <input type="password" name="password" id="password" class="form-control"/>
                                            <label class="form-label" for="password">Пароль</label>
                                        </div>
                                    </div>

                                    <!--Поле з повторним паролем-->
                                    <div class="d-flex flex-row align-items-center mb-4">
                                        <i class="fas fa-key fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <input type="password" name="repeatPassword" id="repeat-password"
                                                   class="form-control"/>
                                            <label class="form-label" for="repeat-password">Повторіть пароль</label>
                                        </div>
                                    </div>

                                    <!--Поле зі считуванням фотографії-->
                                    <div class="d-flex flex-row align-items-center mb-4 ">
                                        <i class="fas fa-lock fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <div class="row">
                                                <button class="btn btn-danger col-2 visually-hidden" type="button" id="delete-image">
                                                    x
                                                </button>
                                                <input type="file" name="photoAccount" accept="image/jpeg, image/png"
                                                       id="photo-account" class="form-control col"/>
                                            </div>
                                            <label class="form-label" for="photo-account">Фото профілю</label>
                                        </div>
                                    </div>

                                    <!--Збереження/видалення профілю-->
                                    <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                                        <?php if (!empty($firstName)) : ?>
                                            <button type="submit" class="btn btn-primary btn-lg">Зберегти</button>
                                            <a href="/users/delete?id=<?= $id ?>" onclick="return false;"
                                               id="delete-user" class="btn btn-danger btn-lg">Видалити</a>
                                        <?php else : ?>
                                            <button type="submit" class="btn btn-primary btn-lg">Реєстрація</button>
                                        <? endif; ?>
                                    </div>

                                </form>
                            </div>

                            <!--Блок з відображенням фотографії-->
                            <div class="col-md-10 col-lg-6 col-xl-7 d-flex justify-content-center align-items-center order-1 order-lg-2">
                                <?php if (!empty($image)) : ?>
                                    <img id="profile-img"
                                         src=<?= "../../images/users/" . $image . "_2" . $type ?> class="img-fluid"
                                         alt="Sample image">
                                <?php else : ?>
                                    <img id="profile-img"
                                         src="https://www.kindpng.com/picc/m/451-4517876_default-profile-hd-png-download.png"
                                         class="img-fluid" alt="Sample image">
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="/js/users.js"></script>