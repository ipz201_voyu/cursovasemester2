<link rel="stylesheet" href="/css/user.css">

<section class="vh-100">
    <div class="container h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-lg-12 col-xl-11">
                <div class="card text-black bor-rad">
                    <div class="card-body p-md-5">
                        <div class="row justify-content-center">

                            <!--Форма запиту на скидання паролю-->
                            <div class="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">
                                <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Скидання паролю</p>
                                <form method="post" action="" class="mx-1 mx-md-4">

                                    <!--Поле з логіном-->
                                    <div class="d-flex flex-row align-items-center mb-4">
                                        <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <input type="email" name="login" value="<?= $_POST["login"] ?>" id="form3Example3c" class="form-control" />
                                            <label class="form-label" for="form3Example3c">Логін(email)</label>
                                        </div>
                                    </div>

                                    <!--Кнопка "Скинути"-->
                                    <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                                        <button type="submit" class="btn btn-primary btn-lg">Скинути</button>
                                    </div>
                                </form>
                            </div>

                            <!--Блок з малюнком-->
                            <div class="col-md-10 col-lg-6 col-xl-7 d-flex align-items-center order-1 order-lg-2">
                                <img src="https://mdbootstrap.com/img/Photos/new-templates/bootstrap-registration/draw1.png" class="img-fluid" alt="Sample image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>