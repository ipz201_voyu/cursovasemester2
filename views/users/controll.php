<div class="table-responsive-sm">

    <!--Пошук користувачів-->
    <input type="text" name="searcherUser">
    <a class="btn btn-outline-primary" id="search-users-button">Знайти</a>

    <!--Таблиця юзерів-->
    <table class="table" id="my-tovars-table">
        <thead>
            <tr>
                <th>#</th>
                <th>Ім'я користувача</th>
                <th>Логін</th>
                <th>Рівень доступу</th>
                <th>Назва доступу</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<script src="/js/usersControll.js"></script>
<script>
    writeTbody(<?php echo json_encode($users) ?>, <?php echo json_encode($_SESSION["user"]["idUser"]) ?>)
</script>