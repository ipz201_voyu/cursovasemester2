<link rel="stylesheet" href="/css/questionnaire.css">

<section class="questionnaire-tovar">
    <div class="container h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-lg-12 col-xl-11">
                <div class="card text-black bor-rad">
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Замовлення товару<br>
                                <?=$tovar["nameTovar"]?></p>

                            <!--Форма реєстрації-->
                            <div class="col-md-11 col-lg-6 col-xl-5 order-2 order-lg-1">
                                <form class="" method="post" action="" enctype="multipart/form-data">


                                    <!--Поле з повним іменем користувача-->
                                    <div class="d-flex flex-row align-items-center mb-2">
                                        <div class="form-outline flex-fill mb-0">
                                            <input type="text" name="fullNameUser" id="full-name" value="<?=$user["firstNameUser"]." "
                                            .$user["secondNameUser"]?>"
                                                   class="form-control" required />
                                            <label class="form-label" for="full-name">Повне ім'я</label>
                                        </div>
                                    </div>

                                    <input type="hidden" name="idTovar" id="idTovar" value="<?=$tovar["idTovar"]?>"
                                           class="form-control" required />

                                    <!--Поле з розміром замовлення-->
                                    <div class="d-flex flex-row align-items-center mb-2">
                                        <div class="form-outline flex-fill mb-0">
                                            <input type="number" name="countTovar" id="count" value="1"
                                                   class="form-control" min="1" max="<?=$tovar["countTovar"]?>" required />
                                            <label class="form-label" for="count">Кількість товару(не більше <?=$tovar["countTovar"]?> так як
                                                на складі на даний момент лише стільки)</label>
                                        </div>
                                    </div>

                                    <!--Поле з номером телефону-->
                                    <div class="d-flex flex-row align-items-center mb-2">
                                        <div class="form-outline flex-fill mb-0">
                                            <input type="tel"
                                                   pattern="([0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2})|([0-9]{10})"
                                                   name="telephoneUser" id="telephone" value="<?=$user["telephoneUser"]?>" class="form-control"
                                                   required />
                                            <label class="form-label" for="telephone">Номер телефону</label>
                                        </div>
                                    </div>

                                    <!--Поле з логіном юзера-->
                                    <div class="d-flex flex-row align-items-center mb-2">
                                        <div class="form-outline flex-fill mb-0">
                                            <input type="email" name="emailUser" id="email" value="<?=$user["loginUser"]?>"
                                                   class="form-control" required />
                                            <label class="form-label" for="email">Електронна пошта</label>
                                        </div>
                                    </div>

                                    <!--Адреса доставки-->
                                    <div class="d-flex flex-row align-items-center mb-2">
                                        <div class="form-outline flex-fill mb-0">
                                            <input type="text" name="adressUser" id="adress" value=""
                                                   class="form-control" required />
                                            <label class="form-label" for="adress">Адреса доставки</label>
                                        </div>
                                    </div>

                                    <div class="d-flex flex-row align-items-center mb-2">
                                        <div class="form-outline flex-fill mb-0 d-flex justify-content-center">
                                            <button type="submit" class="btn btn-primary">
                                                Замовити
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!--Блок з головною фотографією товару-->
                            <div class="col-md-11 col-lg-6 col-xl-7 d-flex justify-content-center align-items-start order-1 order-lg-2">
                                <?if($image):?>
                                    <img id="profile-img"
                                         src="<?= "/images/products/" . $image["hrefImage"] . "_2" . $image["typeImage"] ?>"
                                         class="img-fluid mb-5" alt="Sample image">
                                <?else:?>
                                    <img class="d-block w-100" src="https://kebabchef.ua/images/photo_default_1_0.png" alt="Default image">
                                <?endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>