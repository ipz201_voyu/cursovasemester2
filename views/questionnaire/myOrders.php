<div class="table-responsive-sm">

    <!--Таблиця юзерів-->
    <table class="table" id="my-tovars-table">
        <thead>
        <tr>
            <th>#</th>
            <th>Назва товару</th>
            <th>Замовлена кількість</th>
            <th>Поточна сума</th>
            <th>Статус замовлення</th>
        </tr>
        </thead>
        <tbody>
        <?php $i=0;
        foreach ($orders as $order) :
            $i++;?>
            <tr>
                <td>
                    <?=$i?>
                </td>
                <td>
                    <?=$order["nameTovar"]?>
                </td>
                <td>
                    <?=$order["countTovar"]?>
                </td>
                <td>
                    <?=$order["price"]?>
                </td>
                <td>
                    <span id="order<?=$order["idQuestionnaire"]?$order["idQuestionnaire"]:""?>"><?=$order["status"]?></span>
                </td>
            </tr>
        <?endforeach;?>
        </tbody>
    </table>
</div>

<script>
    let orders = Array.from(document.querySelectorAll("span"));
    const ordersWhichNeedAccept  = orders.filter(order => order.innerHTML==="Очікує підтвердження");
    ordersWhichNeedAccept.forEach(elem=> {
        elem.addEventListener("mousemove", e => {
            if (e.target.innerHTML === "Очікує підтвердження") {
                e.target.innerHTML = '<button class="btn btn-danger">Скасувати замовлення</button>';
                e.target.querySelector("button").addEventListener("click", event => {
                    const actualId = e.target.id.split("order")[1];
                    fetch("/questionnaire/closemyquestionnaire?idQuestionnaire=" + actualId)
                        .then(res => res.json())
                        .then(data => {
                            if (!data) {
                                e.target.parentElement.parentElement.remove();
                            } else {
                                alert(data)
                            }
                        })
                })
            }
        });
        elem.addEventListener("mouseleave", e => {
            if (e.target.innerHTML !== "Очікує підтвердження") {
                e.target.innerHTML = "Очікує підтвердження"
            }
        });
    })
</script>