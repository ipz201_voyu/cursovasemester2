<section class="questionnaire-tovar">
    <div class="container">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-lg-12 col-xl-11">
                <div class="card text-black bor-rad">
                    <div class="card-body">
                        <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Анкета замовленого товару<br>
                            <?= $questionnaire["nameTovar"] ?>
                        </p>
                        <!--Форма замовлення-->
                        <div class="">
                            <div class="mx-1 mx-md-4">

                                <!--Поле з ім'ям та прізвіщем-->
                                <div class="d-flex flex-row align-items-center mb-4">
                                    <div class="form-outline flex-fill text-center mb-0">
                                        Повне ім'я: <?= $questionnaire["fullNameUser"] ?>
                                    </div>
                                </div>

                                <!--Поле з розміром замовлення-->
                                <div class="d-flex flex-row align-items-center mb-4">
                                    <div id="count-wanted" class="form-outline flex-fill text-center mb-0">
                                        Кількість: <?= $questionnaire["wantedCount"] ?>
                                    </div>
                                </div>

                                <!--Поле з ціною товару-->
                                <div class="d-flex flex-row align-items-center mb-4">
                                    <div class="form-outline flex-fill text-center mb-0">
                                        Ціна: <?= $questionnaire["price"]?>
                                    </div>
                                </div>
                                <!--Поле з номером телефону-->
                                <div class="d-flex flex-row align-items-center mb-4">
                                    <div class="form-outline flex-fill text-center mb-0">
                                        Номер телефону: <?= $questionnaire["telUser"] ?>
                                    </div>
                                </div>

                                <!--Поле електронною поштою-->
                                <div class="d-flex flex-row align-items-center mb-4">
                                    <div class="form-outline flex-fill text-center mb-0">
                                        Електронна пошта покупця: <?= $questionnaire["emailUser"] ?>
                                    </div>
                                </div>

                                <!--Адреса доставки-->
                                <div class="d-flex flex-row align-items-center mb-4">
                                    <div class="form-outline flex-fill text-center mb-0">
                                        Куди доставити: <?= $questionnaire["addressDelivery"] ?>
                                    </div>
                                </div>

                                <div class="d-flex flex-row align-items-center mb-4 row">
                                    <div class="form-outline flex-fill mb-0 d-flex justify-content-center col-6">
                                        <a type="button" class="btn btn-success" id="accept"
                                           onclick="return false;"
                                           href="/questionnaire/accept?id=<?=$questionnaire["idQuestionnaire"]?>">
                                            Виконано
                                        </a>
                                    </div>
                                    <div class="form-outline flex-fill mb-0 d-flex justify-content-center col-6">
                                        <input type="text" name="message" id="unaccept-message" placeholder="Чому не прийняли!" required>
                                        <a style="margin-left: 10px" type="button" class="btn btn-danger" id="unaccept"
                                           onclick="return false;"
                                           href="/questionnaire/unaccept?id=<?=$questionnaire["idQuestionnaire"]?>">
                                            Відхилено
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="/js/waitedQuestionnaire.js"></script>
<script>setFunctional(<?php echo json_encode($questionnaire["storeCount"]); ?>)</script>