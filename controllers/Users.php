<?php

namespace controllers;

use DateTime;
use core\Controller;

class Users extends Controller
{
    protected $usersModel;

    public function __construct()
    {
        $this->usersModel = new \models\Users();
    }

    /**
     * функція, що відповідає за реєстрацію користувача
     */
    public function actionRegister()
    {
        if (!empty($_SESSION['user'])) return $this->generateMessageIndex(1, "Сторінку не знайдено!");
        if ($this->isPost()) {
            $correct_types = ["image/jpeg", "image/png"];
            if (!in_array($_FILES["photoAccount"]["type"], $correct_types) and is_file(is_file($_FILES["photoAccount"]["tmp_name"]))) {
                return $this->render(
                    "register",
                    null,
                    [
                        "MainTitle" => "Реєстрація",
                        "MessageType" => "danger",
                        "MessageContent" => "Некоректний тип зображення!"
                    ]
                );
            }
            $uns = $this->usersModel->addTemplateUser($_POST);
            if (is_array($uns)) {

                if (is_file($_FILES["photoAccount"]["tmp_name"])) {
                    $rand = strval(time());
                    $type = null;
                    switch ($_FILES["photoAccount"]["type"]) {
                        case "image/png":
                            $type = ".png";
                            break;
                        case "image/jpeg":
                            $type = ".jpg";
                            break;
                        default:
                            $type = null;
                    }
                    $href = $uns["id"] . hash("ripemd128", $rand);
                    move_uploaded_file($_FILES["photoAccount"]["tmp_name"], 'images/users/' . $href . $type);
                    $this->usersModel->changePhoto("tempusers", $uns["id"], $href, $type);
                    $im = new \Imagick();
                    $im->readImage($_SERVER["DOCUMENT_ROOT"] . '/images/users/' . $href . $type);
                    $im->thumbnailImage(450, 450, true, false);
                    $im->writeImage($_SERVER["DOCUMENT_ROOT"] . '/images/users/' . $href . "_2" . $type);
                    $im->thumbnailImage(40, 40, false, false);
                    $im->writeImage($_SERVER["DOCUMENT_ROOT"] . '/images/users/' . $href . "_1" . $type);
                    unlink('images/users/' . $href . $type);
                }

                mail(
                    $_POST["login"],
                    "Варифікація акаунту",
                    "Ваш акаунт було створено на нашому сайті для варифікації перейдіть за посиланням(воно активне лише один раз)" .
                        "\ncursova/users/valificate?key="
                        . $uns["tempKeyHash"] . "\nДякуємо за використання нашого сайту!"
                );
                return $this->generateMessageIndex(0, "Реєстрацію успішно виконано!");
            } else {
                return $this->render(
                    "register",
                    null,
                    [
                        "MainTitle" => "Реєстрація",
                        "MessageType" => "danger",
                        "MessageContent" => $uns
                    ]
                );
            }
        } else {
            return $this->render(
                'register',
                null,
                ["MainTitle" => "Реєстрація"]
            );
        }
    }

    /**
     *  функція, що відповідає за вихід з акаунту користувача
     */
    public function actionLogout()
    {
        if (empty($_SESSION['user'])) {
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");
        }
        unset($_SESSION['user']);
        return $this->generateMessageIndex(0, "Вихід успішно виконано!");
    }

    /**
     *  функція, що відповідає за вхід в акаунт користувача
     */
    public function actionLogin()
    {
        if (!empty($_SESSION['user'])) return $this->generateMessageIndex(1, "Сторінку не знайдено!");
        if ($this->isPost()) {
            $uns = $this->usersModel->authUser($_POST);
            if (is_string($uns)) {
                return $this->render(
                    'login',
                    null,
                    [
                        "MainTitle" => "Логінінг",
                        "MessageType" => "danger",
                        "MessageContent" => $uns
                    ]
                );
            } else {
                $_SESSION['user']=$uns;
                return $this->generateMessageIndex(0, "Вхід успішно виконано!");
            }
        } else {
            return $this->render('login', null, ["MainTitle" => "Логінінг"]);
        }
    }

    /**
     *  функція, що відповідає за генерацію ключа для скидання забутого паролю
     */
    public function actionForgotPassword()
    {
        if (!empty($_SESSION['user'])) {
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");
            /*return $this->renderMessage("success", "Success", null,
                ["MainTitle" => "Відновлення паролю"]);*/
        }
        if ($this->isPost()) {
            if ($this->usersModel->checkLoginInDb("users", $_POST["login"])) {
                $hash = "";
                $userId = $this->usersModel->getUserIdByLogin($_POST["login"]);
                if (!$this->usersModel->checkTempKeyByIdUserInBd($userId)) {
                    $hash = $this->usersModel->createTempKey(1, $userId)["tempKeyHash"];
                } else {
                    $hash = $this->usersModel->updateTempKey($userId);
                    if (empty($hash)) {
                        return $this->generateMessageIndex(1, "Невідома помилка!");
                    }
                    $hash = $hash["tempKeyHash"];
                }
                mail(
                    $_POST["login"],
                    "Зміна паролю на вашому акаунті",
                    "Доброго дня! Було надіслано запит на зміну пароля на нашому сайті, якщо запит надіслали не ви, то краще змініть старий пароль!\nПосилання для зміни паролю:" .
                        "\ncursova/users/resetPassword?key="
                        . $hash . "\nДякуємо за використання нашого сайту!(Примітка: посилання дійсне лише годину)"
                );
                return $this->generateMessageIndex(0, "Запит на зміну паролю надіслано!");
            } else {
                return $this->render('forgotPassword', null, [
                    "MainTitle" => "Відновлення паролю",
                    "MessageType" => "danger",
                    "MessageContent" => "Користувач не знайдений"
                ]);
            }
        } else {
            return $this->render('forgotPassword', null, ["MainTitle" => "Відновлення паролю"]);
        }
    }

    /**
     *  функція, що відповідає за варифікацію акаунту
     */
    public function actionValificate()
    {
        if ($this->isGet()) {
            if (!empty($_SESSION['user'])) {
                return $this->generateMessageIndex(1, "Сторінку не знайдено!");
            }
            if (!$this->usersModel->checkTempKeyInDb($_GET["key"])) {
                return $this->generateMessageIndex(1, "Сторінку не знайдено!");
            }
            $res = $this->usersModel->getTempKey($_GET["key"]);
            if ($res["statusKey"] == 0) {
                $this->usersModel->addNormalUser($res);
                $this->usersModel->removeTempKey($res["idKey"]);
            } else {
                return $this->generateMessageIndex(1, "Сторінку не знайдено!");
            }
            return $this->render("valificate", null, ["MainTitle" => "Варифікація користувача"]);
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }

    /**
     *  функція, що відповідає за оновлення паролю користувачем
     */
    public function actionResetPassword()
    {
        if ($this->isGet()) {
            if (!empty($_SESSION['user'])) {
                return $this->generateMessageIndex(1, "Сторінку не знайдено!");
            }
            if (!$this->usersModel->checkTempKeyInDb($_GET["key"])) {
                return $this->generateMessageIndex(1, "Сторінку не знайдено!");
            }
            $res = $this->usersModel->getTempKey($_GET["key"]);
            if ($res["statusKey"] == 1) {

                $date = new DateTime($res["dateKey"]);
                $dateNow = new DateTime('now');
                $interval = $dateNow->diff($date);
                $res = ($interval->days * 24) + $interval->h;
                if ($res >= 1) {
                    $this->usersModel->removeTempKey($res["idKey"]);
                    return $this->generateMessageIndex(1, "Сторінку не знайдено!");
                } else {
                    return $this->render("resetPassword", null, ["MainTitle" => "Скидання пароля"]);
                }
            } elseif (!empty($res["statusKey"])) {
                return $this->generateMessageIndex(1, "Сторінку не знайдено!");
            } else {
                return $this->render("resetPassword", null, ["MainTitle" => "Оновлення паролю"]);
            }
        }

        if ($this->isPost()) {
            $res = $this->usersModel->getTempKey($_GET["key"]);
            if (
                $_POST["passwordUser"] == $_POST["repeatPasswordUser"]
                && !empty($_POST["repeatPasswordUser"])
                && strlen($_POST["passwordUser"]) > 7
            ) {
                $this->usersModel->updatePasswordUser($_POST["passwordUser"], $res["idUser"]);
                $this->usersModel->removeTempKey($res["idKey"]);
                return $this->generateMessageIndex(0, "Пароль успішно оновлено!");
            } else {
                return $this->render("resetPassword", null, [
                    "MainTitle" => "Оновлення користувача",
                    "MessageType" => "danger",
                    "MessageContent" => strlen($_POST["passwordUser"]) < 7 ?
                        "Пароль має складатись хоча б з 8 символів" :
                        "Паролі не співпадають"
                ]);
            }
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }

    /**
     *  функція, що відповідає за оновлення профілю користувачем
     */
    public function actionUpdate()
    {
        if (empty($_SESSION['user'])) return $this->generateMessageIndex(1, "Сторінку не знайдено!");
        if ($this->isPost()) {
            $correct_types = ["image/jpeg", "image/png"];
            if (!in_array($_FILES["photoAccount"]["type"], $correct_types) and is_file(is_file($_FILES["photoAccount"]["tmp_name"]))) {
                return $this->render(
                    "register",
                    null,
                    [
                        "MainTitle" => "Оновлення профілю",
                        "MessageType" => "danger",
                        "MessageContent" => "Некоректний тип зображення!"
                    ]
                );
            }
            $uns = $this->usersModel->updateUser($_POST, $_GET["id"]);
            if (!is_string($uns)) {
                if (is_file($_FILES["photoAccount"]["tmp_name"])) {
                    $rand = strval(time());
                    $type = null;
                    switch ($_FILES["photoAccount"]["type"]) {
                        case "image/png":
                            $type = ".png";
                            break;
                        case "image/jpeg":
                            $type = ".jpg";
                            break;
                        default:
                            $type = null;
                    }
                    $href = $_GET["id"] . hash("ripemd128", $rand);
                    move_uploaded_file($_FILES["photoAccount"]["tmp_name"], 'images/users/' . $href . $type);
                    $this->usersModel->changePhoto("users", $_GET["id"], $href, $type);
                    $im = new \Imagick();
                    $im->readImage($_SERVER["DOCUMENT_ROOT"] . '/images/users/' . $href . $type);
                    $im->thumbnailImage(450, 450, true, false);
                    $im->writeImage($_SERVER["DOCUMENT_ROOT"] . '/images/users/' . $href . "_2" . $type);
                    $im->thumbnailImage(40, 40, false, false);
                    $im->writeImage($_SERVER["DOCUMENT_ROOT"] . '/images/users/' . $href . "_1" . $type);
                    unlink('images/users/' . $href . $type);
                }

                $res = $this->usersModel->reAuthUser($_GET["id"]);
                if (is_string($res)) return $this->generateMessageIndex(1, "Трапилась невідома помилка!");
                $_SESSION['user'] = $res;
                return $this->generateMessageIndex(0, "Оновлення даних успішно виконано!");
            } else {
                return $this->render(
                    "register",
                    null,
                    [
                        "MainTitle" => "Реєстрація",
                        "MessageType" => "danger",
                        "MessageContent" => $uns
                    ]
                );
            }
        }
        if ($this->isGet()) {
            $user = $this->usersModel->getUserById($_GET["id"]);
            return $this->render(
                'register',
                [
                    'firstName' => $user["firstNameUser"],
                    'secondName' => $user["secondNameUser"],
                    'login' => $user["loginUser"],
                    'image' => $user["image"],
                    'type' => $user["typeImage"],
                    'id' => $user["idUser"],
                    'tel'=>$user["telephoneUser"],
                ],
                ["MainTitle" => "Оновлення профілю"]
            );
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }

    /**
     *  функція, що відповідає за видалення профілю користувачем
     */
    public function actionDelete()
    {
        if ($this->isGet()) {
            $this->usersModel->deleteUser($_GET["id"]);
            unset($_SESSION['user']);
            return $this->generateMessageIndex(0, "Акаунт видалено!");
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }

    /**
     *  функція, що відповідає за видалення фотографію профілю
     */
    public function actionDeleteimage()
    {
        if ($this->isGet()) {
            $uns = $this->usersModel->deletePhoto(explode("_", $_GET["hrefImage"])[0]);
            $res = $this->usersModel->reAuthUser($uns);
            $_SESSION['user'] = $res;
            exit(json_encode(["res"=>"OK!"]));
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }

    /**
     *  функція, що відповідає за відображення коротких даних про користувача, та його рівня доступу на сторінці
     */
    public function actionControll()
    {
        if (empty($_SESSION['user']) or ($_SESSION['user']["accessUser"] != 3 and $_SESSION['user']["accessUser"] != 4 and $_SESSION['user']["accessUser"] != 5))
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");
        global $Config;
        $countUsers = $Config["CountIndexUsers"];
        if ($this->isGet()) {
            $users = $this->usersModel->getUsersToUpdateAccess($countUsers, 0);
            return $this->render("controll", ["users" => $users], ["MainTitle" => "Керування доступом користувачів"]);
        }
        $users = $this->usersModel->getUsersToUpdateAccessWhere($_POST["where"], $countUsers, $_POST["start"]);
        exit(json_encode(["users" => $users]));
    }

    /**
     *  функція, що відповідає за оновлення доступу адміном або менеджером користувача
     */
    public function actionUpdateaccess()
    {
        if (empty($_SESSION['user']) or ($_SESSION['user']["accessUser"] != 3 and $_SESSION['user']["accessUser"] != 5))
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");
        if ($this->isPost()) {
            if ($_POST["accessUser"] >= $_SESSION["user"]["accessUser"] or $_POST["accessUser"] < 0) {
                exit(json_encode(["error" => "не коректний ввід даних!"]));
            }
            $this->usersModel->updateUserAccess($_POST["idUser"], $_POST["accessUser"]);
            $res = $this->usersModel->getUserToUpdateAccessById($_POST["idUser"]);
            exit(json_encode($res));
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }


}
