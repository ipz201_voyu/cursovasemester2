<?php

namespace controllers;

use core\Controller;
use models\Users;


class Groups extends Controller
{
    protected $groupModel;
    protected $user;
    protected $brandModel;
    protected $productModel;
    public function __construct()
    {
        $this->groupModel = new \models\Groups();
        $modelUser = new Users();
        $this->user = $modelUser->getUser();
        $this->brandModel = new \models\Brands();
        $this->productModel = new \models\Products();

    }

    /**
     * додання нової групи товарів
     */
    public function actionAdd()
    {
        if ($this->user["accessUser"] !=2 and $this->user["accessUser"] !=4 and $this->user["accessUser"] !=5)
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");

        if ($this->isPost()) {
            $res = $this->groupModel->addGroup($_POST);

            if (is_string($res))
                exit(json_encode(["error" => $res]));

            $groups = $this->groupModel->getAllGroups();
            exit(json_encode(["groups"=>$groups]));
        }
        return $this->generateMessageIndex(1, "Сторінка не знайдена!");
    }

    /**
     * оновлення певної групи товарів
     */
    public function actionUpdate()
    {
        if ($this->user["accessUser"] !=2 and $this->user["accessUser"] !=4 and $this->user["accessUser"] !=5)
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");

        if ($this->isPost()) {
            $res = $this->groupModel->updateGroup($_POST);

            if (is_string($res))
                exit(json_encode(["error" => $res]));

            $groups = $this->groupModel->getAllGroups();
            exit(json_encode(["groups"=>$groups]));
        }
        return $this->generateMessageIndex(1, "Сторінка не знайдена!");
    }

    /**
     * видаленя певної групи товарів
     */
    public function actionDelete()
    {
        if ($this->user["accessUser"] !=2 and $this->user["accessUser"] !=4 and $this->user["accessUser"] !=5)
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");

        if ($this->isGet()) {
            $this->groupModel->deleteGroup($_GET["idTovarGroup"]);
            $groups = $this->groupModel->getAllGroups();
            exit(json_encode(["groups"=>$groups]));
        }
        return $this->generateMessageIndex(1, "Сторінка не знайдена!");
    }
}
