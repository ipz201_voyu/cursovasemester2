<?php

namespace controllers;

use core\Controller;
use core\Core;

class Comments extends Controller
{
    protected $commentsModel;
    public function __construct()
    {
        $this->commentsModel = new \models\Comments();
    }

    /**
     * відповідає за додавання коментаря на пряму до товара
     */
    public function actionAddtotovar()
    {
        if ($this->isPost()) {
            $idComment = $this->commentsModel->add($_POST["idTovar"], $_POST["textComment"], $_POST["ratingComment"]);
            $lastComment =  $this->commentsModel->getCommentById($idComment);
            $lastComment["idForComment"] = $lastComment["idComment"];
            $lastComment["mainRating"] = $this->commentsModel->getAvgStars($_POST["idTovar"]);
            $lastComment["countComments"] = $this->commentsModel->getMainCommentsCount($_POST["idTovar"]);
            exit(json_encode($lastComment));
        }
        return $this->generateMessageIndex(1, "Сторінка не знайдена!");
    }

    /**
     * відповідає за додавання коментаря до коментаря
     */
    public function actionAddtocomment()
    {
        if ($this->isPost()) {
            $this->commentsModel->addToComment($_POST["idTovar"], $_POST["textComment"], $_POST["idFor"]);
            $comment = $this->commentsModel->getCommentById($_POST["idFor"]);
            $comment["dopComments"] = $this->commentsModel->getCommentsByIdTovarAndIdFor($_POST["idTovar"], $_POST["idFor"]);
            $comment["idForComment"] = $comment["idComment"];
            exit(json_encode($comment));
        }
        return $this->generateMessageIndex(1, "Сторінка не знайдена!");
    }
}
