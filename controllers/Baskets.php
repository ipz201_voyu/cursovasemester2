<?php

namespace controllers;

use core\Controller;
use models\Users;

class Baskets extends Controller
{
    protected $basketsModel;
    protected $productModel;
    protected $commentsModel;
    protected $user;
    public function __construct()
    {
        $this->basketsModel = new \models\Baskets();
        $this->productModel = new \models\Products();
        $usersModel = new \models\Users();
        $this->user = $usersModel->getUser();
        $this->commentsModel = new \models\Comments();
    }

    public function actionAdd()
    {
        if ($this->isPost()) {
            $idBasket = $this->basketsModel->add($_POST["idTovar"],  $this->user["idUser"]);
            exit(json_encode($idBasket));
        }
        header("Location: /site/index?messageType=danger&&messageValue=Сторінку не знайдено!");
        return 0;
    }

    public function actionIndex()
    {
        if (empty($_SESSION["user"]))
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");

        if ($this->isGet()) {
            $res = $this->basketsModel->get($this->user["idUser"]);
            for ($i = 0; $i < count($res); $i++) {
                $res[$i]["images"] = $this->productModel->getImagesByIdProduct($res[$i]["idTovar"]);
                $res[$i]["rating"] = $this->commentsModel->getAvgStars($res[$i]["idTovar"]);
                $res[$i]["countComments"] = $this->commentsModel->getMainCommentsCount($res[$i]["idTovar"]);
            }

            exit(json_encode(["tovars"=>$res, "user"=>$this->user]));
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }

    public function actionLocalindex()
    {
        if ($this->isPost()) {
            $res = [];
            for ($i = 0; $i < count(json_decode($_POST["tovarsIndex"])); $i++) {
                $res[$i] = $this->productModel->getShrotInfoById(json_decode($_POST["tovarsIndex"])[$i]);
                $res[$i]["images"] = $this->productModel->getImagesByIdProduct($res[$i]["idTovar"]);
            }
            exit(json_encode(["tovars"=>$res, "user"=>$this->user]));
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }

    public function actionDelete()
    {
        if (empty($_SESSION["user"]))
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");

        if ($this->isPost()) {
            $res = $this->basketsModel->delete($_POST["idBasket"]);
            exit(json_encode($res));
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }
}
