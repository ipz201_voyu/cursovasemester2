<?php

namespace controllers;

use core\Controller;
class Questionnaire extends Controller
{
    protected $productModel;
    protected $questionnaireModel;
    protected $user;
    protected $orderModel;
    public function __construct()
    {
        $this->productModel = new \models\Products();
        $modelUser = new \models\Users();
        $this->user = $modelUser->getUser();
        $this->questionnaireModel = new \models\Questionnaire();
        $this->orderModel = new \models\Orders();
    }

    public function actionAdd(){
        if($this->isGet()){
            $tovar = $this->productModel->getShrotInfoById($_GET["idTovar"]);
            $image = $this->productModel->getImagesByIdProduct($_GET["idTovar"])[0];
            return $this->render("add", ["tovar"=>$tovar, "image"=>$image,"user"=>$this->user],["Заповнення анкети"]);
        }
        if($this->isPost()){
            $idClient = $this->user?$this->user["idUser"]:null;
            $this->questionnaireModel->add($_POST, $idClient);
            return $this->generateMessageIndex(0, "Виконано заповнення анкети! Очікуйте дзвінка продавця!");
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }

    public function actionIndex(){
        if($this->isGet()){
            $questionnaire = $this->questionnaireModel->getInfo($_GET["id"]);
            return $this->render("index", ["questionnaire"=>$questionnaire],["Перегляд анкети"]);
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }

    public function actionGetall(){
        if($this->isGet()){
            $questionnaires = $this->questionnaireModel->getForUser($this->user["idUser"]);
            return $this->render("getall", ["questionnaires"=>$questionnaires],["Замовлення моїх товарів"]);
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }

    public function actionAccept(){
        if($this->isGet()) {
            $this->orderModel->addOrder($_GET["id"]);
            $shortQuestionnaire = $this->questionnaireModel->accept($_GET["id"]);
            mail(
                $shortQuestionnaire["emailUser"],
                "Прийнято запит на купівлю ".$shortQuestionnaire["nameTovar"],
                "Доброго дня шановний, !".$shortQuestionnaire["fullNameUser"].
                "\n Вами було надіслано запит на купівлю товару ".$shortQuestionnaire["nameTovar"] .
                ".\nЗапит було прийнято та оброблено, чекайте повідомлення від продавця про прибуття товару до точки призначення!"
                ."\nДякуємо за використання нашого сайту! З повагою, адміністрація!"
            );
            header("Location: /questionnaire/getall");
        }
    }

    public function actionUnaccept(){
        if($this->isGet()) {
            $this->orderModel->addOrder($_GET["id"], true);
            $shortQuestionnaire = $this->questionnaireModel->delete($_GET["id"]);
            mail(
                $shortQuestionnaire["emailUser"],
                "Відхилено запит на купівлю ".$shortQuestionnaire["nameTovar"],
                "Доброго дня шановний, !".$shortQuestionnaire["fullNameUser"].
                "\n Вами було надіслано запит на купівлю товару ".$shortQuestionnaire["nameTovar"] .
                ".\nНа жаль, товар не прицнято через: "
                . lcfirst($_GET["message"]) .
                "\nДякуємо за використання нашого сайту! З повагою, адміністрація!"
            );
            header("Location: /questionnaire/getall");
        }
    }

    public function actionClosemyquestionnaire(){
        if($this->isGet()) {
            $idUser = $this->user["idUser"];
            exit (json_encode(($this->questionnaireModel->close($_GET["idQuestionnaire"], $idUser))));
        }
    }

    public function actionGetallmyorders(){
        if($this->isGet()) {
            $idUser = $this->user["idUser"];
            $questionnaires = $this->questionnaireModel->getForClient($idUser);
            $orders = $this->orderModel->getForClient($idUser);
            for($i = 0; $i < count($orders); $i++){
                $orderStatus = $orders[$i]["dateStart"]?$orders[$i]["dateFinish"]?"Виконано":"Виконується":"Не прийнято";
                $orders[$i]["status"]=$orderStatus;
            }
            $res = array_merge($questionnaires, $orders);
            for($i = 0; $i < count($res); $i++) {

                $nameTovar = $this->productModel->getShrotInfoById($res[$i]["idTovar"])["nameTovar"];

                $res[$i]["nameTovar"] = $nameTovar;
            }
            return $this->render("myOrders", ["orders"=>$res],["Мої замовлення"]);
        }
    }
}