<?php

namespace controllers;

use core\Controller;

class Site extends Controller
{
    protected $groupsModel;
    protected $productModel;
    protected $commentsModel;
    protected $basketsModel;
    protected $countTovars;
    protected $usersModel;
    protected $brandsModel;
    protected $ordersModel;

    public function __construct()
    {
        $this->groupsModel = new \models\Groups();
        $this->productModel = new \models\Products();
        $this->commentsModel = new \models\Comments();
        $this->basketsModel = new \models\Baskets();
        $this->usersModel = new \models\Users();
        $this->brandsModel = new \models\Brands();
        $this->ordersModel = new \models\Orders();
        global $Config;
        $this->countTovars = $Config["CountIndexTovars"];
    }

    /**
     * генерує початкову сторінку сайту
     */
    public function actionIndex()
    {
        $groups = $this->groupsModel->getAllGroups();
        $tovars = "";
        $brands = null;
        if ($this->isGet()) {
            $tovars = $this->productModel->getSomeProducts($this->countTovars);
        } else if ($this->isPost()) {
            $tovars = $this->productModel->getSomeProductsByName($_POST["searchStr"]);
            if (empty($tovars)) {
                $tovars = $this->productModel->getSomeProductsByBrandName($_POST["searchStr"]);
                if (!empty($tovars)) {
                    $brands = $tovars["brand"];
                }
            }
        }
        for ($i = 0; $i < count($tovars); $i++) {
            $tovars[$i]["images"] = $this->productModel->getImagesByIdProduct($tovars[$i]["idTovar"]);
            $tovars[$i]["idBasket"] = $this->basketsModel->getByTovarId($tovars[$i]["idTovar"], $_SESSION["user"]["idUser"]);
            $tovars[$i]["rating"] = $this->commentsModel->getAvgStars($tovars[$i]["idTovar"]);
        }

        return $this->render("index", [
            "groups" => $groups,
            "brandId" => $brands,
            "tovars" => $tovars
        ], [
            "MainTitle" => "Головна сторінка"
        ]);
    }

    /**
     * повертає товари(або ще їх характеристики для фільтрації) за вхідними даними
     * */
    public function actionGettovarsbyrules()
    {
        if ($this->isGet()) {
            $tovars = $this->productModel->getSomeProducts($this->countTovars, $_GET["idTovarGroup"]);
            for ($i = 0; $i < count($tovars); $i++) {
                $tovars[$i]["images"] = $this->productModel->getImagesByIdProduct($tovars[$i]["idTovar"]);
                $tovars[$i]["idBasket"] = $this->basketsModel->getByTovarId($tovars[$i]["idTovar"], $_SESSION["user"]["idUser"]);
                $tovars[$i]["rating"] = $this->commentsModel->getAvgStars($tovars[$i]["idTovar"]);
            }
            $res["tovars"] = $tovars;
            $res["user"] = $_SESSION["user"];
            if (!empty($_GET["idTovarGroup"])) {
                $res["rules"] = $this->groupsModel->getCharacteristics($_GET["idTovarGroup"]);
            }
            exit(json_encode($res));
        }
        if ($this->isPost()) {
            $tovars = null;
            if(!empty($_POST["brandId"]))
                $tovars = $this->productModel->getSomeProductsByBrandName($_POST["brandId"], $this->countTovars,$_POST["actualIndex"]);
            else {
                if($_POST["min"] && $_POST["min"]!="null")
                    $tovars = $this->productModel->getSomeProductsByParams($this->countTovars, $_POST["idTovarGroup"],
                        $_POST["min"], $_POST["max"], $_POST["brands"], $_POST["action"], $_POST["actualIndex"]);
                else
                    $tovars = $this->productModel->getSomeProducts($this->countTovars, $_POST["idTovarGroup"], $_POST["actualIndex"]);
            }
            for ($i = 0; $i < count($tovars); $i++) {
                $tovars[$i]["images"] = $this->productModel->getImagesByIdProduct($tovars[$i]["idTovar"]);
                $tovars[$i]["idBasket"] = $this->basketsModel->getByTovarId($tovars[$i]["idTovar"], $_SESSION["user"]["idUser"]);
                $tovars[$i]["rating"] = $this->commentsModel->getAvgStars($tovars[$i]["idTovar"]);
            }
            $res["user"] = $_SESSION["user"];
            $res["tovars"] = $tovars;
            exit(json_encode($res));
        }
    }

    public function actionStatistic(){
        $countUsers = $this->usersModel->getCountUsers();
        $countSellers = $this->usersModel->getCountSellers();
        $countTovarsAdmins = $this->usersModel->getCountTovarsAdmins();
        $countUsersAdmins = $this->usersModel->getCountUsersAdmins();
        $countTovarsStat = $this->productModel->getCountProducts();
        $countGroups = $this->groupsModel->getCountGroups();
        $countBrands = $this->brandsModel->getCountBrands();

        $allSellers = $this->usersModel->getAllSellers();
        for($i=0;$i<count($allSellers);$i++){
            $allSellers[$i]["money"] = $this->ordersModel->getMoney($allSellers[$i]["idUser"]);
        }

        return $this->render("statistic", ["statistic"=>[
            "countUsers"=>$countUsers,
            "countSellers"=>$countSellers,
            "countAdmins"=>$countTovarsAdmins+$countUsersAdmins,
            "countTovars"=>$countTovarsStat,
            "countGroups"=>$countGroups,
            "countBrands"=>$countBrands,
            "allSellers"=>$allSellers
        ]], ["MainTitle"=>"Заповнення анкети"]);
    }
}
