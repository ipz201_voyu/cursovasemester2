<?php

namespace controllers;

use core\Controller;
use models\Users;

class Products extends Controller
{
    protected $user;
    protected $productModel;
    protected $brandsModel;
    protected $groupsModel;
    protected $commentsModel;
    protected $basketModel;

    public function __construct()
    {
        $this->productModel = new \models\Products();
        $this->brandsModel = new \models\Brands();
        $this->groupsModel = new \models\Groups();
        $this->commentsModel = new \models\Comments();
        $this->basketModel = new \models\Baskets();
        $modelUser = new Users();
        $this->user = $modelUser->getUser();
    }

    /**
     * відповідає за тимчасового товару
     */
    public function actionAdd()
    {
        if ($this->user["accessUser"] != 1 and $this->user["accessUser"] != 5)
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");
        $brands = $this->brandsModel->getAll();
        for ($i = 0; $i < count($brands); $i++) {
            $brands[$i]["group"] = $this->groupsModel->getGroupById($brands[$i]["idTovarGroup"]);
        }
        if ($this->isPost()) {
            $res = $this->productModel->addProduct($_POST);
            if (is_string($res)) {
                return $this->render(
                    "edit",
                    ["brands" => $brands],
                    [
                        "MainTitle" => "Додавання товару",
                        "MessageType" => "danger",
                        "MessageContent" => $res
                    ]
                );
            }

            $countFiles = count($_FILES['inputFiles']['name']);
            for ($i = 0; $i < $countFiles; $i++) {
                if (is_file($_FILES["inputFiles"]["tmp_name"][$i])) {
                    $type = null;
                    switch ($_FILES["inputFiles"]["type"][$i]) {
                        case "image/png":
                            $type = ".png";
                            break;
                        case "image/jpeg":
                            $type = ".jpg";
                            break;
                        default:
                            $type = null;
                    }
                    $hrefImage = $this->productModel->addTempImageProduct($res, $type);
                    move_uploaded_file($_FILES["inputFiles"]["tmp_name"][$i], 'images/products/' . $hrefImage . $type);
                    $im = new \Imagick();
                    $im->readImage($_SERVER["DOCUMENT_ROOT"] . '/images/products/' . $hrefImage . $type);
                    $im->thumbnailImage(640, 480, true, true);
                    $im->writeImage($_SERVER["DOCUMENT_ROOT"] . '/images/products/' . $hrefImage . "_2" . $type);
                    $im->thumbnailImage(150, 150, false, true);
                    $im->writeImage($_SERVER["DOCUMENT_ROOT"] . '/images/products/' . $hrefImage . "_1" . $type);
                    unlink('images/products/' . $hrefImage . $type);
                }
            }
            return $this->generateMessageIndex(0, "Товар відправлено на підтвердження адміну!");
        } else {
            return $this->render(
                "edit",
                ["brands" => $brands],
                ["MainTitle" => "Додавання товару"]
            );
        }
    }

    /**
     * повертає сторінку з інформацією для певного товару
     */
    public function actionIndex()
    {
        if ($this->isGet()) {

            if (!empty($_GET["idTemp"])) {
                $res = $this->productModel->getProductById($_GET["idTemp"], true);
                $images = $this->productModel->getImagesByIdProduct($res["idTovar"], false, true);
                $tempImages = $this->productModel->getImagesByIdProduct($res["idTempTovar"], true);
                $res["tovarImages"] = array_merge($images, $tempImages);
                /*if (empty($res) or ($this->user["accessUser"] != 4 and $this->user["accessUser"] != 2 and $this->user["accessUser"] != 5))
                    return $this->generateMessageIndex(1, "Сторінка не знайдена!");*/
                $res["rating"] = 5;
                $res["countComments"] = 0;


                if (empty($res) or ($this->user["accessUser"] == 3 or empty($this->user)) or
                    ($this->user["accessUser"] == 1 and $res["idUser"] != $this->user["idUser"]))
                    return $this->generateMessageIndex(1, "Сторінка не знайдена!");

                return $this->render(
                    "index",
                    [
                        "tovar" => $res,
                        "comments" => null,
                        "isCommented" => true
                    ],
                    ["MainTitle" => $res["nameTovar"]]
                );
            }

            if (!empty($_GET["id"])) {
                $res = $this->productModel->getProductById($_GET["id"]);
                $res["tovarImages"] = $this->productModel->getImagesByIdProduct($res["idTovar"]);
                if (empty($res))
                    return $this->generateMessageIndex(1, "Сторінка не знайдена!");

                $res["rating"] = $this->commentsModel->getAvgStars($res["idTovar"]);
                $res["countComments"] = $this->commentsModel->getMainCommentsCount($res["idTovar"]);
                $res["idBasket"] = $this->basketModel->getByTovarId($res["idTovar"], $this->user["idUser"]);
                $isCommented = $this->commentsModel->checkUsersCommentsTovars($res["idTovar"]);
                $comments = $this->commentsModel->getCommentsTovar($res["idTovar"]);
                for ($i = 0; $i < count($comments); $i++) {
                    $comments[$i]["dopComments"] = $this->commentsModel->getCommentsByIdTovarAndIdFor($res["idTovar"], $comments[$i]["idComment"]);
                    if ($comments[$i]["idForComment"] == -1) {
                        $comments[$i]["idForComment"] = $comments[$i]["idComment"];
                    }
                }
                return $this->render("index", [
                    "tovar" => $res,
                    "comments" => $comments,
                    "isCommented" => $isCommented
                ], ["MainTitle" => $res["nameTovar"]]);
            }
        }
        return $this->generateMessageIndex(1, "Сторінка не знайдена!");
    }

    /**
     * повертає сторінку з товарами, що очікують підтвердження додання або оновлення
     */
    public function actionVarificate()
    {
        if ($this->user["accessUser"] != 2 and $this->user["accessUser"] != 4 and $this->user["accessUser"] != 5)
            return $this->generateMessageIndex(1, "Сторінка не знайдена!");
        $res = $this->productModel->getTempProducts();
        return $this->render(
            "varificate",
            ["tovars" => $res],
            ["MainTitle" => "Варифікація товарів"]
        );
    }

    /**
     * повертає сторінку інформацією про товар, що очікує підтвердження додання або оновлення
     */
    public function actionAcceptview()
    {
        if ($this->user["accessUser"] != 2 and $this->user["accessUser"] != 4 and $this->user["accessUser"] != 5)
            return $this->generateMessageIndex(1, "Сторінка не знайдена!");
        $res = $this->productModel->getProductById($_GET["idTemp"], true);
        $res["tovarImages"] = $this->productModel->getImagesByIdProduct($res["idTovar"], false, true);
        $res["tovarImages"] = array_merge($res["tovarImages"], $this->productModel->getImagesByIdProduct($res["idTempTovar"], true));
        return $this->render(
            "index",
            ["tovar" => $res, "access" => 2],
            ["MainTitle" => "Варифікація товару"]
        );
    }

    /**
     * при підтвердженні прийняття товару, відбувається перенесення товару з тимчасової категорії у постійну,
     * та виведення його на показ для користувачів
     */
    public function actionAccept()
    {
        if ($this->user["accessUser"] != 2 and $this->user["accessUser"] != 4 and $this->user["accessUser"] != 5)
            return $this->generateMessageIndex(1, "Сторінка не знайдена!");
        if ($this->isGet()) {
            $this->productModel->acceptProduct($_GET["idTemp"]);
            return $this->generateMessageIndex(0, "Товар прийнято!", "products/varificate");
        }
        return $this->generateMessageIndex(1, "Сторінка не знайдена!");
    }

    /**
     * при відхиленні товару відбудеться видалення усієї тимчаосової інформації про товар
     */
    public function actionUnaccept()
    {
        if ($this->user["accessUser"] != 2 and $this->user["accessUser"] != 4 and $this->user["accessUser"] != 5)
            return $this->generateMessageIndex(1, "Сторінка не знайдена!");
        if ($this->isPost()) {
            if (empty($_POST["message"])) {
                $res = $this->productModel->getProductById($_POST["idTemp"], true);
                $res["tovarImages"] = $this->productModel->getImagesByIdProduct($res["idTovar"], false, true);
                $res["tovarImages"] = array_merge($res["tovarImages"], $this->productModel->getImagesByIdProduct($res["idTempTovar"], true));
                return $this->render(
                    "index",
                    [
                        "tovar" => $res,
                        "access" => 2
                    ],
                    [
                        "MainTitle" => "Додавання товару",
                        "MessageType" => "danger",
                        "MessageContent" => "Повідомте користувача про причину відмовлення!"
                    ]
                );
            }
            $res = $this->productModel->getProductById($_POST["idTemp"], true);
            if ($res["idTovar"])
                $this->productModel->resetImageByIdProduct($res["idTovar"]);

            $tovar = $this->productModel->getAdminProductMail($_POST["idTemp"]);
            $this->productModel->delete($_POST["idTemp"], true);
            $images = $this->productModel->getImagesByIdProduct($_POST["idTemp"], true);
            if (!empty($images)) {
                foreach ($images as $image) {
                    $this->productModel->deleteImage($image["idTempImage"], true);
                }
            }
            $title = $tovar["typeChanging"] == 0 ? "Ваш товар не прийнято!" : "Зміни у вашому товарі не прийнято!";
            $text = $tovar["typeChanging"] == 0 ? 'дозвіл продажу вашого товару ' . $tovar['nameTovar'] . ' на нашому сайті'
                : 'зміни вашого товару ' . $tovar['nameTovar'] . ' що вже є популярним на нашому сайті!';
            mail(
                $tovar["loginUser"],
                $title,
                "Доброго дня!\n Було надіслано запит на " . $text .
                ".\nНа жаль, у запиті було відмовлено, причина: "
                . lcfirst($_POST["message"]) . "\nДякуємо за використання нашого сайту! З повагою, адміністрація!"
            );
            return $this->generateMessageIndex(1, "Товар не прийнято!", "products/varificate");
        }
        return $this->generateMessageIndex(1, "Сторінка не знайдена!");
    }

    /**
     * повертає сторінку з товарами, які надсилав користувач на погодження додавання, які були прийняті, або очікують свого прийняття
     */
    public function actionMyproducts()
    {
        if ($this->user["accessUser"] != 1 and $this->user["accessUser"] != 5)
            return $this->generateMessageIndex(1, "Сторінка не знайдена!");
        $res = $this->productModel->getSomeProductsByUserId($this->user["idUser"], false, 5);
        return $this->render(
            "myProducts",
            ["tovars" => $res],
            ["MainTitle" => "Мої товари"]
        );
    }

    /**
     * повертає дані, що складають коротку інформацію про товари власником яких є користувач
     */
    public function actionGetmyproducts()
    {
        if ($this->isGet()) {
            if ($this->user["accessUser"] != 1 and $this->user["accessUser"] != 5)
                exit(json_encode(["error" => "Сторінку не знайдено!"]));
            $res = "";
            if ($_GET["typeTovars"] == 0) {
                $res = $this->productModel->getSomeProductsByUserId($this->user["idUser"], false, 20, $_GET["startIndex"]);
            } elseif ($_GET["typeTovars"] == 1) {
                $res = $this->productModel->getSomeProductsByUserId($this->user["idUser"], true, 20, $_GET["startIndex"]);
            }
            exit(json_encode($res));
        }
    }

    /**
     * відповідає за легке оновлення даних(оновлення, що не потребують підтвердження адмінами)
     */
    public function actionLightupdate()
    {
        if ($this->isGet()) {
            $res = "";
            $comments = null;
            if (!empty($_GET["idTemp"])) {
                $res = $this->productModel->getProductById($_GET["idTemp"], true);
                $res["tovarImages"] = $this->productModel->getImagesByIdProduct($res["idTovar"], false, true);
                $res["tovarImages"] = array_merge($res["tovarImages"], $this->productModel->getImagesByIdProduct($res["idTempTovar"], true));
            } else {
                if (!empty($_GET["id"])) {
                    $res = $this->productModel->getProductById($_GET["id"]);
                    $res["tovarImages"] = $this->productModel->getImagesByIdProduct($res["idTovar"]);
                    $res["rating"] = $this->commentsModel->getAvgStars($res["idTovar"]);
                    $res["countComments"] = $this->commentsModel->getMainCommentsCount($res["idTovar"]);
                    $comments = $this->commentsModel->getCommentsTovar($res["idTovar"]);
                    for ($i = 0; $i < count($comments); $i++) {
                        $comments[$i]["dopComments"] = $this->commentsModel->getCommentsByIdTovarAndIdFor($res["idTovar"], $comments[$i]["idComment"]);
                        if ($comments[$i]["idForComment"] == -1) {
                            $comments[$i]["idForComment"] = $comments[$i]["idComment"];
                        }
                    }
                }
            }
            if ($res["idUser"] != $this->user["idUser"] and ($this->user["accessUser"] != 2 or $this->user["accessUser"] != 5))
                return $this->generateMessageIndex(1, "Сторінка не знайдена!");
            return $this->render(
                "index",
                [
                    "tovar" => $res,
                    "comments" => $comments,
                    "isCommented" => true,
                    "access" => 1
                ],
                ["MainTitle" => $res["nameTovar"]]
            );
        }
        if ($this->isPost()) {
            if ($this->user["accessUser"] != 1 and $this->user["accessUser"] != 5)
                return $this->generateMessageIndex(1, "Сторінка не знайдена!");

            if (!empty($_POST["idTemp"])) {
                $this->productModel->updateById($_POST, $_POST["idTemp"], true, true);
            } else if (!empty($_POST["id"])) {
                $this->productModel->updateById($_POST, $_POST["id"], false, true);
            }
            return $this->generateMessageIndex(0, "Успішно змінено!", "products/myproducts");
        }
        return $this->generateMessageIndex(1, "Сторінка не знайдена!");
    }

    /**
     * видалення товару
     */
    public function actionDelete()
    {
        if ($this->isGet()) {
            $res = "";
            if (!empty($_GET["idTemp"])) {
                $res = $this->productModel->getProductById($_GET["idTemp"], true);
            } else {
                if (!empty($_GET["id"])) {
                    $res = $this->productModel->getProductById($_GET["id"]);
                }
            }
            if ($res["idUser"] != $this->user["idUser"] and ($this->user["accessUser"] != 5 or $this->user["accessUser"] != 1))
                return $this->generateMessageIndex(1, "Сторінка не знайдена!");
            $images = null;
            if (!empty($_GET["id"])) {
                $this->productModel->delete($_GET["id"]);
            }
            if (!empty($_GET["idTemp"])) {
                $this->productModel->delete($_GET["idTemp"], true);
            }
            return $this->generateMessageIndex(1, "Товар видалено!", "products/myproducts");
        }
        return $this->generateMessageIndex(1, "Сторінка не знайдена!");
    }

    /**
     * відповідає за серйозне оновлення даних(оновлення, що потребують підтвердження адмінами)
     */
    public function actionMainupdate()
    {
        if ($this->isGet()) {
            $res = "";
            $comments = "";
            if (!empty($_GET["idTemp"])) {
                $res = $this->productModel->getProductById($_GET["idTemp"], true);
            } else {
                if (!empty($_GET["id"])) {
                    $res = $this->productModel->getProductById($_GET["id"]);
                    $res["rating"] = $this->commentsModel->getAvgStars($res["idTovar"]);
                    $res["countComments"] = $this->commentsModel->getMainCommentsCount($res["idTovar"]);
                    $comments = $this->commentsModel->getCommentsTovar($res["idTovar"]);
                    for ($i = 0; $i < count($comments); $i++) {
                        $comments[$i]["dopComments"] = $this->commentsModel->getCommentsByIdTovarAndIdFor($res["idTovar"], $comments[$i]["idComment"]);
                        if ($comments[$i]["idForComment"] == -1) {
                            $comments[$i]["idForComment"] = $comments[$i]["idComment"];
                        }
                    }
                }
            }
            $res["tovarImages"] = $this->productModel->getImagesByIdProduct($res["idTovar"]);
            if ($res["idUser"] != $this->user["idUser"] and ($this->user["accessUser"] != 1 or $this->user["accessUser"] != 5))
                return $this->generateMessageIndex(1, "Сторінка не знайдена!");
            if ($_GET["idTemp"]) {
                $res["tovarImages"] = $this->productModel->getImagesByIdProduct($res["idTovar"], false, true);
                $tempImages = $this->productModel->getImagesByIdProduct($_GET["idTemp"], true);
                $res["tovarImages"] = array_merge($res["tovarImages"], $tempImages);
            }
            $brands = $this->brandsModel->getAll();
            for ($i = 0; $i < count($brands); $i++) {
                $brands[$i]["group"] = $this->groupsModel->getGroupById($brands[$i]["idTovarGroup"]);
            }
            return $this->render(
                "edit",
                [
                    "tovar" => $res,
                    "brands" => $brands,
                    "comments" => $comments,
                    "isCommented" => true
                ],
                [
                    "MainTitle" => "Редагування " . $res["nameTovar"]
                ]
            );
        }
        if ($this->isPost()) {
            if (empty($_POST["id"])) {
                $res = $this->productModel->updateById($_POST, $_POST["idTempTovar"], true);
            } else {
                $res = $this->productModel->updateById($_POST, $_POST["id"]);
            }

            if ($res !== true) {
                $brands = $this->brandsModel->getAll();
                for ($i = 0; $i < count($brands); $i++) {
                    $brands[$i]["group"] = $this->groupsModel->getGroupById($brands[$i]["idTovarGroup"]);
                }
                return $this->render(
                    "edit",
                    ["brands" => $brands],
                    [
                        "MainTitle" => "Додавання товару",
                        "MessageType" => "danger",
                        "MessageContent" => $res
                    ]
                );
            } else {
                $idTempTovar = $_POST["idTempTovar"];
                if (!empty($_POST["id"])) {
                    $idTempTovar = $this->productModel->getTempProductByIdTovar($_POST["id"]);
                }
                $countFiles = count($_FILES['inputFiles']['name']);
                for ($i = 0; $i < $countFiles; $i++) {
                    if (is_file($_FILES["inputFiles"]["tmp_name"][$i])) {
                        $type = null;
                        switch ($_FILES["inputFiles"]["type"][$i]) {
                            case "image/png":
                                $type = ".png";
                                break;
                            case "image/jpeg":
                                $type = ".jpg";
                                break;
                            default:
                                $type = null;
                        }
                        $hrefImage = $this->productModel->addTempImageProduct($idTempTovar, $type);
                        move_uploaded_file($_FILES["inputFiles"]["tmp_name"][$i], 'images/products/' . $hrefImage . $type);
                        $im = new \Imagick();
                        $im->readImage($_SERVER["DOCUMENT_ROOT"] . '/images/products/' . $hrefImage . $type);
                        $im->thumbnailImage(640, 480, true, true);
                        $im->writeImage($_SERVER["DOCUMENT_ROOT"] . '/images/products/' . $hrefImage . "_2" . $type);
                        $im->thumbnailImage(150, 150, false, true);
                        $im->writeImage($_SERVER["DOCUMENT_ROOT"] . '/images/products/' . $hrefImage . "_1" . $type);
                        unlink('images/products/' . $hrefImage . $type);
                    }
                }
                return $this->generateMessageIndex(0, "Відправлено запит на оновлення даних!", "products/myproducts");
            }
        }
        return $this->generateMessageIndex(1, "Сторінка не знайдена!");
    }

    /**
     * відповідає за видалення малюнків, прив'язаних до товару
     */
    public function actionDeleteimage()
    {
        if ($this->isPost()) {
            $res = null;
            if ($_POST["idTempImage"]) {
                $this->productModel->deleteImage($_POST["idTempImage"], true);
                $idNormalTovar = $this->productModel->getProductById($_POST["idTempTovar"], true)["idTovar"];
                $images = $this->productModel->getImagesByIdProduct($idNormalTovar);
                $tempImages = $this->productModel->getImagesByIdProduct($_POST["idTempTovar"], true);
                if (empty($images)) {
                    if (!empty($tempImages)) {
                        $res = $tempImages;
                    }
                } else {
                    if (empty($tempImages)) {
                        $res = $images;
                    } else {
                        $res = array_merge($images, $tempImages);
                    }
                }
            } else {
                $this->productModel->deleteImage($_POST["idImage"]);
                $res = $this->productModel->getImagesByIdProduct($_POST["idTovar"]);
                $idTempTovar = $this->productModel->getTempProductByIdTovar($_POST["idTovar"]);
                if (!empty($idTempTovar)) {
                    $tempImages = $this->productModel->getImagesByIdProduct($idTempTovar, true);
                    if (!empty($res)) {
                        $res = array_merge($res, $tempImages);
                    } else {
                        $res = $tempImages;
                    }
                }
            }
            exit(json_encode($res));
        }
        return $this->generateMessageIndex(1, "Сторінка не знайдена!");
    }
}
