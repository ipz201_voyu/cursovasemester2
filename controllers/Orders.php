<?php

namespace controllers;

use core\Controller;

class Orders extends Controller
{
    protected $user;
    protected $orderModel;

    public function __construct()
    {
        $modelUser = new \models\Users();
        $this->user = $modelUser->getUser();
        $this->orderModel = new \models\Orders();
    }

    public function actionGetforseller(){
        $idSeller = $this->user["idUser"];
        $statistic = $this->orderModel->getStatisticForSeller($idSeller);
        return $this->render("index", ["statistics"=>$statistic],["Статистика продажу моїх товарів"]);
    }

    public function actionGetsellerinfo(){
        $idSeller = $_GET["idSeller"];
        $statistic = $this->orderModel->getStatisticForSeller($idSeller)["ordersReady"];
        /*$statistic = $this->orderModel->GetStatisticForSeller($idSeller)["ordersProcessed"];*/
        exit (json_encode($statistic));
    }

    public function actionGetorders(){
        $idSeller = $this->user["idUser"];
        $statistic = $this->orderModel->getForSeller($idSeller);
        return $this->render("index", ["statistics"=>$statistic, "ignore"=>true],["Замовлення, що в обробці"]);
    }

    public function actionReadyorder(){
        if($this->isGet()){
            $this->orderModel->setFinishDate($_GET["idOrder"]);
            header("Location: /orders/getorders");
        }
    }
}