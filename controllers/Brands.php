<?php

namespace controllers;

use core\Controller;
use models\Users;

class Brands extends Controller
{
    protected $user;
    protected $brandsModel;
    protected $groupsModel;
    protected $productModel;
    public function __construct()
    {
        $this->brandsModel = new \models\Brands();
        $this->groupsModel = new \models\Groups();
        $this->productModel = new \models\Products();
        $modelUser = new Users();
        $this->user = $modelUser->getUser();
    }

    /**
     * сторінка з брендами
     */
    public function actionIndex()
    {
        if ($this->user["accessUser"] !=1 and $this->user["accessUser"] !=2 and $this->user["accessUser"] !=5)
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");


        $brands = $this->brandsModel->getAll();
        $tovarsGroups = $this->groupsModel->getAllGroups();

        for($i=0;$i<count($brands);$i++){
            $brands[$i]["countTovars"] = $this->productModel->getCountProductsByIdBrand($brands[$i]["idBrand"]);
        }

        return $this->render(
            "index",
            [
                "brands" => $brands,
                "groups" => $tovarsGroups
            ],
            ["MainTitle" => "Бренди"]
        );
    }

    /**
     * додання бренду
     */
    public function actionAdd()
    {
        if ($this->user["accessUser"] !=2 and $this->user["accessUser"] !=4 and $this->user["accessUser"] !=5 )
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");
        if ($this->isPost()) {
            $uns = $this->brandsModel->add($_POST);
            $res["brands"] = $this->brandsModel->getAll();

            for($i=0;$i<count($res["brands"]);$i++){
                $res["brands"][$i]["countTovars"] = $this->productModel->getCountProductsByIdBrand($res["brands"][$i]["idBrand"]);
            }

            if (is_string($uns)) {
                $res["error"] = $uns;
            }
            exit(json_encode($res));
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }

    /**
     * оновлення бренду
     */
    public function actionUpdate()
    {
        if ($this->user["accessUser"] !=2 and $this->user["accessUser"] !=4 and $this->user["accessUser"] !=5 )
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");
        if ($this->isPost()) {
            $uns = $this->brandsModel->update($_POST);
            $res["brands"] = $this->brandsModel->getAll();

            for($i=0;$i<count($res["brands"]);$i++){
                $res["brands"][$i]["countTovars"] = $this->productModel->getCountProductsByIdBrand($res["brands"][$i]["idBrand"]);
            }

            if (is_string($uns)) {
                $res["error"] = $uns;
            }
            exit(json_encode($res));
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }

    /**
     * видалення бренду
     */
    public function actionDelete()
    {
        if ($this->user["accessUser"] !=2 and $this->user["accessUser"] !=4 and $this->user["accessUser"] !=5 )
            return $this->generateMessageIndex(1, "Сторінку не знайдено!");
        if ($this->isGet()) {
            $this->brandsModel->delete($_GET["idBrand"]);
            $res["brands"] = $this->brandsModel->getAll();

            for($i=0;$i<count($res["brands"]);$i++){
                $res["brands"][$i]["countTovars"] = $this->productModel->getCountProductsByIdBrand($res["brands"][$i]["idBrand"]);
            }

            exit(json_encode($res));
        }
        return $this->generateMessageIndex(1, "Сторінку не знайдено!");
    }
}
