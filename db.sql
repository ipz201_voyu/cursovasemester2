-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 21 2022 г., 02:35
-- Версия сервера: 5.7.33
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cursovadb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `baskets`
--

CREATE TABLE `baskets` (
  `idBasket` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idTovar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `baskets`
--

INSERT INTO `baskets` (`idBasket`, `idUser`, `idTovar`) VALUES
(94, 14, 1),
(96, 8, 4),
(97, 8, 3),
(98, 15, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `brands`
--

CREATE TABLE `brands` (
  `idBrand` int(11) NOT NULL,
  `nameBrand` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `idTovarGroup` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `brands`
--

INSERT INTO `brands` (`idBrand`, `nameBrand`, `idTovarGroup`) VALUES
(12, 'LG', 4),
(13, 'Lenovo', 4),
(14, 'Бренд5', 4),
(15, 'ASUS', 11),
(16, 'Sumsung', 12);

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `idComment` int(11) NOT NULL,
  `textComment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ratingTovar` tinyint(4) DEFAULT NULL,
  `idTovar` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `dateAdding` datetime NOT NULL,
  `idForComment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`idComment`, `textComment`, `ratingTovar`, `idTovar`, `idUser`, `dateAdding`, `idForComment`) VALUES
(15, 'Класний телевізор! Дуууууууже задоволений!!!!!', 5, 1, 8, '2022-01-03 00:58:15', -1),
(16, ' Alex1231 Verbovskiy, Вау, дякую за відгук, обов\'язково візьму!!!', NULL, 1, 14, '2022-01-03 01:13:04', 15),
(17, 'user user, чудово!', NULL, 1, 8, '2022-01-03 01:14:34', 15),
(18, 'Alex1231 Verbovskiy, )', NULL, 1, 14, '2022-01-03 01:14:52', 15);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `idOrder` int(11) NOT NULL,
  `idTovar` int(11) NOT NULL,
  `dateStart` date DEFAULT NULL,
  `dateFinish` date DEFAULT NULL,
  `addressDelivery` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullNameUser` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `countTovar` int(11) NOT NULL,
  `telUser` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emailUser` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idClient` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`idOrder`, `idTovar`, `dateStart`, `dateFinish`, `addressDelivery`, `fullNameUser`, `countTovar`, `telUser`, `emailUser`, `idClient`, `price`) VALUES
(1, 1, '2020-01-22', '2020-01-22', 'цук у ', 'Alex1231 Verbovskiy', 1, '0678888888', 'cofeek5@gmail.com', 8, 160),
(2, 1, '2020-01-22', '2021-01-22', 'цук у ', 'Alex1231 Verbovskiy', 1, '0678888888', 'cofeek5@gmail.com', 8, 200),
(3, 3, '2021-01-22', '2021-01-22', 'tg dfghdf', 'Alex1231 Verbovskiy', 1, '0678888888', 'cofeek5@gmail.com', 8, 29600);

-- --------------------------------------------------------

--
-- Структура таблицы `questionnaire`
--

CREATE TABLE `questionnaire` (
  `idQuestionnaire` int(11) NOT NULL,
  `idTovar` int(11) NOT NULL,
  `addressDelivery` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullNameUser` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `countTovar` int(11) NOT NULL,
  `telUser` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emailUser` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idClient` int(11) DEFAULT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tempkeys`
--

CREATE TABLE `tempkeys` (
  `idKey` int(11) NOT NULL,
  `hashKey` text COLLATE utf8mb4_unicode_ci,
  `dateKey` datetime NOT NULL,
  `idUser` int(11) NOT NULL,
  `statusKey` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tempkeys`
--

INSERT INTO `tempkeys` (`idKey`, `hashKey`, `dateKey`, `idUser`, `statusKey`) VALUES
(16, '09ea4c771fd41d6e857f2a6f64488f5e', '2021-10-27 00:00:00', 17, 0),
(17, '713316ba1a8964f313524c440d294ce9', '2021-10-31 12:48:05', 18, 0),
(18, 'c7fd91576dab4ee4c978e94daea75c88', '2021-10-31 12:50:02', 19, 0),
(19, '9a7bc849b76262e8f796a3e7fdf23e94', '2021-10-31 14:15:22', 0, 0),
(20, '1e3b46615ee79f118e177e8127b13734', '2021-11-21 23:29:33', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `temptovars`
--

CREATE TABLE `temptovars` (
  `idTempTovar` int(11) NOT NULL,
  `actionTovar` tinyint(4) NOT NULL,
  `nameTovar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `idBrand` int(11) NOT NULL,
  `guaranteeTovar` int(11) NOT NULL,
  `countTovar` int(11) NOT NULL,
  `priceTovar` float NOT NULL,
  `idUser` int(11) NOT NULL,
  `countryCreator` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `infoTovar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `descriptionTovar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateSending` datetime NOT NULL,
  `typeChanging` tinyint(4) NOT NULL,
  `idTovar` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `temptovarsimages`
--

CREATE TABLE `temptovarsimages` (
  `idTempImage` int(11) NOT NULL,
  `hrefImage` text COLLATE utf8mb4_unicode_ci,
  `typeImage` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `idTempTovar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tempusers`
--

CREATE TABLE `tempusers` (
  `idUser` int(11) NOT NULL,
  `loginUser` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstNameUser` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `secondNameUser` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `passwordUser` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `accessUser` int(11) NOT NULL,
  `salt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `typeImage` text COLLATE utf8mb4_unicode_ci,
  `telephoneUser` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tovargroups`
--

CREATE TABLE `tovargroups` (
  `idTovarGroup` int(11) NOT NULL,
  `nameTovarGroup` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tovargroups`
--

INSERT INTO `tovargroups` (`idTovarGroup`, `nameTovarGroup`) VALUES
(4, 'Телевізори'),
(11, 'Ноутбуки'),
(12, 'Телефони');

-- --------------------------------------------------------

--
-- Структура таблицы `tovars`
--

CREATE TABLE `tovars` (
  `idTovar` int(11) NOT NULL,
  `actionTovar` tinyint(4) NOT NULL,
  `nameTovar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `idBrand` int(11) NOT NULL,
  `guaranteeTovar` int(11) NOT NULL,
  `countTovar` int(11) NOT NULL,
  `priceTovar` float NOT NULL,
  `idUser` int(11) NOT NULL,
  `countryCreator` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `infoTovar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `descriptionTovar` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tovars`
--

INSERT INTO `tovars` (`idTovar`, `actionTovar`, `nameTovar`, `idBrand`, `guaranteeTovar`, `countTovar`, `priceTovar`, `idUser`, `countryCreator`, `infoTovar`, `descriptionTovar`) VALUES
(1, 20, 'Телевізор LG OLED55A16LA', 12, 12, 18, 200, 8, 'Південна Корея', '<p>Диагональ экрана <a href=\"https://rozetka.com.ua/all-tv/c80037/21668=6432/\">55\"</a></p><p>Поддержка Smart TV <a href=\"https://rozetka.com.ua/all-tv/c80037/25849=21238/\">со Smart TV</a></p><p>Разрешение <a href=\"https://rozetka.com.ua/all-tv/c80037/21667=25512/\">3840x2160</a></p><p>Тип телевизора <a href=\"https://rozetka.com.ua/all-tv/c80037/unique=50134/\">OLED</a></p><p>Страна-производитель <a href=\"https://rozetka.com.ua/all-tv/c80037/strana-proizvoditelj-tovara-90098=622416/\">Польша</a></p><p>Год выпуска <a href=\"https://rozetka.com.ua/all-tv/c80037/god-vipuska-209371=3801438/\">2021</a></p><p>Форма экрана <a href=\"https://rozetka.com.ua/all-tv/c80037/forma-ekrana=ploskiy/\">Плоский</a></p><p>Цвет рамки <a href=\"https://rozetka.com.ua/all-tv/c80037/22453=black/\">Черный</a></p><p>Назначение <a href=\"https://rozetka.com.ua/all-tv/c80037/naznachenie-209365=dlya-gostinnoy/\">Для гостиной</a></p><p>Выходная мощность звука, Вт <a href=\"https://rozetka.com.ua/all-tv/c80037/vihodnaya-moshchnost-zvuka-vt=2-x-10/\">2 x 10</a></p><p>Операционная система <a href=\"https://rozetka.com.ua/all-tv/c80037/53052=380337/\">WebOS</a></p>', '<h2><strong>НАПОЛНИТЕ СВОЙ МИР СИЯНИЕМ С ПОМОЩЬЮ САМОПОДСВЕЧИВАЮЩИХСЯ ПИКСЕЛЕЙ</strong></h2><p>OLED-телевизор LG - это наслаждение созерцания мира. Самоподсвечивающиеся пиксели обеспечивают непревзойденное качество изображения и целый ряд возможностей для дизайна, в то время как новейшие передовые технологии заставляют восхищаться. Всё, что вы любите в телевизоре. Во всех смыслах.</p><p>&nbsp;</p><p><img src=\"https://img.moyo.ua/img/products_desc/4879/487924_1616753341_0.jpg\" alt=\"\" srcset=\"https://img.moyo.ua/img/products_desc/4879/487924_1616753341_0.jpg\" sizes=\"100vw\"></p><p>&nbsp;</p><h2><strong>В чем отличие OLED от других технологий?</strong></h2><p>Ответ — самоподсвечивающиеся пиксели. Технология самоподсвечивающегося дисплея имеет огромное значение для качества изображения. В отличие от LED-телевизоров, которые ограничены технологией подсветки, OLED-телевизоры LG обладают исключительным реализмом и уникальным дизайном.</p><p>&nbsp;</p><p><img src=\"https://img.moyo.ua/img/products_desc/4879/487924_1616753342_1.jpg\" alt=\"\" srcset=\"https://img.moyo.ua/img/products_desc/4879/487924_1616753342_1.jpg\" sizes=\"100vw\"></p><p>&nbsp;</p><h2><strong>100 миллионов причин полюбить OLED</strong></h2><p>По качеству изображения OLED-телевизоры легко затмевают LED-телевизоры. Телевизоры с OLED-технологией имеют миллионы самоподсвечивающихся пикселей, способных воспроизводить безупречный оттенок черного и точные цвета. Результат - непревзойденные впечатления от просмотра.</p><p>&nbsp;</p><p><img src=\"https://img.moyo.ua/img/products_desc/4879/487924_1616753342_2.jpg\" alt=\"\" srcset=\"https://img.moyo.ua/img/products_desc/4879/487924_1616753342_2.jpg\" sizes=\"100vw\"></p><p>&nbsp;</p><h2><strong>Технология OLED значительно превосходит LED</strong></h2><p>Если говорить просто, в OLED используются миллионы самоподсвечивающихся пикселей, которые могут включаться и выключаться для достижения идеального черного и бесконечной контрастности. Черный цвет в OLED гораздо глубже, чем у мини-LED и LED. Поэтому, независимо от того, сколько тысяч подсветок в них используется, LED-телевизоры остаются в тени OLED.</p><p>&nbsp;</p><p><img src=\"https://img.moyo.ua/img/products_desc/4879/487924_1616753342_3.jpg\" alt=\"\" srcset=\"https://img.moyo.ua/img/products_desc/4879/487924_1616753342_3.jpg\" sizes=\"100vw\"></p><p>&nbsp;</p><h2><strong>OLED обеспечивает непревзойденную точность цветов</strong></h2><p>Итоги тестирования международного агентства Intertek подтвердили 100% точность цветопередачи OLED-дисплеев LG. То есть цвета, которые вы видите на экране, точно соответствуют цветам исходного изображения. Поэтому все, что вы видите, выглядит именно так, как задумал создатель.</p>'),
(3, 20, 'Ноутбук ASUS TUF Gaming F15 FX506HCB-HN161 (90NR0723-M04940) Eclipse Gray', 15, 12, 39, 37000, 8, 'Тайбей', '<p><strong>Процессор:</strong> Шестиядерный Intel Core i5-11400H (2.7 - 4.5 ГГц)</p><p><strong>Операционная система:</strong> Без ОС</p><p><strong>Поколение процессора Intel:</strong> 11-ое Tiger Lake</p>', '<h3>Продуктивний процесор і дисплей з частотою оновлення 144 Гц</h3><p>TUF від Asus - це ще одна лінійка ігрових ноутбуків початкового рівня, яка вийшла настільки вдалою, що виробник намагається нічого в ній не лагодити і не ламати протягом багатьох років. Замість цього інженери компанії оновили залізо ноутбука, осучаснили зовнішність і провели обряд підвищення рівня, додавши до вже наявного списку можливостей приймач Wi-Fi 6, швидку зарядку, сучасні порти USB і аудіосистему, яка тепер дружить з DTS X Ultra і має на борту пачку звукових профілів під шутери, стратегії, рольові ігри, спортивні симулятори і т. д. Оновили і дисплей. Рік-півтора тому для ігрових ноутбуків за тисячу доларів нормою була частота оновлення 60 Гц, нині ж все поголовно перейшли на 144 Гц. І дивлячись на всі ці поліпшення мимоволі задаєшся питанням, Навіщо взагалі купувати ігровий ноутбук за 3 тисячі доларів, коли майже все необхідне є і тут?</p><h3>Знамениті бронебійні лептопи</h3><p>Традиційно сильною стороною ноутбуків TUF є міцний, відмінно зібраний корпус і продумана система охолодження, яка не дасть ноутбуку перетворитися в жерло вулкана, що прокинувся. Тестова модель не виключення ― F15 FX506HCB не скрипить, не скручується і не люфтить, кришка відкривається з невеликим зусиллям, а сам корпус пройшов ряд військових тестів за підсумком яких заслужив жаданий сертифікат MIL-STD-810G. У той же час дизайнери Asus внесли кілька косметичних змін, завдяки яким ноутбук йде в ногу з часом. З внутрішньої частини корпусу прибрали майже всі елементи декору, а обридлу всім червоне підсвічування замінили на повноцінну RGB. Верхня кришка з матовою поверхнею, масивними болтами і логотипом TUG виглядає набагато брутальніше, ніж раніше, а агресивні повітрозабірники і сітчасте днище надають корпусу родзинку.</p><h3>Можливості апгрейда ОЗУ і пам\'яті</h3><p>По начинці картина наступна: 12-потоковий процесор Core i5 11400h з максимальною частотою 4.5 ГГц працює в парі з відеокартою NVIDIA RTX 3050, об\'єм SSD становить 512 ГБ, а комплект оперативної пам\'яті обмежується 8 ГБ. Це однозначний мінус, але його легко виправити, задіявши для апгрейда другий слот слот ОЗУ. Такий же фокус можна провернути і з накопичувачем, додавши в систему другий M. 2 SSD. Благо, що наявні 512 ГБ за нинішніми часами заб\'ються за лічені тижні. В іншому це це чи не максимально можлива комбінація заліза, яку тільки можна знайти за тисячу з гаком доларів. Додайте сюди явну відсутність будь-яких недоліків і отримаєте серйозного кандидата на звання народного ігрового ноутбука.</p>'),
(4, 5, 'Ноутбук ASUS ROG Zephyrus Duo 15 SE GX551QS-HB190R (90NR04N1-M04490) Off Black', 15, 12, 100, 140000, 8, 'Китай', '<p><strong>Диагональ экрана:</strong> 15.6\" (3840x2160) Ultra HD 4K</p><p><strong>Тип экрана:</strong> IPS</p><p><strong>Частота обновления экрана:</strong> 120 Гц</p>', '<h3>Два экрана. Бесчисленные возможности</h3><p>Расширяйте свои игровые и творческие возможности с помощью ноутбука ROG Zephyrus Duo 15 SE, оснащенного инновационным дополнительным дисплеем ROG ScreenPad Plus. Работая под управлением операционной системы Windows 10, он обладает мощной конфигурацией, включающей новейший процессор AMD Ryzen 9&nbsp;5900HX&nbsp;и видеокарту NVIDIA GeForce RTX 3080, и предлагается с дисплеями двух версий: скоростной (300 Гц, 3 мс) для геймеров и высокого разрешения (4K, 120 Гц, 3 мс) для профессиональной работы. Аудиосистема устройства состоит из четырех динамиков с поддержкой технологии иммерсивного звучания Dolby Atmos.*</p><p><img src=\"https://i.citrus.ua/uploads/content/product-photos/topchiy/January-2021/ROG-Zephyrus-Duo-15-SE_GX551_01.jpg\" alt=\"\"></p><h3>Удвоенные возможности</h3><p>Выполняйте любые задачи еще более эффективно, используя все возможности дополнительного дисплея ROG ScreenPad Plus. Располагающийся под комфортным для просмотра контента углом в 13°, этот второй экран с разрешением до 4K облегчает работу в многозадачном режиме, в том числе за счет специально оптимизированных под него приложений. На нем удобно держать окошко чата или элементы управления стримингом во время игры на основном экране, а при обработке цифрового контента в профессиональных приложениях на него можно перенести часто используемые меню с инструментами, чтобы освободить больше экранного пространства для самого проекта.*</p><p><img src=\"https://i.citrus.ua/uploads/content/product-photos/topchiy/January-2021/ROG-Zephyrus-Duo-15-SE_GX551_02.jpg\" alt=\"\"></p><h3>Мощная конфигурация</h3><p>Данный ноутбук предлагает высокую скорость в любых приложениях благодаря мощной конфигурации, которая может включать в себя процессор AMD Ryzen 9&nbsp;5900HX&nbsp;и видеокарту NVIDIA GeForce RTX 3080. Такой процессор легко справится со сложными вычислительными задачами, включая обработку изображений, трехмерный рендеринг и стриминг, а видеокарта обеспечит высокую частоту кадров и реалистичную картинку в современных играх. Для эффективной работы в многозадачном режиме ноутбук укомплектован быстрой памятью DDR4-3200, максимальный объем которой может достигать 32 ГБ. Подсистема хранения данных состоит из массива RAID0 на базе твердотельных накопителей общей емкостью до 2 ТБ.*</p><p><img src=\"https://i.citrus.ua/uploads/content/product-photos/topchiy/January-2021/ROG-Zephyrus-Duo-15-SE_GX551_03.jpg\" alt=\"\"></p><h3>Высококачественное изображение</h3><p>Имеется два варианта основного дисплея ноутбука: профессиональный и геймерский. Первый отличается повышенным разрешением (4K), широким цветовым охватом (100% Adobe RGB), хорошими скоростными параметрами (120 Гц, 3 мс) и точной цветопередачей, достигаемой за счет фабричной калибровки, а второй, с разрешением Full-HD, может похвастать фантастически высокой частотой обновления – 300 Гц. Дисплей поддерживает технологию адаптивной синхронизации Adaptive-Sync, еще больше повышающую качество игровой картинки. И, разумеется, в распоряжении пользователя всегда будет дополнительный сенсорный экран ROG ScreenPad Plus с поддержкой стилусов.*</p><p><img src=\"https://i.citrus.ua/uploads/content/product-photos/topchiy/January-2021/ROG-Zephyrus-Duo-15-SE_GX551_04.jpg\" alt=\"\"></p><h3>Для любых задач</h3><p>Создание ультратонкого геймерского ноутбука с двумя дисплеями и мощной конфигурацией невозможно без инновационных решений, в том числе в области охлаждения. Поэтому позади дополнительного экрана открывается воздухозаборник высотой 28,5 мм, а в качестве термоинтерфейса центрального процессора используется жидкий металл. Для задач различной сложности имеется несколько режимов работы ноутбука с отличающимися соотношениями производительности и уровня шума.</p><p><img src=\"https://i.citrus.ua/uploads/content/product-photos/topchiy/January-2021/ROG-Zephyrus-Duo-15-SE_GX551_05.jpg\" alt=\"\"></p><h3>Инновационное решение</h3><p>Сочетание высокопроизводительных компонентов и дополнительного экрана в изящном корпусе ноутбука требует особой точности в изготовлении. Инженерам пришлось придумать новый вариант дисплейного шарнира – при раскрытии крышки он автоматически приподнимает дополнительный сенсорный экран под наиболее удобным для комфортного взаимодействия углом, а также одновременно открывает воздухозаборное отверстие, способствуя лучшей вентиляции устройства. Неброский дизайн корпуса акцентируется точечным узором, отличающим данную модель от прочих.</p><p><img src=\"https://i.citrus.ua/uploads/content/product-photos/topchiy/January-2021/ROG-Zephyrus-Duo-15-SE_GX551_06.jpg\" alt=\"\"></p><h3>Кристально чистый звук</h3><p>Встроенная акустическая система ноутбука, состоящая из четырех динамиков, умеет создавать эффект 5.1.2-канального иммерсивного звучания на базе технологии Dolby Atmos, позволяя полностью погрузиться в то, что происходит на экране. Два высокочастотных динамика направлены непосредственно на пользователя, а пара низкочастотных направляют звуковую волну вниз, чтобы та отражалась от поверхности стола. Динамичное звучание с минимумом искажений гарантируется даже на полной громкости благодаря технологии интеллектуального усиления. Настройка акустической системы под конкретные приложения осуществляется вручную или автоматически путем выбора одного из шести предустановленных режимов. Для повышения качества голосовой связи служит технология двухстороннего интеллектуального шумоподавления.</p><p><img src=\"https://i.citrus.ua/uploads/content/product-photos/topchiy/January-2021/ROG-Zephyrus-Duo-15-SE_GX551_07.jpg\" alt=\"\"></p><h3>Безграничная свобода</h3><p>Выполненный в тонком и легком корпусе из прочного магний-алюминиевого сплава, ноутбук ROG Zephyrus Duo 15 SE – это по-настоящему мобильное устройство, которое позволяет запускать компьютерные игры, работать над творческими проектами, организовывать стриминговые трансляции и делать массу других вещей, где бы вы ни находились. Этому способствует и длительное время автономной работы, обеспечиваемое аккумулятором емкостью 90 Вт·ч. Подзарядка ноутбука может осуществляться от внешних аккумуляторов через разъем USB-C. Для выполнения простых повседневных задач, таких как просмотр веб-сайтов или трансляция видео, достаточно будет компактного 100-ваттного зарядного устройства, с которым путешествовать будет еще легче.</p><p><img src=\"https://i.citrus.ua/uploads/content/product-photos/topchiy/January-2021/ROG-Zephyrus-Duo-15-SE_GX551_091.jpg\" alt=\"\"></p>'),
(5, 50, 'Мобильный телефон Samsung Galaxy M52 5G 6/128GB Black (SM-M526BZKHSEK)', 16, 12, 1, 12000, 8, 'Корея', '<p><strong>Диагональ экрана:</strong> <a href=\"https://rozetka.com.ua/mobile-phones/c80003/23777=25316/\">6.7</a></p><p><strong>Разрешение дисплея:</strong> <a href=\"https://rozetka.com.ua/mobile-phones/c80003/23778=25480/\">2400 x 1080</a></p><p><strong>Тип матрицы:</strong> <a href=\"https://rozetka.com.ua/mobile-phones/c80003/31565=38085/\">Super AMOLED Plus</a></p><p><strong>Частота обновления экрана</strong>: <a href=\"https://rozetka.com.ua/mobile-phones/c80003/chastota-obnovleniya-ekrana-237226=120-gts/\">120 Гц</a></p>', '<p><strong>Мобільність на максимум</strong></p><p>&nbsp;</p><p><img src=\"https://files.foxtrot.com.ua/StaticContent/ext/foxtrot/samsung/GalaxyM52/img/s1.jpg\"></p><p><strong>Потужний інтелект</strong></p><p>&nbsp;</p><p>Встигайте більше з новим Galaxy M52, що об\'єднав всю міць топового 8-ядерного процесора і оперативної пам\'яті 6 ГБ. Втілюючи абсолютно новий формат швидкості і багатозадачності, він прекрасно підійде для активного життя без обмежень. Поринайте в будь-які вимогливі ігри, транслюйте стрім та переглядайте улюблені відеоблоги – його швидкості з запасом вистачить для всього, що вас надихає.</p><p><img src=\"https://files.foxtrot.com.ua/StaticContent/ext/foxtrot/samsung/GalaxyM52/img/s2.jpg\"></p><p><strong>Ще більше пам\'яті для миттєвого відгуку</strong></p><p>&nbsp;</p><p>Технологія RAM Plus тепер динамічно управляє запасом оперативної пам\'яті в Galaxy M52, розширюючи її за рахунок додаткових 4 ГБ віртуальної пам\'яті, коли ви відкриваєте одразу декілька різних додатків. А це означає, що ви зможете переглядати всі улюблені відео, обробляти фото і спілкуватися в відеочатах без постійних затримок.</p><p><img src=\"https://files.foxtrot.com.ua/StaticContent/ext/foxtrot/samsung/GalaxyM52/img/s3.jpg\"></p><p><strong>Зберігайте, не видаляючи</strong></p><p>&nbsp;</p><p>Створюйте кращу колекцію ігор, відео та фотографій, які завжди будуть поруч з новим Galaxy M52 в ваших руках. Об\'єднуючи 128 ГБ вбудованої пам\'яті та до 1 ТБ на microSD карті, він збереже всі улюблені спогади і додатки, які роблять ваш день неймовірно яскравим! Знімайте, редагуйте і записуйте кращі відео в максимальній роздільній здатності – цей смартфон стане справжньою творчою студією в вашій кишені.</p><p><img src=\"https://files.foxtrot.com.ua/StaticContent/ext/foxtrot/samsung/GalaxyM52/img/s4.jpg\"></p><p><strong>Елегантний мінімалізм</strong></p><p>&nbsp;</p><p>Витончена естетика Galaxy об\'єднала всю міць швидкості, бездоганної оптики і технологій в витонченому Galaxy M52, який виглядає неперевершено. Його плавні лінії тепер доповнені смуговою текстурою, що створює особливий перелив на яскравому світлі. І все це в ультратонкому профілі товщиною всього 7.4 мм, що значно спрощує взаємодію з смартфоном.</p><p><img src=\"https://files.foxtrot.com.ua/StaticContent/ext/foxtrot/samsung/GalaxyM52/img/s5.jpg\"></p><p><strong>Ультраплавний Super AMOLED+ дисплей</strong></p><p>&nbsp;</p><p>Поринайте в колір на бездоганному 6,7-дюймовому Super AMOLED+ дисплеї, який робить зображення оживаючим. З роздільною здатністю Full HD+ та O-подібним вирізом, що не відволікає ваш погляд, він перетворить будь-який контент прямо на вашій долоні. Захоплюючий візуальний досвід доповнить надшвидка частота оновлення до 120 Гц, з якою всі улюблені фільми, ігри та веб-сторінки будуть позбавлені навіть найменших затримок.</p><p><img src=\"https://files.foxtrot.com.ua/StaticContent/ext/foxtrot/samsung/GalaxyM52/img/s6.jpg\"></p><p><img src=\"https://files.foxtrot.com.ua/StaticContent/ext/foxtrot/samsung/GalaxyM52/img/s7.jpg\"></p><p><strong>Майстер професійної фотографії</strong></p><p>&nbsp;</p><p>Потрійна AI-камера, що прекрасно захоплює світло в будь-якому оточенні, дозволить знімати абсолютно різні сюжети. Основна камера 64 МП відобразить всю магію заходу сонця та нічних прогулянок містом, надширококутна камера 12 МП розкриє захоплюючий дух архітектури і широких планів на знімках, а макро-об\'єктив 5 МП дозволить доторкнутися до найменших текстур. Просто шукайте натхнення і миттєво зберігайте найяскравіші моменти життя на приголомшливих знімках та відео.</p><p><strong>Ловіть ще більше емоцій</strong></p><p>&nbsp;</p><p>Надширококутна 12 МП камера Galaxy M52 тепер точно повторює ширину вашого погляду, передаючи ваше бачення кожної фотографії. З кутом огляду 123° вона знімає приголомшливі панорамні кадри, що об’єднують ще більше посмішок, міської архітектури та яскравих пейзажів.</p><p><img src=\"https://files.foxtrot.com.ua/StaticContent/ext/foxtrot/samsung/GalaxyM52/img/s8ico1.jpg\"></p><p><img src=\"https://files.foxtrot.com.ua/StaticContent/ext/foxtrot/samsung/GalaxyM52/img/s8ico2.jpg\"></p>'),
(6, 12, 'dfgd', 12, 12, 20, 123, 8, 'fgdfg', '<p>cxbxcvbcvnbn vc n</p>', '<p>cvbn cvcncvbnc</p>');

-- --------------------------------------------------------

--
-- Структура таблицы `tovarsimages`
--

CREATE TABLE `tovarsimages` (
  `idImage` int(11) NOT NULL,
  `idTovar` int(11) NOT NULL,
  `typeImage` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hrefImage` text COLLATE utf8mb4_unicode_ci,
  `isDel` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tovarsimages`
--

INSERT INTO `tovarsimages` (`idImage`, `idTovar`, `typeImage`, `hrefImage`, `isDel`) VALUES
(68, 1, '.jpg', '86130276c1586c3217ddd111f851ce3b', 0),
(69, 1, '.jpg', '8cd2f8da4889b1215bdb9ef8440afae2', 0),
(75, 1, '.jpg', '49e609b84a0480fb99c13e40f975a2dc', 0),
(76, 1, '.jpg', '14aa0e6ffad019f8971be70a5ab37432', 0),
(77, 3, '.jpg', 'c29837877f697e2bb6bce5011d64ab04', 0),
(78, 3, '.jpg', '1735ec2c5d62acc4a320e3078f43392b', 0),
(79, 3, '.jpg', '4b0a4fd937d1df28d465f2520e0fa09c', 0),
(80, 4, '.jpg', 'e8da81f7aed3fd68376c7bc529a5cddc', 0),
(81, 4, '.jpg', '580f54ff6260599a3bac5af62f50e52d', 0),
(82, 4, '.jpg', 'e5c07ea2ffccafb20f03bccd5f73f049', 0),
(83, 4, '.jpg', '930fb4fbcc0d524a69694cfcfa49c712', 0),
(84, 4, '.jpg', '5a7154dedc596c10843ddfa47faf7611', 0),
(85, 5, '.jpg', '576bfcec324b4704d35a6d03e754d8d6', 0),
(86, 5, '.jpg', '2248ba45fdf22560655fd708ca4333c6', 0),
(87, 5, '.jpg', 'd66b92fa59ae1283de78d95beb1094bd', 0),
(88, 6, '.jpg', 'b1c7d08ae3eb1496f78cfff7f81a526e', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `idUser` int(11) NOT NULL,
  `loginUser` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `passwordUser` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstNameUser` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `secondNameUser` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `accessUser` tinyint(4) NOT NULL,
  `salt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `typeImage` text COLLATE utf8mb4_unicode_ci,
  `telephoneUser` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`idUser`, `loginUser`, `passwordUser`, `firstNameUser`, `secondNameUser`, `accessUser`, `salt`, `image`, `typeImage`, `telephoneUser`) VALUES
(8, 'cofeek5@gmail.com', 'f9a74ff39282c84b0c49680b56996d06', 'Alex1231', 'Verbovskiy', 5, '1637760939', '84f23216a3fc5f663ba718b69fcbc250d', '.jpg', '0678888888'),
(12, 'c1@gmail.com', '21588f0e085ed8a8118e4ddb03ab2bdf', 'wqer', 'qwrqwrq', 1, '1637838573', '27168dec1798850ebfc60d973da9388dd', '.png', '0678888888'),
(14, 'user@gmail.com', '22b7a2022182f71d6fb1fb98adb002a0', 'user', 'user', 4, '1641158830', '22f85e893a461fb92f9ba3663a407caa3', '.jpg', '0678811111'),
(15, 'c2@gmail.com', '79696c4037edb052c60a648c1c1b9ab9', 'уцкк', 'цукуц', 0, '1641805523', '1f697d82ce3691fd25250ae8c961dbfcf', '.jpg', '0678888888');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `baskets`
--
ALTER TABLE `baskets`
  ADD PRIMARY KEY (`idBasket`);

--
-- Индексы таблицы `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`idBrand`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`idComment`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`idOrder`);

--
-- Индексы таблицы `questionnaire`
--
ALTER TABLE `questionnaire`
  ADD PRIMARY KEY (`idQuestionnaire`);

--
-- Индексы таблицы `tempkeys`
--
ALTER TABLE `tempkeys`
  ADD PRIMARY KEY (`idKey`);

--
-- Индексы таблицы `temptovars`
--
ALTER TABLE `temptovars`
  ADD PRIMARY KEY (`idTempTovar`);

--
-- Индексы таблицы `temptovarsimages`
--
ALTER TABLE `temptovarsimages`
  ADD PRIMARY KEY (`idTempImage`);

--
-- Индексы таблицы `tempusers`
--
ALTER TABLE `tempusers`
  ADD PRIMARY KEY (`idUser`);

--
-- Индексы таблицы `tovargroups`
--
ALTER TABLE `tovargroups`
  ADD PRIMARY KEY (`idTovarGroup`);

--
-- Индексы таблицы `tovars`
--
ALTER TABLE `tovars`
  ADD PRIMARY KEY (`idTovar`);

--
-- Индексы таблицы `tovarsimages`
--
ALTER TABLE `tovarsimages`
  ADD PRIMARY KEY (`idImage`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `baskets`
--
ALTER TABLE `baskets`
  MODIFY `idBasket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT для таблицы `brands`
--
ALTER TABLE `brands`
  MODIFY `idBrand` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `idComment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `idOrder` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `questionnaire`
--
ALTER TABLE `questionnaire`
  MODIFY `idQuestionnaire` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `tempkeys`
--
ALTER TABLE `tempkeys`
  MODIFY `idKey` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `temptovars`
--
ALTER TABLE `temptovars`
  MODIFY `idTempTovar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `temptovarsimages`
--
ALTER TABLE `temptovarsimages`
  MODIFY `idTempImage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `tempusers`
--
ALTER TABLE `tempusers`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tovargroups`
--
ALTER TABLE `tovargroups`
  MODIFY `idTovarGroup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `tovars`
--
ALTER TABLE `tovars`
  MODIFY `idTovar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `tovarsimages`
--
ALTER TABLE `tovarsimages`
  MODIFY `idImage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
